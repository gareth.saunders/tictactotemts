import {animate, initThreeJs, updateStateFromStatus} from './engine3d.js';
import {initMenus} from './menuing.js';

//TODO: In the future this can be replaced by a json with assert import
import {menuData, getRegistrantId} from "./menuing/states/menu-data.js";

window.addEventListener("load", () => {
    initThreeJs();
    animate();
    initMenus();

    let ws = new WebSocket(menuData.environment().eventsHost)
    ws.onmessage = (event) => {
        switch (event.data) {
            case "new-registration": return menuData.updateGame(updateStateFromStatus)
            case "move-made": return menuData.updateGame(updateStateFromStatus)
            case "server-connected": return ws.send(getRegistrantId())
        }
    }
})