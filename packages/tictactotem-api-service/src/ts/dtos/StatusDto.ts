import {GameEndResult, GameStatus} from "../../../../tictactotem-engine/src/ts/ResultEntites";

export interface StatusDto {
    status: GameStatus,
    players: {
        order: number,
        name: string,
        accepted: boolean
    }[],
    currentPlayer: number
    finalResult: GameEndResult | undefined,
    boardMap: any
}