import {RedisConnector} from "../connectors/RedisConnector";
import {GameBoard} from "../../../../tictactotem-engine/src/ts/GameBoard";
import {GameResult, TurnResult} from "../../../../tictactotem-engine/src/ts/ResultEntites";
import {PlayerConnectionType} from "../../../../tictactotem-engine/src/ts/BasicEntities";
import {optimumWinCompute} from "../../../../tictactotem-engine/src/ts/Player";
import {EventEmitter} from 'events';
import {RegistrationDto} from "../dtos/RegistrationDto";
import {StatusDto} from "../dtos/StatusDto";

let _service: GameService | undefined = undefined;

export class GameService extends EventEmitter {
    constructor() {
        super();
    }

    async newGame(
        playerConnectionType: PlayerConnectionType, player2Type: string, player3Type?: string): Promise<GameBoard> {

        const players = []
        players.push({
            id: 1,
            name: "Player One (Human)"
        })
        if (player2Type === "computer") {
            players.push({
                id: 2,
                name: "Player Two (Computer)",
                attachedCompute: optimumWinCompute()
            })
        } else {
            players.push({
                id: 2,
                name: "Player Two (Human)"
            })
        }
        if (player3Type) {
            if (player3Type === "computer") {
                players.push({
                    id: 3,
                    name: "Player Three (Computer)",
                    attachedCompute: optimumWinCompute()
                })
            } else {
                players.push({
                    id: 3,
                    name: "Player Three (Human)"
                })
            }
        }

        const game = new GameBoard({
            playerConnectionType: playerConnectionType,
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        })
        const redisHelper = await RedisConnector.getInstance()
        await redisHelper.set(game)

        return game
    }

    async register(gameId: string, registrantId: string, registrantName?: string): Promise<RegistrationDto|null> {
        const redisHelper = await RedisConnector.getInstance()
        const game = await redisHelper.get(gameId)
        return this.maybe( async obj => {
            const result = obj.register(registrantId, registrantName)
            await redisHelper.set(obj)
            this.emit('new-registration', obj);
            return {
                gameId: gameId,
                awaiting: result.awaitingPlayers.map(player => {
                    return {
                        order: player.id,
                        name: player.name,
                        accepted: !!player.externalPlayerHash || !!player.attachedCompute
                    }
                }),
                links: {
                    syncMoveLink: `/invite/turnsync/${ gameId }`,
                    moveLink: `/invite/turn/${ gameId }`,
                    statusLink: `/invite/state/${ gameId }`
                }
            }
        }, game)
    }

    async makeMove(id: string, segment: number, radius: number, depth: number, registrantId?: string): Promise<GameResult | null> {
        const redisHelper = await RedisConnector.getInstance()
        const game = await redisHelper.get(id)
        return this.maybe( async obj => {
            let result = obj.makeMove({
                segment,
                radius,
                depth
            }, obj.currentPlayer, registrantId)
            let nextPlayer = (result as TurnResult).nextPlayer
            while (nextPlayer?.attachedCompute != undefined) {
                result = obj.makeMove(nextPlayer.attachedCompute.function(obj, nextPlayer), nextPlayer)
                nextPlayer = (result as TurnResult).nextPlayer
            }

            const redisHelper = await RedisConnector.getInstance()
            await redisHelper.set(obj)
            this.emit('move-made', obj);
            return result
        }, game)
    }

    async gameState(id: string): Promise<StatusDto | null> {
        const redisHelper = await RedisConnector.getInstance()
        const game = await redisHelper.get(id)
        return this.maybe( obj => {
            // eslint-disable-next-line  @typescript-eslint/no-explicit-any
            const gameObj: any = {}

            obj.gameState().boardMap.forEach((value, key) => {
                gameObj[key] = value.id
            });
            return {
                status: obj.gameState().status,
                players: obj.gameState().players.map(player => {
                    return {
                        order: player.id,
                        name: player.name,
                        accepted: !!player.externalPlayerHash || !!player.attachedCompute
                    }
                }),
                currentPlayer: obj.gameState().currentPlayer,
                finalResult: obj.gameState().finalResult,
                boardMap: gameObj
            }
        }, game)
    }

    private maybe<Nullish, Result>(
        operation: (obj: Nullish) => Result, obj?: Nullish | void): Result | null {

        if (obj) {
            return operation(obj)
        }
        return null
    }

    public static get(): GameService {
        if (_service == undefined) {
            _service = new GameService()
        }
        return _service
    }
}