# 15. Use browser generated tokens for auth

Date: 2022-10-19

## Status

2022-10-19 proposed

## Context

The introduction of an 'invite' game mode, where the game state is now distributed to multiple clients, requires
the invocation of a registration step in order for these clients to indicate they're wish to join and register their
identity.

As this is a distributed system, each client needs to uniquely identify themselves for this registration process, but
adding a user login mechanism would introduce a mundane step for users as well as increasing costs and storage demands
for the TicTacTotem service infrastructure.

## Decision

Generate UUID for registration id in the browser

Store in local cache

Store user name locally as well, to pass during registration step.

Allow for future storage provision to track multiple games (surface through lobby?)

## Consequences

Consequences here...
