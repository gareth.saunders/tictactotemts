export interface RegistrationDto {
    gameId: string,
    awaiting: {
        order: number,
        name: string,
        accepted: boolean
    }[],
    links: {
        syncMoveLink: string,
        moveLink: string,
        statusLink: string
    }
}
