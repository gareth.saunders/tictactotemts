import {menuData} from "../../states/menu-data.js";

export default {
  data() {
    return menuData
  },
  template: ` 
    <div>
        <div v-if="awaitingPlayers.length === 0">
            <p>Oh Dear!</p>
            <div class="spacer"></div>
            <p>There isn't a game happening at the moment, please start a new one or wait for an invite.</p>
        </div>
        
        <div v-if="awaitingPlayers.length > 0">
          <div>
            <button @click="shareRegistrationLink()">Share Link</button>
            <button @click="shareRegistrationLink(true)">Copy Link</button>
          </div>
          <div class="grid-container" v-for="player in awaitingPlayers">
              <div class="grid-item">
                <img v-show="player.order === 1" src="assets/images/player-cross.png">
                <img v-show="player.order === 2" src="assets/images/player-circle.png">
                <img v-show="player.order === 3" src="assets/images/player-diamond.png">
              </div>
              <div>
                <p>
                  <span>{{player.name}}</span>
                </p>
              </div>
              <div>
                <p>
                  <span v-show="player.accepted">Ready</span>
                  <span v-show="!player.accepted">Pending</span>
                </p>
              </div>
          </div>
          <div>
              <button :disabled="awaitingPlayers.reduce((p,c) => p.accepted || !c.accepted, false)" @click="startGame()">Start Game</button>
          </div>
        </div>
    </div>
  `
}