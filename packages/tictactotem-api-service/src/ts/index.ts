import express from "express";
import cors from 'cors'
import {Server} from "typescript-rest";
import {HotseatController} from "./controllers/HotseatController";
import {HealthController} from "./controllers/HealthController";
import {RedisConnector} from "./connectors/RedisConnector";
import {toNumber} from "./Utils";
import {InviteController} from "./controllers/InviteController";
import {WebSocket} from "ws";
import {GameService} from "./services/GameService";
import {GameBoard} from "../../../tictactotem-engine/src/ts/GameBoard";

const app: express.Application = express();
app.set('port', process.env.PORT || 3001);
app.set('ws-port', process.env.WS_PORT || 3002);

const corsOptions = {
    origin: true,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    methods: ['GET', 'PUT', 'POST']
}
app.use(cors(corsOptions));

// Helper conf
RedisConnector.getInstance({
    url: process.env.REDIS_URL,
    backoffMultiplier: toNumber(process.env.REDIS_BACKOFF_MULTIPLIER),
    backoffIncreaseLimit: toNumber(process.env.REDIS_BACKOFF_INCREASE_LIMIT)
})

Server.buildServices(app, HotseatController, InviteController, HealthController);
export const server = app.listen(app.get('port'), function() {
    console.log(`Rest Server listening on port ${app.get('port')}!`)
});

export const websocketServer = new WebSocket.Server({
    port: app.get('ws-port')
}, function() {
    console.log(`WebSocket Server listening on port ${app.get('ws-port')}!`)
});
websocketServer.on("connection", (webSocketClient) => {
    let registrantId = ""
    webSocketClient.send("server-connected")
    GameService.get().on("new-registration", (gameBoard: GameBoard) => {
        if (gameBoard.isPlayerRegistered(registrantId)) {
            webSocketClient.send(`new-registration`)
        }
    })
    GameService.get().on("move-made", (gameBoard: GameBoard) => {
        if (gameBoard.isPlayerRegistered(registrantId)) {
            webSocketClient.send(`move-made`)
        }
    })
    webSocketClient.on('message', (data) => {
       registrantId = data.toString()
    })
});

