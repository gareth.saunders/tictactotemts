# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.2.0...tictactotem-api-service@1.3.0) (2022-11-21)


### Features

* Introduce invite mode to UI ([ff01e7e](https://gitlab.com/gareth.saunders/tictactotemts/commit/ff01e7ef839e8081c876a5d9e64b7b5f45e3a2d2))



## [1.2.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.1.3...tictactotem-api-service@1.2.0) (2022-10-19)


### Features

* Add backend functionality and endpoint for invite-based games ([04c7b4c](https://gitlab.com/gareth.saunders/tictactotemts/commit/04c7b4c7c3d68ddfe6186ce486bf69d29304a7fa))



### [1.1.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.1.2...tictactotem-api-service@1.1.3) (2022-08-18)


### Bug Fixes

* Security and functional improvements to the API health endpoint ([0a28954](https://gitlab.com/gareth.saunders/tictactotemts/commit/0a289549a7a924ac6eaedbad8cd2fe5e767e9227))



### [1.1.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.1.1...tictactotem-api-service@1.1.2) (2022-07-28)


### Bug Fixes

* Correct Redis address and simplify RProbe for API ([4863fac](https://gitlab.com/gareth.saunders/tictactotemts/commit/4863facda584d9e042e586c1fd558d29d0a25c8d))



### [1.1.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.1.0...tictactotem-api-service@1.1.1) (2022-07-27)


### Bug Fixes

* Ensure docker versions are not trailing release versions ([49b731f](https://gitlab.com/gareth.saunders/tictactotemts/commit/49b731fd5aec6b18630ec7969b25541433c6968e))



## [1.1.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.6...tictactotem-api-service@1.1.0) (2022-07-26)


### Features

* Add Redis shared state support to the API service ([16f2609](https://gitlab.com/gareth.saunders/tictactotemts/commit/16f26099936ac1a1db890a0c5cef31af286367ed))



### [1.0.6](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.5...tictactotem-api-service@1.0.6) (2022-04-03)


### Bug Fixes

* Add rolling update support (with health checks), and seperate live and preview pipeline steps ([a4d0687](https://gitlab.com/gareth.saunders/tictactotemts/commit/a4d06873732e19ab23fe1e3de2a6f3119f6d9607))



### [1.0.5](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.4...tictactotem-api-service@1.0.5) (2022-01-31)

**Note:** Version bump only for package tictactotem-api-service





### [1.0.4](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.3...tictactotem-api-service@1.0.4) (2022-01-28)

**Note:** Version bump only for package tictactotem-api-service





### [1.0.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.2...tictactotem-api-service@1.0.3) (2022-01-28)

**Note:** Version bump only for package tictactotem-api-service





### [1.0.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@1.0.1...tictactotem-api-service@1.0.2) (2022-01-26)


### Bug Fixes

* **build:** Add general root build step, enable pipelines for root changes ([bd45bb5](https://gitlab.com/gareth.saunders/tictactotemts/commit/bd45bb52093de5dd49e9e6f0bc0fe0faadc0cdd2))



### [1.0.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-api-service@0.5.0...tictactotem-api-service@1.0.1) (2022-01-21)


### Bug Fixes

* Manual bump to one-aught (leave pre-release) ([17fb56d](https://gitlab.com/gareth.saunders/tictactotemts/commit/17fb56d4b187e716ceb80dedd87fffe66674f41a))



## 0.5.0 (2022-01-21)


### Bug Fixes

* Correct API docker build to use correct context ([6188396](https://gitlab.com/gareth.saunders/tictactotemts/commit/61883962720d4e704b7a07575a2cdc8bc58d9544))
* Lerna builds to be versioned independently ([a71f7d6](https://gitlab.com/gareth.saunders/tictactotemts/commit/a71f7d68d3223b04874780305f2f429163b14c44))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))

## 0.2.0 (2022-01-06)


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([c3fb156](https://gitlab.com/gareth.saunders/tictactotemts/commit/c3fb1568cee4dcb84dfc4cbd38f6858b11abc8c8))


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* replace missing yarn start for API service ([27eb590](https://gitlab.com/gareth.saunders/tictactotemts/commit/27eb5906ef946fbcaf430eb890ca6769dd54962b))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Undo revertion of api url from past bad merge ([7cf922f](https://gitlab.com/gareth.saunders/tictactotemts/commit/7cf922f9f61ff2f4d44a88d89d5806ca3b36d80c))



# 0.4.0 (2022-01-13)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct API docker build to use correct context ([6188396](https://gitlab.com/gareth.saunders/tictactotemts/commit/61883962720d4e704b7a07575a2cdc8bc58d9544))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* replace missing yarn start for API service ([27eb590](https://gitlab.com/gareth.saunders/tictactotemts/commit/27eb5906ef946fbcaf430eb890ca6769dd54962b))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Undo revertion of api url from past bad merge ([7cf922f](https://gitlab.com/gareth.saunders/tictactotemts/commit/7cf922f9f61ff2f4d44a88d89d5806ca3b36d80c))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([c3fb156](https://gitlab.com/gareth.saunders/tictactotemts/commit/c3fb1568cee4dcb84dfc4cbd38f6858b11abc8c8))





# 0.3.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* replace missing yarn start for API service ([27eb590](https://gitlab.com/gareth.saunders/tictactotemts/commit/27eb5906ef946fbcaf430eb890ca6769dd54962b))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Undo revertion of api url from past bad merge ([7cf922f](https://gitlab.com/gareth.saunders/tictactotemts/commit/7cf922f9f61ff2f4d44a88d89d5806ca3b36d80c))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([c3fb156](https://gitlab.com/gareth.saunders/tictactotemts/commit/c3fb1568cee4dcb84dfc4cbd38f6858b11abc8c8))





# 0.2.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* replace missing yarn start for API service ([27eb590](https://gitlab.com/gareth.saunders/tictactotemts/commit/27eb5906ef946fbcaf430eb890ca6769dd54962b))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Undo revertion of api url from past bad merge ([7cf922f](https://gitlab.com/gareth.saunders/tictactotemts/commit/7cf922f9f61ff2f4d44a88d89d5806ca3b36d80c))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([c3fb156](https://gitlab.com/gareth.saunders/tictactotemts/commit/c3fb1568cee4dcb84dfc4cbd38f6858b11abc8c8))
