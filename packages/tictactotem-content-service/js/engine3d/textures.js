import * as THREE from 'https://cdn.skypack.dev/three@0.129.0';

export let frameworkTexture = createFrameworkTexture();
export let toonTexture = createToonTexture();

const backgroundMap = new Map()

function createFrameworkTexture() {
    const loader = new THREE.TextureLoader();
    return loader.load('assets/textures/FrameworkMap.jpg');
}

function createToonTexture() {
    const loader = new THREE.TextureLoader();
    const texture = loader.load('assets/textures/twotone.jpg');
    return texture
}

export function createBackgroundTexture(filename, rotationDenominator, xRepetition, yRepetition) {
    if( backgroundMap.has(filename) ) {
        return backgroundMap.get(filename)
    }

    const loader = new THREE.TextureLoader();
    const texture = loader.load(
        `assets/textures/${filename}.jpg`);

    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    if (xRepetition && yRepetition) {
        texture.repeat.set( xRepetition, yRepetition );
    }
    if (rotationDenominator) {
        texture.rotation = Math.PI / rotationDenominator
    }

    backgroundMap.set(filename, texture)
    return texture
}
