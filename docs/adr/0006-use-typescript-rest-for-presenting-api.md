# 6. Use 'typescript-rest' for presenting API

Date: 2021-06-06

## Status

Accepted

Builds on [5. Seperate API and content services](0005-seperate-api-and-content-services.md)

## Context

A choice is required at this stage for a Rest Service framework.

Requirements:
 - Clear implementation syntax
 - Typescript integration
 - Usual NFR requirements (Speed, Stability, Security, etc)
 - Swagger support or similar, if possible, for future contract tests & programmatic API exposure.
 
## Decision

Future development of the API will be done using [typescript-rest](https://d0whc3r.github.io/typescript-rest); it's built as a Typescript wrapper around express.js atop node.js, both well established frameworks that should go someway to satisfy NFRs.

Beyond this, it has been implemented in a very Typescript-aware way, with good use of types and classes to define resource structures and endpoints. Moreover, this has native Swagger generation support.

## Consequences

- Tests should be easier going forward.
- Code will be cleaner, especial given the Typescript focus; this will equate to improved maintainability. 
- There are some newer technologies used (ES7 decorators), which while make for cleaner syntax, _are_ experimental.