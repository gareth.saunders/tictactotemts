import {buildData} from "../states/build-data.js";

export default {
    data() {
        return buildData
    },
    template: ` 
    <div id="system-notifier">
        {{releaseIdentifier()}}
    </div>
  `
}