import {WinConditionManager} from "./WinConditions";
import {
    CylindricalKeyHash,
    cylindricalKeyHash,
    WinConditionType,
    Player
} from "./BasicEntities";
import {WinResult} from "./ResultEntites";

const dimensions = {
    segment: 4,
    radius: 2,
    depth: 4
}

describe('condition detection', () => {
    test('should detect a clean circle win condition', () => {
        //Given
        const gameMap = new Map<CylindricalKeyHash, Player>()
        const onlyPlayer = {
            id: 1,
            name: ""
        }

        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 1,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 2,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 3,
            radius: 1,
            depth: 2
        }), onlyPlayer)

        //When
        const result = new WinConditionManager(dimensions).testForCondition(gameMap)

        expect(result).toBeDefined()

        const expectedResult = result as WinResult
        expect(expectedResult.winningPlayer).toBe(onlyPlayer)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)

    })

    test('should detect a dirty circle win condition', () => {
        //Given
        const gameMap = new Map<CylindricalKeyHash, Player>()
        const onlyPlayer = {
            id: 1,
            name: ""
        }
        const otherPlayer = {
            id: 1,
            name: ""
        }

        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 2
        }), otherPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 1,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 1
        }), otherPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 2,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 3
        }), otherPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 3,
            radius: 1,
            depth: 2
        }), onlyPlayer)

        //When
        const result = new WinConditionManager(dimensions).testForCondition(gameMap)

        expect(result).toBeDefined()

        const expectedResult = result as WinResult
        expect(expectedResult.winningPlayer).toBe(onlyPlayer)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)

    })

    test('should fail to detect non-existent win condition', () => {
        //Given
        const gameMap = new Map<CylindricalKeyHash, Player>()
        const onlyPlayer = {
            id: 1,
            name: ""
        }
        const otherPlayer = {
            id: 1,
            name: ""
        }

        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 2
        }), otherPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 1,
            radius: 1,
            depth: 2
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 1
        }), otherPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 2,
            radius: 1,
            depth: 0
        }), onlyPlayer)
        gameMap.set(cylindricalKeyHash({
            segment: 0,
            radius: 2,
            depth: 3
        }), otherPlayer)

        //When
        const result = new WinConditionManager(dimensions).testForCondition(gameMap)

        expect(result).toBeUndefined()
    })
})

describe('condition generators', () => {

    test('should generate a circle condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.CIRCLE, {
            segment: 0,
            radius: 1,
            depth: 2
        }, dimensions)
        expect(result.length).toBe(4)
        expect(result[0]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 2
        })
        expect(result[1]).toStrictEqual({
            segment: 1,
            radius: 1,
            depth: 2
        })
        expect(result[2]).toStrictEqual({
            segment: 2,
            radius: 1,
            depth: 2
        })
        expect(result[3]).toStrictEqual({
            segment: 3,
            radius: 1,
            depth: 2
        })
    })

    test('should generate a thruline condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.THRU_LINE, {
            segment: 2,
            radius: 0,
            depth: 3
        }, dimensions)
        expect(result[1]).toStrictEqual({
            segment: 2,
            radius: 0,
            depth: 3
        })
        expect(result[2]).toStrictEqual({
            segment: 0,
            radius: 0,
            depth: 3
        })
        expect(result[3]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 3
        })
    })

    test('should generate a dropline condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.DROP_LINE, {
            segment: 0,
            radius: 1,
            depth: 2
        }, dimensions)
        expect(result.length).toBe(4)
        expect(result[0]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 0
        })
        expect(result[1]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 1
        })
        expect(result[2]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 2
        })
        expect(result[3]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 3
        })
    })

    test('should generate a diagonal line condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.DIAGONAL_LINE, {
            segment: 2,
            radius: 0,
            depth: 0
        }, dimensions)
        expect(result.length).toBe(4)
        expect(result[0]).toStrictEqual({
            segment: 2,
            radius: 1,
            depth: 0
        })
        expect(result[1]).toStrictEqual({
            segment: 2,
            radius: 0,
            depth: 1
        })
        expect(result[2]).toStrictEqual({
            segment: 0,
            radius: 0,
            depth: 2
        })
        expect(result[3]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 3
        })
    })

    test('should generate a clockwise spiral condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.CLOCKWISE_SPIRAL, {
            segment: 2,
            radius: 1,
            depth: 0
        }, dimensions)
        expect(result.length).toBe(4)
        expect(result[0]).toStrictEqual({
            segment: 2,
            radius: 1,
            depth: 0
        })
        expect(result[1]).toStrictEqual({
            segment: 3,
            radius: 1,
            depth: 1
        })
        expect(result[2]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 2
        })
        expect(result[3]).toStrictEqual({
            segment: 1,
            radius: 1,
            depth: 3
        })
    })

    test('should generate a anti-clockwise spiral condition', () => {
        const result = WinConditionManager.generateCondition(WinConditionType.ANTI_CLOCKWISE_SPIRAL, {
            segment: 2,
            radius: 1,
            depth: 0
        }, dimensions)
        expect(result.length).toBe(4)
        expect(result[0]).toStrictEqual({
            segment: 2,
            radius: 1,
            depth: 0
        })
        expect(result[1]).toStrictEqual({
            segment: 1,
            radius: 1,
            depth: 1
        })
        expect(result[2]).toStrictEqual({
            segment: 0,
            radius: 1,
            depth: 2
        })
        expect(result[3]).toStrictEqual({
            segment: 3,
            radius: 1,
            depth: 3
        })
    })
})