import {menuData} from "../states/menu-data.js";

export default {
  data() {
    return menuData
  },
  template: `
    <div id="toggle-menu" :disabled="!gameStarted" v-on:click="menuActive = gameStarted ? !menuActive : menuActive">
    </div>
  `
}