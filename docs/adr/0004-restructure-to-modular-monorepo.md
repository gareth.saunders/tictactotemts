# 4. Restructure to modular monorepo

Date: 2021-05-25

## Status

Accepted

Built upon by [5. seperate-api-and-content-services](0005-seperate-api-and-content-services.md)

Built upon by [10. Direct imports to be used between internal libraries and services](0010-direct-imports-to-be-used-between-internal-libraries-and-services.md)

## Context

Now that the main game engine is functionally complete, the next step is to build a service to wrap it, and a front-end to play it. 

These should be considered different artifacts; they occupy differing layers, deployments will be easier to manage, and the core game engine can still be used in other, external projects (should the need arise).

Multiple repositories can turn into maintenance overhead; given the present nature of this project (a proving ground), such a course would be overkill. This suggests that the best approach would be a Monorepo.

* http://danluu.com/monorepo/
* https://en.wikipedia.org/wiki/Monorepo

This would also be a good opportunity to explore the Monorepo concept, as there appear to be a fair number of proponents and opponents for a rather new idea - below are two good competing views (from many), as well as one more slightly hysterical one (?) from the opposition:

* https://blog.nrwl.io/misconceptions-about-monorepos-monorepo-monolith-df1250d4b03c
* https://alexey-soshin.medium.com/monorepo-is-a-bad-idea-5e587e848a07
* https://medium.com/@mattklein123/monorepos-please-dont-e9a279be011b

## Decision

The current repository contents will be moved to a new _engine_ package, and to do this we will leverage [Yarn Modules](https://classic.yarnpkg.com/en/docs/workspaces/), as well as (the recommended) [Lerna](https://lerna.js.org) to manage this as a Monorepo going forward. A useful guide to all the available Yarn stack "moduling" mechanisms can be found [here](https://doppelmutzi.github.io/monorepo-lerna-yarn-workspaces/).  

Also required is an update to the GitLab CICD pipeline - a very useful guide can be found [here](https://darekkay.com/blog/gitlab-ci-monorepo-config/)

## Consequences

* Better organisation between the various components of TicTacTotem.
* The ability to cleanly deploy the game engine artifact in projects other than this.
* Changes to one package will only cause build overhead in that package (if done correctly).
* Slight overhead increase in repo structure.
