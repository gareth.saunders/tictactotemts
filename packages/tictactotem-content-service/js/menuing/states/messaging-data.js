import { reactive } from 'https://cdn.jsdelivr.net/npm/vue@3.2.41/dist/vue.esm-browser.min.js'
import {menuData} from "./menu-data.js";

export const messagingData = reactive({
    currentMessage: "",
    logAndInform(error, message) {
        this.currentMessage = message
        console.log(error)
        menuData.uiLock = false
    }
})
