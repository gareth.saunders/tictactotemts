import {CylindricalKey, Player, WinCondition} from "./BasicEntities";

export interface GameResult {
    status: GameStatus
}

export enum GameStatus {
    REGISTRATION = "registration",
    PLAYING = "playing",
    WIN = "win",
    STALEMATE = "stalemate"
}

export interface TurnResult extends GameResult {
    moveAccepted: boolean
    acceptedMove?: CylindricalKey
    nextPlayer: Player
}

export type GameEndResult = WinResult | StalemateResult

export type StalemateResult = GameResult

export interface WinResult extends GameResult {
    winConditionAcheived: WinCondition
    winningPath: CylindricalKey[]
    winningPlayer: Player
}

export interface WaitingResult extends GameResult {
    awaitingPlayers: Player[]
    registrationResult: RegistrationResult
}

export enum RegistrationResult {
    RegistrationSuccessful = "registration-successful",
    AlreadyRegistered = "already-registered",
    NoMoreRegistrationSlots = "no-more-registration-slots"
}