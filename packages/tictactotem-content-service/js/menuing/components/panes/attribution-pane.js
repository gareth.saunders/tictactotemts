export default {
  template: ` 
    <div id="info-attribution">
        <p>An (over-engineered) open-source game.</p>
        <p><a href="https://gitlab.com/gareth.saunders/tictactotemts">Source available under Apache License Version 2.0</a>.</p>
        <div class="spacer"></div>
        <p>Thanks to <a href="https://www.arjowiggins.com/">Arjowiggins</a> for use of their Inuit typeface, © Arjowiggins 2006.</p>
        <p>The dark mode background is provided Phillipe Donn via the open-licence-friendly <a href="https://www.pexels.com/photo/milky-way-illustration-1169754">Pexels service</a></p>
        <p>Also, this uses the Inconsolata typeface by Raph Levien, courtesy of <a href="https://fonts.google.com/specimen/Inconsolata">Google Fonts</a> using the <a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL">Open Font Licence</a>.</p>
        <div class="spacer"></div>
        <p>© Gareth David Saunders, 2021</p>
    </div>
  `
}