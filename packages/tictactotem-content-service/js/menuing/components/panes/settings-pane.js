import {menuData} from "../../states/menu-data.js";

export default {
  data() {
    return menuData
  },
  watch: {
    playerName(newName) {
      const playerSettings = JSON.stringify({
        version: 1,
        playerName: newName,
        darkMode: menuData.darkMode
      });
      window.localStorage.setItem("player-settings", playerSettings);
    },
    darkMode(newMode) {
      const playerSettings = JSON.stringify({
        version: 1,
        playerName: menuData.playerName,
        darkMode: newMode
      });
      window.localStorage.setItem("player-settings", playerSettings);
    }
  },
  template: ` 
    <div>
        <p>Player Name</p>
        <div class="any-group">
            <input type="text" v-model="playerName"/>
            <button @click="reassignPlayerName()">Lucky Dip!</button>
        </div>
        <p>Style</p>
        <div class="btn-group">
            <button v-on:click="toggle('darkMode', true)" v-bind:class="{ active: isSelected('darkMode', true) }">Dark</button>
            <button v-on:click="toggle('darkMode', false)" v-bind:class="{ active: isSelected('darkMode', false) }">Light</button>
        </div>

    </div>
  `
}