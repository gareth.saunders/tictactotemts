# 11. Use Kubernetes namespaces to define environment boundaries

Date: 2022-02-18

## Status

Accepted

## Context

As TicTacTotem (_T4M_) approached the MVD milestone, it becomes necessary to have a minimum of one separate preview environment for staging changes prior to the domain-root-hosted live environment.

However, this is a personal project, so funds are limited to the one minimal cluster dedicated to hosting this project. This _de-facto_ prevents the option of using hard-seperated environments, considered to be the industry-standard approach.

## Decision

We are going to leverage **namespaces** within the Kubernetes cluster to provide a sufficient degree of isolation between environments. This will be supported by the built-in **Kustomize** namespace-overriding mechanism to provide streamlined paths to deployment.

Kubernetes namespaces [(official docs)](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) provide full communication isolation for most types of object that will concern us; there are a number that are cluster-wide, so far the only one identified as a potential concern are Persistent Volumes. It can also be bypassed with services that lookup objects with fully-qualified names, but this risk can be mitigated with rules and a good review process.

This is not a purely financial decision - it is also an experiment as to whether a single CI/CD cluster can be sub-divided internally and maintained as such, without the risk of environmental bleeding becoming too great.

Furthermore, this change now allows the deployment of a holding service until such time that there is a deployment of the game that can be considered Production-worthy.

## Consequences

- There are now two environments for _T4M_, Live (root) and Preview.
- Live can have a less-embarrassing holding page until _T4M_ has reached a certain standard, and passed MVD.
- Namespaces will not provide perfect isolation between environment - ingress control and certificate issuance are shared, and Kubernetes external service links could be used to tunnel between the environments.
  - Shared ingress might yet prove to be a net benefit, avoiding dependence on external route management.
- By keeping it within the cluster, we can continue to avoid deployment of resources using more generalised deployment tools such as Terraform.
- Further environments are possible, including side ones for NFR functions like auth and monitoring.