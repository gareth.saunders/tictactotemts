# 12. Enable pod replication using Redis

Date: 2022-04-13

## Status

Accepted

## Context

Recent changes involving rolling deployments has shown that having replication support in pods would be beneficial, even if the need to scale is not yet present.

Furthermore, it would be good to introduce game persistence, for both short-term continuation of ongoing games (i.e. across deployments and outages), as well as factoring in support for future functionality such as historical game playback.  

## Decision

Redis is to be used to provide shared game state between (otherwise stateless) API pods, replacing the in-memory state map. 

Memcached was considered, but Redis has built-in support for persistence, as well as advanced type support that could be useful later.

Given the current use-case, key-store memory caches are preferable over a full DB-esque store.

## Consequences

- Services should be scalable from here on out
- Persistence is now available, provided it can fit in allocated memory
  - Said datastore is not queryable like a DBMS
  - Data retrieval will be very performant
- Cluster now has non-negligible resource requirement from here on in
