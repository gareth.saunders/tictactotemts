# 3. Use DI to ensure testability of automated player end moves

Date: 2021-05-21

## Status

Accepted

## Context

Currently, testing of computer players is hampered by first moves and tie-breaks of the highest-ranked moves are hard-wired random choices; final moves cannot be decided using any informed means.  

## Decision

Use Dependency Injection to wire in the ideal tie-breaker mechanism for a shortlist of equally-valid moves; this mechanism should be universal for any automated player functionality.

By using DI for end-selection resolution, it is possible to increase predictability for tests that would otherwise be impossible to determine the outcome, whilst still allowing for random play in real-world situations (predictability is not so good for actual play).

This will allow for a near-complete suite of tests for the computer player functionality.

## Consequences

- Adds a little more indirection (and thus complexity) to the CPlayer mechanisms.
- Makes testing possible for some cases in which randomisation would interfere, and a lot easier to grok as well.
