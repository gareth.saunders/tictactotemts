# 8. Use Vue.js for Frontend Menus

Date: 2021-11-11

## Status

Accepted

Builds on [7. Try ThreeJS as a MDP for Web FrontEnd](0007-try-threejs-as-a-mdp-for-web-frontend.md)

## Context

Secondary UI elements have proven to be more difficult to implement in Three.js than hoped. Also, testing has been an issue at a conceptual level.

There are a number of proven frameworks out there than can hopefully be used on top of this current implementation instead.

## Decision

After reviewing a number of potential front-end frameworks to drive the game overlays, the one to be used is [Vue.js](https://vuejs.org/). 

Angular and React were eliminated as they are built to be the central framework, whereas the UI has been built around a loose ES6 modular structure. The former is also very opinionated.

Knockout bindings to not enforce or encourage any overarching structure, but it does not presently support import as an ES6 import; again, this is a requirement by the current client-first structure.

Mithril was explored, but its approach to managing a VDOM (by being it) would be quite a departure.

Vue.js was adopted as it is a simple drop-in for MVVM bindings, with minimal changes to the current ES6 + three.js implementation. There are no server-side integration requirements and no complications from opinionated design rails. It's also well regarded, mature and has good test support; better support again if componentisation is adopted in the future.

## Consequences

- Separating into models and view models allows for unit-testing the behaviour.
  - Sharing some of this data with Three.js has led to refactors that lend to this being true for some of that aspect, too.
  - Although I've avoided implementation so far to get an MVD out (with lack of sufficient knowlegde in this area), there is otherwise no further excuse. 
- If deemed worth it, allows separation of the UI into components; this will enhance readability, maintainability and testiblity. 
- Might be wrong about performance impacts of overlays in the future (some stack overflow comments have alluded to this).
- Implementing Vue has been a pleasure, always nice to find in a new framework.