import * as THREE from 'https://cdn.skypack.dev/three@0.129.0';
import {frameworkTexture, toonTexture} from "./textures.js";

export let genericMaterial = new THREE.MeshPhongMaterial({
    alphaMap: frameworkTexture,
    color: 0x333333,
    transparent: true,
    emissive: 0xffffff,
    emissiveMap: frameworkTexture,
    emissiveIntensity: 0.5
})

export let playerMaterial = (colour) => {
    const material = new THREE.MeshToonMaterial({
        color: colour,
        gradientMap: toonTexture,
        emissive: 0xffffff,
        emissiveIntensity: 1
    })
    return material
}

let selectorMaterial = () => {
    return new THREE.MeshToonMaterial({
        color: 0xffffc8,
        gradientMap: toonTexture
    })
}

export let baseMaterial = createBaseMaterial();
export let hoverMaterial = createHoverMaterial();
export let selectedMaterial = createSelectedMaterial();
export let chosenMaterialP1 = createChosenMaterial(0xc8c8ff);
export let chosenMaterialP2 = createChosenMaterial(0xffc8c8);
export let chosenMaterialP3 = createChosenMaterial(0xc8ffc8);

function createBaseMaterial() {
    let mat = selectorMaterial()
    return mat;
}

function createHoverMaterial() {
    let mat = selectorMaterial()
    mat.color.r = 1;
    mat.color.g = 1;
    mat.color.b = 1;
    return mat;
}

function createSelectedMaterial() {
    let mat = selectorMaterial()
    mat.color.b = 1;
    return mat;
}

function createChosenMaterial(colour) {
    let mat = selectorMaterial()
    mat.opacity = 0.1;
    mat.side = THREE.BackSide;
    mat.color.set(colour)
    return mat;
}