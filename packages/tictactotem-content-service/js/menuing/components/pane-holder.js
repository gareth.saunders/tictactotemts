import {viewData} from "../states/view-data.js";

export default {
    props: {
        title: String
    },
    data() {
        return viewData
    },
    template: ` 
        <div id="holder">    
            <transition name="fade" mode="out-in">
                <p v-show="title !== ''" class="tab-title">{{title}}</p>
            </transition>   
            
            <transition name="fade" mode="out-in">
                <div class="btn-group breadcrumb" v-if="currentView() != undefined">
                    <button  v-for="nameObj in pastViewNames()" v-on:click="pullView(nameObj.step)">&lt; {{nameObj.name}}</button>
                </div>
            </transition>
            
            <transition name="pane" mode="out-in">
                <slot></slot>
            </transition>
        </div>
    `
}