= TicTacTotem Client & Content Service

This package is a demonstration of how the TicTacTotem game should actually look and play. It uses https://threejs.org[Three.js] to provide an environment capable of presenting the cylindrical interface required to correctly and simply visualise the twisting nature of the moves used to compete.

The client is entirely browser-based; although the logic is off-device, the entirety of the client is served via static files using ES6 modules and a simple content server.

== Quick Start Guide (Service Only)

CAUTION: This section describes a pre-release interface, which is neither yet focussed nor stable.

NOTE: If one wishes to run the whole project locally, read the link:../../readme.adoc#_running_locally[root readme section].

=== Local client, for remote server

Settings for the remote host can be overridden by changing the `host:` field in ./settings/environmental-settings.js - this is the mechanism used by the local dev stack and the cluster environments.
