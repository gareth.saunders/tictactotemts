# 9. Enforce conventional commits to automate semver

Date: 2021-12-18

## Status

Accepted

## Context

Recent fixes to the build pipeline now ensure that pushes to the master branch result in generation of artifacts; to facilitate this, the engine also presently performs a patch bump before publishing to NPM  (a requirement of the repository).

As this project already has a "tradition" of using [conventional commits](https://www.conventionalcommits.org) in the gitlog, might as well go all-in and automate SemVer bumps using a tool that can interpret those messages. This will need to be be implemented for all packages, for it to have the desired effect of full automation.

## Decision

All packages are to independently implement [standard-version](https://github.com/conventional-changelog/standard-version), an NPM tool that reads the Gitlog for the commit, and automates the bump; this is to be committed to master post-PR-merge in a way similar to the current dumb patch-bump process used with the engine.

To ensure that this process is side-stepped, extra validation of the commit messages will be enforced by [commitlint](https://commitlint.js.org/#/).

## Consequences

* The only cognitive overhead related to versioning is around the commit prefix - this should be apparent from the nature of the submitted change.
* Multiple package updates will have independent _master_ bumps if updated the same time, as with the planned style of implementation.
* The standard-version tool also gives us a free CHANGELOG - bonus.
