# 14. Introduce tech debt log to repository

Date: 2022-10-11

## Status

2022-10-11 proposed

## Context

In order to progress the project within a number of unfamiliar contexts (compounded by a change in project goal), a number 
of non-functional aspects have been deliberately overlooked. Some have been resolved over time, but the list still 
includes the following:

 - Any testing at the client, notably UI tests for the game itself (how can this be done effectively?)
 - Dependency injection at the API service (unsure which technology to use, but prevents London-style unit testing)

It is important that these are tracked as technical debt, but GitLab issue tracking:

 - is still somewhat lacking compared to similar services (Jira, etc)
 - is often overlooked, as not part of process yet (sole developer at time of writing)
 - is not portable, should the project be migrated at any point

Thus, a better solution would be to hold the Technical Debt Log as part of the repository itself...

## Decision

A new file is to be created (tech-debt-log.yaml) that is to be used to encode known technical debt until a better 
tracking solution can be implemented. 

To open the possibility for future automation, it will use the following YAML structure to record known technical debt:

```yaml
%YAML 1.1
---
$schema: "http://stsci.edu/schemas/yaml-schema/draft-01"
id: "https://tictactotem.com/schemas/tech-debt/log-1.0.0"
tag: "tag:tictactotem.com:tech-debt/log-1.0.0"

title: |
  Technical debt log definition
description: |
  A list of identified technical debt items to track and encode known technical debt until a better 
  tracking solution can be implemented. This list has been created to straddle the needs of both human 
  and (future) automated parsing.
#examples:
  
type: object
properties:
  items:
    type: array
    items: 
      type: object
      description: |
        List of Technical Debt items
      properties:
        outline:
          type: string
          description: |
            An brief one-line outline of the technical debt that has been left in-place, and raised here.
          maxLength: 120
        scope:
          type: string
          description: |
            Relative path to the repository space affected by this debt item.
            This can be the whole repository ("/"), a sub-directory, file or even a span of lines in a file.
            
            e.g. / [root]
                 /dir/sub-dir/ [directory]
                 /dir/sub-dir/file.ext [file]
                 /dir/sub-dir/file.ext:20 [line in file]
                 /dir/sub-dir/file.ext:20-24 [line-span in file]
            
          pattern: ^(/[\w-\.]+)+/?(:\d+)?(-\d+)?$
        date:
          type: string
          description: |
            [Optional] Date of debt item was recording. 
            
            Whilst the date is implicitly recorded by the commit, this provides a convenient lookup for 
            triaging and a means of backdating an item if appropriate.
          pattern: ^\d{4}-\d{2}-\d{2}$
        tags:
          type: array
          description: |
            Tags for providing quasi-fixed metadata for express categorisation of the debt item.
            
            Presently, this is defined in a two-part group structure with the following format:
            
            group::tag
            
            Note: this is very likely to be iterated on as a concept as the log definition is refined.
          items:
            type: string
            pattern: ^[\w-]+::[\w-]+$
          uniqueItems: true
        reasons:
          type: array
          description: |
            A set of brief single-line reasons for why the technical debt has been left in place,
            rather than resolved as part of the current work package.
            
            Such reasons may take the form of technical explanations, arguments, mitigations or statements 
            of extenuating circumstances, but must be short; further details should be captured as part 
            of the context.
          items:
            type: string
            maxLength: 120
          minItems: 1
        context:
          type: string
          description: |
            This section is where any background information is recorded, that is important for determining 
            how to assess, mitigate and deal with this debt item.
            
            Such information can and should include:
             - The history of the work around which the debt has formed
             - The circumstances which lead to the accumulation of the debt
             - How the debt was recognised
             - What needs to be addressed in order to remove the debt
            
            Feel free to record as much detail in this field as is needed.
            
            Note: the list above is not yet exhaustive.
      required: [outline, scope, tags, reasons, context]

required: [items]
additionalProperties: false 
```

This is not a perfect solution, as it does not include any form of automation or provision of monitoring; future 
improvement and automation of logs will be recorded as tech debt itself.

## Consequences

- Tech debt is now logged in a structured format, categorised and not forgotten
- Tech debt is held as part of the repository, with no external dependency
- Logging is currently a manual process, but has scope for future automation and monitoring
- Solution ignores potential pre-existing alternatives at present (future re-evaluation)
