import * as redis from 'redis';
import {ReplyError, RedisError} from 'redis-errors';
import {Errors} from "typescript-rest";
import {GameBoard} from "../../../../tictactotem-engine/src/ts/GameBoard";
import {GoneError} from "typescript-rest/dist/server/model/errors";
import {sleep} from "../Utils";

export class RedisConnector {
    private static _instance: RedisConnector

    private redisClient: redis.RedisClientType
    private redisUp = false
    private _currentBackoffPeriod = 1
    private options: RedisConnectorOptions

    constructor(options: RedisConnectorOptions) {
        this.options = options

        this.redisClient = redis.createClient({
            url: this.options.url ?? "redis://tictactotemts-redis-1:6379"
        })

        this.redisClient.on('error', (err) => {
            this.redisUp = false
            console.log('Cache connection has failed for error: ', err)
        });
    }

    async attemptConnectionWithBackoff(): Promise<void> {
        await sleep(this._currentBackoffPeriod)
        await this.attemptConnection(true)

        if (!this.redisUp) {
            const backoffMultiplier = this.options.backoffMultiplier ?? 10
            const maxBackoffPeriod = backoffMultiplier * (this.options.backoffIncreaseLimit ?? 4)
            if (this._currentBackoffPeriod < maxBackoffPeriod) {
                this._currentBackoffPeriod *= backoffMultiplier
            }
            await this.attemptConnectionWithBackoff()
        } else {
            this._currentBackoffPeriod = 1
        }
    }

    async attemptConnection(suppressExceptions?: boolean): Promise<void> {
        try {
            await this.redisClient.connect()
            console.log("Redis connected")
            this.redisUp = true
        } catch (err) {
            this.redisUp = false
            console.log('Cache connection has failed', err)

            this.attemptConnectionWithBackoff().finally()
            if (!suppressExceptions) {
                throw new GoneError('Cache connection has failed')
            }
        }
    }

    async get(key: string): Promise<GameBoard | void> {
        if (!this.redisUp) {
            await this.attemptConnection()
            if (!this.redisUp) {
                throw new Errors.GoneError("The cache cannot be accessed at present");
            }
        }

        try {
            const boardJson = await this.redisClient.GET(key)
            if (boardJson) {
                return new GameBoard(boardJson)
            }
        } catch (error) {
            const isFromRedis = error instanceof ReplyError

            console.log((error as RedisError).name)
            console.log((error as RedisError).message)
            this.redisUp = isFromRedis
            throw new Errors.GoneError("The game you are looking for cannot be found");
        }
    }

    async set(value: GameBoard): Promise<void> {
        if (!this.redisUp) {
            await this.attemptConnection()
            if (!this.redisUp) {
                throw new Errors.GoneError("The cache cannot be accessed at present");
            }
        }

        try {
            await this.redisClient.SET(value.id, value.persist())
        } catch (error) {
            const isFromRedis = error instanceof ReplyError

            console.log((error as RedisError).name)
            console.log((error as RedisError).message)
            this.redisUp = isFromRedis
            throw new Errors.GoneError("This game cannot be persisted at this time");
        }
    }

    isRedisUp(): boolean {
        return this.redisUp;
    }

    currentBackoffPeriod(): number {
        return this._currentBackoffPeriod;
    }

    static async getInstance(options?: RedisConnectorOptions): Promise<RedisConnector> {
        if (this._instance) {
            return this._instance
        }

        this._instance = new RedisConnector(options ?? {})
        await this._instance.attemptConnection()
        return this._instance
    }
}

export type RedisConnectorOptions = {
    url?: string;
    backoffMultiplier?: number;
    backoffIncreaseLimit?: number;
}
