import { reactive } from 'https://cdn.jsdelivr.net/npm/vue@3.2.41/dist/vue.esm-browser.min.js'
import {messagingData} from "./messaging-data.js";
import {settings} from "../../../settings/environmental-settings.js";
import {generatePlayerName} from "../../utilities/name-generation.js";
import {uuid} from "../../utilities/uuid-generation.js";
import {viewData} from "./view-data.js";

export const getRegistrantId = () => {
    let registrantId = window.localStorage.getItem("registrant-id")
    if (!registrantId) {
        if (crypto.randomUUID) {
            registrantId = crypto.randomUUID();
        } else {
            registrantId = uuid()
        }
        window.localStorage.setItem("registrant-id", registrantId);
    }
    return registrantId
}

export const getRegistrantSettings = () => {
    let playerSettings = window.localStorage.getItem("player-settings")
    if (!playerSettings) {
        playerSettings = JSON.stringify({
            version: 1,
            playerName: generatePlayerName(),
            darkMode: false
        });
        window.localStorage.setItem("player-settings", playerSettings);
    }
    return JSON.parse(playerSettings)
}

export const menuData = reactive({
    menuActive: true,
    gameStarted: false,

    apiRegistrationUrl: null,
    userRegistrationUrl: '',
    statusUrl: null,
    awaitingPlayers: [],

    type: 'hotseat',
    playerTwo: 'computer',
    playerThree: null,
    playerName: getRegistrantSettings().playerName,
    darkMode: getRegistrantSettings().darkMode,

    _currentPlayerDetails: {
        id: 1,
        name: "?",
        isWinner: false,
        isStalemate: false
    },

    currentGameLinks: {
        moveLinkStub: '',
        syncMoveLinkStub: '',
        statusLinkStub: ''
    },

    playerByCoord: new Map(),
    winningCoords: [],
    uiLock: false,
    env: settings,

    menuToggle() {
        menuData.menuActive = !menuData.menuActive;
    },
    toggle(section, item) {
        menuData[section] = item
    },
    isSelected(section, item) {
        return menuData[section] == item
    },
    newGame() {
        const player2 = `?player2type=${menuData.playerTwo}`
        const player3 = menuData.playerThree ? `&player3type=${menuData.playerThree}` : ''
        this.uiLock = true
        fetch(`${this.environment().apiHost}/${this.type}/new${player2}${player3}`, {
            method: 'GET',
            cache: "no-cache",
            headers: {
                'Content-Type': 'application/json',
                'registrant-id': getRegistrantId()
            }
        }).then(
            response => response.json(),
            error => messagingData.logAndInform(error, "Unable to create game")
        ).then(
            result => {
                if (result.moveLink) {
                    this.handleNew(result)
                    this.startGame()
                    return
                }

                this.apiRegistrationUrl = `${this.environment().apiHost}${result.links.apiRegistrationLink}`
                this.userRegistrationUrl = `${this.environment().host}${result.links.userRegistrationLink}`
                this.statusUrl = `${this.environment().apiHost}${result.links.statusLink}`
                this.registerForGame()
                viewData.pushView('lobby')
            }
        )
    },
    registerForGame() {
        this.uiLock = true
        fetch(this.apiRegistrationUrl, {
            method: 'GET',
            cache: "no-cache",
            headers: {
                'Content-Type': 'application/json',
                'registrant-id': getRegistrantId(),
                'registrant-name': this.playerName
            }
        }).then(
            response => response.json(),
            error => messagingData.logAndInform(error, "Registration for game failed")
        ).then(
            result => {
                this.uiLock = false
                this.awaitingPlayers = result.awaiting
                this.handleNew(result.links)
            }
        )
    },
    startGame() {
        this.uiLock = false
        menuData.menuActive = false
        menuData.gameStarted = true
    },
    updateGame(updateStateFromStatus) {
        this.uiLock = true
        fetch(this.statusUrl, {
            method: 'GET',
            cache: "no-cache",
            headers: {
                'Content-Type': 'application/json',
                'registrant-id': getRegistrantId()
            }
        }).then(
            response => response.json(),
            error => messagingData.logAndInform(error, "Unable to obtain game state")
        ).then(
            result => {
                updateStateFromStatus(result)
                this.awaitingPlayers = result.players
                this.uiLock = false
            }
        )
    },
    shareRegistrationLink(skipShare) {
        let doShare = !skipShare
        if (doShare && navigator.share) {
            navigator.share({
                title: 'TicTacTotem Registration Link',
                url: this.userRegistrationUrl,
            }).then(() => {
                console.log('Thanks for sharing!')
            }).catch(console.error);
        } else {
            navigator.clipboard.writeText(this.userRegistrationUrl).then(() => {
                console.log('Registration link has been copied to clipboard')
            }, (error) => {
                console.log("Error copying registration link to clipboard", error)
            })
        }
    },

    getStubs() {
        return this.currentGameLinks
    },
    getPlayerByCoord(coord) {
        return this.playerByCoord.get(coord)
    },
    setPlayerByCoord(coord, player) {
        this.playerByCoord.set(coord, player)
    },
    getWinningCoords() {
        return this.winningCoords
    },

    handleNew(data) {
        this.currentGameLinks.moveLinkStub = data.moveLink
        this.currentGameLinks.syncMoveLinkStub = data.syncMoveLink
        this.currentGameLinks.statusLinkStub = data.statusLink

        this.playerByCoord.clear()
        this.winningCoords = []
        this._currentPlayerDetails.id = 1
        this._currentPlayerDetails.name = "?"
        this._currentPlayerDetails.isWinner = false
        this._currentPlayerDetails.isStalemate = false

        window.dispatchEvent(new Event('gameNew', {
                bubbles: false,
                cancelable: false,
                composed: false
            })
        )
    },
    currentPlayerDetails(details, isWinner) {
        if (details !== undefined) {
            this._currentPlayerDetails.id = details.id == undefined ? details.order : details.id
            this._currentPlayerDetails.name = details.name
            this._currentPlayerDetails.isWinner = isWinner
            this._currentPlayerDetails.isStalemate = !isWinner && this.playerByCoord.size > 30
        }
        return this._currentPlayerDetails
    },
    environment() {
        return {
            host: this.env.host,
            apiHost: this.env.apiHost,
            eventsHost: this.env.eventsHost,
            releaseIdentifier: this.env.releaseIdentifier
        }
    },
    doMove(position, uiUpdateFn) {
        this.uiLock = true

        let url = `${this.environment().apiHost}${ menuData.getStubs().syncMoveLinkStub }?segment=${ position.segment }&radius=${ position.radius }&depth=${ position.depth }`
        fetch(url , {
            method: 'GET',
            cache: "no-cache",
            headers: {
                'Content-Type': 'application/json',
                'registrant-id': getRegistrantId()
            }
        }).then(result =>
            result.json()
        ).then(data => {
            uiUpdateFn(data)
            this.uiLock = false
        }, error => messagingData.logAndInform(error, "Request to move failed"))
    },
    reassignPlayerName() {
        this.playerName = generatePlayerName()
    }
})
