import {server, websocketServer} from "./index";
import request from 'supertest';
import {WinConditionType} from "tictactotem-engine/src/ts/BasicEntities";
import {GameStatus, WinResult} from "tictactotem-engine/src/ts/ResultEntites";
import {NewHotseatGameStart, SynchronousResult} from "./controllers/HotseatController";
import * as redis from 'redis';
import {RedisError} from "redis-errors";
import {NewInviteGameRegister, NewInviteGameStart} from "./controllers/InviteController";
import {WebSocket} from "ws";

const redisEquivalentMap = new Map<string, string>()
let breakConnection = false

jest.mock('redis', () => {
    return {
        createClient: () => {
            return {
                on: (event: string, callback: (err: unknown) => void) => {
                    return
                },
                connect: async () => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    return
                },
                GET: async (key: string) => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    return redisEquivalentMap.get(key)
                },
                SET: async (key: string, value: string) => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    redisEquivalentMap.set(key, value)
                }
            }
        }
    }
});

beforeEach( async () => {
    redisEquivalentMap.clear()
    breakConnection = false
})

describe('Hotseat Service', () => {

    it('can perform basic turn, one human, one AI', async () => {
        const newGameResult = await request(server).get("/hotseat/new");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const gameLinks: NewHotseatGameStart = JSON.parse(newGameResult.text)
        expect(gameLinks.moveLink).toMatch(/\/hotseat\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

        const result = await request(server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=0`);
        expect(result.statusCode).toEqual(200);

        const status = await request(server).get(`${gameLinks.statusLink}`);
        expect(status.statusCode).toEqual(200);

        const gameStatusModel = JSON.parse(status.text)
        expect(gameStatusModel.status).toBe(GameStatus.PLAYING)
        expect(Object.keys(gameStatusModel.boardMap).length).toEqual(2)
    })

    it('can perform basic get-to-win play, three humans', async () => {
        const newGameResult = await request(server).get("/hotseat/new?player2type=human&player3type=human");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const gameLinks: NewHotseatGameStart = JSON.parse(newGameResult.text)
        expect(gameLinks.moveLink).toMatch(/\/hotseat\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

        let result = await request(server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=0&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=1&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=1&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=1&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=2&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=2&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=2&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server).get(`${gameLinks.moveLink}?segment=3&radius=0&depth=0`); //P1 (Win)
        expect(result.statusCode).toEqual(200);
        const winCondition: WinResult = JSON.parse(result.text)
        expect(winCondition).toBeDefined()
        expect(winCondition.winConditionAcheived.type).toEqual(WinConditionType.CIRCLE)

        result = await request(server).get(`${gameLinks.syncMoveLink}?segment=3&radius=1&depth=0`); //P2 (Can't move)
        expect(result.statusCode).toEqual(200);
        const wonCondition: SynchronousResult = JSON.parse(result.text)
        expect(wonCondition).toBeDefined()
        expect((wonCondition.result as WinResult).winConditionAcheived.type).toEqual(WinConditionType.CIRCLE)
        expect((wonCondition.result as WinResult).winningPlayer.id).toEqual(1)

        const gameStatusModel = wonCondition.state
        expect(Object.keys(gameStatusModel.boardMap).length).toEqual(10)
    })
})

describe('Invite Service', () => {

    it('can perform basic turn, one human, one AI', async () => {
        const newGameResult = await request(server).get("/invite/new");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const registerResponse: NewInviteGameRegister = JSON.parse(newGameResult.text)
        const registrationResult1 = await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "123");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const inviteResponse: NewInviteGameStart = JSON.parse(registrationResult1.text)
        expect(inviteResponse.awaiting.length).toBe(2)
        expect(inviteResponse.awaiting[0].order).toBe(1)
        expect(inviteResponse.awaiting[0].accepted).toBeTruthy()
        expect(inviteResponse.awaiting[1].order).toBe(2)
        expect(inviteResponse.awaiting[1].accepted).toBeTruthy()
        expect(inviteResponse.links.moveLink).toMatch(/\/invite\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

        const result = await request(server)
            .get(`${inviteResponse.links.moveLink}?segment=0&radius=0&depth=0`)
            .set("registrant-id", "123");
        expect(result.statusCode).toEqual(200);

        const status = await request(server).get(`${inviteResponse.links.statusLink}`);
        expect(status.statusCode).toEqual(200);

        const gameStatusModel = JSON.parse(status.text)
        expect(Object.keys(gameStatusModel.boardMap).length).toEqual(2)
    })

    it('can perform basic get-to-win play, three humans', async () => {
        const newGameResult = await request(server).get("/invite/new?player2type=human&player3type=human");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const registerResponse: NewInviteGameRegister = JSON.parse(newGameResult.text)
        const registrationResult1 = await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "123");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "456");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "789");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const gameLinks: NewInviteGameStart = JSON.parse(registrationResult1.text)
        expect(gameLinks.links.moveLink).toMatch(/\/invite\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

        let result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=0&radius=0&depth=0`)
            .set("registrant-id", "123"); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=0&radius=1&depth=0`)
            .set("registrant-id", "456"); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=0&radius=0&depth=3`)
            .set("registrant-id", "789"); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=1&radius=0&depth=0`)
            .set("registrant-id", "123"); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=1&radius=1&depth=0`)
            .set("registrant-id", "456"); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=1&radius=0&depth=3`)
            .set("registrant-id", "789"); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=2&radius=0&depth=0`)
            .set("registrant-id", "123"); //P1
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=2&radius=1&depth=0`)
            .set("registrant-id", "456"); //P2
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=2&radius=0&depth=3`)
            .set("registrant-id", "789"); //P3
        expect(result.statusCode).toEqual(200);

        result = await request(server)
            .get(`${gameLinks.links.moveLink}?segment=3&radius=0&depth=0`)
            .set("registrant-id", "123"); //P1 (Win)
        expect(result.statusCode).toEqual(200);
        const winCondition: WinResult = JSON.parse(result.text)
        expect(winCondition).toBeDefined()
        expect(winCondition.winConditionAcheived.type).toEqual(WinConditionType.CIRCLE)

        result = await request(server).get(`${gameLinks.links.syncMoveLink}?segment=3&radius=1&depth=0`); //P2 (Can't move)
        expect(result.statusCode).toEqual(200);
        const wonCondition: SynchronousResult = JSON.parse(result.text)
        expect(wonCondition).toBeDefined()
        expect((wonCondition.result as WinResult).winConditionAcheived.type).toEqual(WinConditionType.CIRCLE)
        expect((wonCondition.result as WinResult).winningPlayer.id).toEqual(1)

        const gameStatusModel = wonCondition.state
        expect(Object.keys(gameStatusModel.boardMap).length).toEqual(10)
    })
})

describe('WebSocket Notifications', () => {

    it('can notify registered clients of game registrations and moves', async () => {
        let registrationEvents1 = 0;
        let moveEvents1 = 0
        let registrationEvents2 = 0
        let moveEvents2 = 0
        let registrationEvents3 = 0
        let moveEvents3 = 0

        // Given
        const wsClient1 = new WebSocket("ws://localhost:3002")
        wsClient1.on("server-connected", () => {wsClient1.send("123")})
        wsClient1.on("new-registration", () => {registrationEvents1++})
        wsClient1.on("move-made", () => {moveEvents1++})
        const wsClient2 = new WebSocket("ws://localhost:3002")
        wsClient2.on("server-connected", () => {wsClient2.send("456")})
        wsClient2.on("new-registration", () => {registrationEvents2++})
        wsClient2.on("move-made", () => {moveEvents2++})
        const wsClient3 = new WebSocket("ws://localhost:3002")
        wsClient3.on("server-connected", () => {wsClient3.send("456")})
        wsClient3.on("new-registration", () => {registrationEvents3++})
        wsClient3.on("move-made", () => {moveEvents3++})

        // When
        const newGameResult = await request(server).get("/invite/new?player2type=human");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined()

        const registerResponse: NewInviteGameRegister = JSON.parse(newGameResult.text)
        const registrationResult1 = await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "123");
        await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "456");
        await request(server)
            .get(registerResponse.links.apiRegistrationLink)
            .set("registrant-id", "789");

        const gameLinks: NewInviteGameStart = JSON.parse(registrationResult1.text)
        expect(gameLinks.links.moveLink).toMatch(/\/invite\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

        await request(server)
            .get(`${gameLinks.links.moveLink}?segment=0&radius=0&depth=0`)
            .set("registrant-id", "123"); //P1
        await request(server)
            .get(`${gameLinks.links.moveLink}?segment=0&radius=1&depth=0`)
            .set("registrant-id", "456"); //P2


        //Then
        // Gets a connection, is reg id accepted
        setTimeout( () => {
            expect(registrationEvents1).toBe(3);
            expect(registrationEvents2).toBe(2);
            expect(registrationEvents3).toBe(1);
            expect(moveEvents1).toBe(2);
            expect(moveEvents2).toBe(2);
            expect(moveEvents3).toBe(2);
        },1000)

        wsClient1.close()
        wsClient2.close()
        wsClient3.close()
    })
})

describe('Health Service',  () => {

    it('reports healthy if Redis is up', async () => {
        await request(server).get("/hotseat/new?player2type=human&player3type=human");

        const healthResult = await request(server).get("/health")

        expect(healthResult.statusCode).toEqual(200)
        expect(healthResult.headers["service_status"]).toBe("healthy")
        expect(healthResult.body).toStrictEqual({
            service: "healthy"
        });
    })

    it('reports degraded if Redis is down', async () => {
        breakConnection = true

        await request(server).get("/hotseat/new?player2type=human&player3type=human");

        const healthResult = await request(server).get("/health")

        expect(healthResult.statusCode).toEqual(200)
        expect(healthResult.headers["service_status"]).toBe("degraded")
        expect(healthResult.body).toStrictEqual({
            service: "degraded"
        });
    })

    it('reports healthy and more if Redis is up and client authorised', async () => {
        process.env.MONITORING_TOKEN = "TOKEN"
        await request(server).get("/hotseat/new?player2type=human&player3type=human");

        const healthResult = await request(server).get("/health").set("monitoring_token", "TOKEN")

        expect(healthResult.statusCode).toEqual(200)
        expect(healthResult.headers["service_status"]).toBe("healthy")
        expect(healthResult.body).toStrictEqual({
            service: "healthy",
            redis: "healthy"
        });
    })

    it('reports degraded and more if Redis is down and client authorised', async () => {
        breakConnection = true
        process.env.MONITORING_TOKEN = "TOKEN"
        await request(server).get("/hotseat/new?player2type=human&player3type=human");

        const healthResult = await request(server).get("/health").set("monitoring_token", "TOKEN")

        expect(healthResult.statusCode).toEqual(200);
        expect(healthResult.headers["service_status"]).toBe("degraded")
        expect(healthResult.body).toStrictEqual({
            service: "degraded",
            redis: "unavailable"
        });
    })
})

afterAll(async () => {
    await server.close();
    await websocketServer.close();
})
