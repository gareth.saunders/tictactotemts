"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
exports.server = exports.GameService = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const typescript_rest_1 = require("typescript-rest");
const GameBoard_1 = require("tictactotem-engine/src/ts/GameBoard");
const Player_1 = require("tictactotem-engine/src/ts/Player");
// Definitely something that I need to move to a shared store like Redis
// This will need to be coupled with a strong persistence mechanism
const currentGames = new Map();
let GameService = class GameService {
    newGame(player2Type = "computer", player3Type) {
        const players = [];
        players.push({
            id: 1,
            name: "Player One (Human)"
        });
        if (player2Type === "computer") {
            players.push({
                id: 2,
                name: "Player Two (Computer)",
                attachedCompute: Player_1.optimumWinCompute()
            });
        }
        else {
            players.push({
                id: 2,
                name: "Player Two (Human)"
            });
        }
        if (player3Type) {
            if (player3Type === "computer") {
                players.push({
                    id: 3,
                    name: "Player Three (Computer)",
                    attachedCompute: Player_1.optimumWinCompute()
                });
            }
            else {
                players.push({
                    id: 3,
                    name: "Player Three (Human)"
                });
            }
        }
        const game = new GameBoard_1.GameBoard({
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        });
        currentGames.set(game.id, game);
        return {
            moveLink: `/local/turn/${game.id}`,
            statusLink: `/local/state/${game.id}`
        };
    }
    makeTurn(id, segment, radius, depth) {
        const game = currentGames.get(id);
        if (game) {
            let result = game.makeMove({
                segment,
                radius,
                depth
            }, game.currentPlayer);
            let nextPlayer = result.nextPlayer;
            while ((nextPlayer === null || nextPlayer === void 0 ? void 0 : nextPlayer.attachedCompute) != undefined) {
                result = game.makeMove(nextPlayer.attachedCompute(game, nextPlayer), nextPlayer);
                nextPlayer = result.nextPlayer;
            }
            return result;
        }
        throw new typescript_rest_1.Errors.GoneError("The game you are looking for cannot be found");
    }
    gameState(id) {
        const game = currentGames.get(id);
        if (game) {
            const gameObj = {};
            game.gameState().boardMap.forEach((value, key) => {
                gameObj[key] = value.name;
            });
            return gameObj;
        }
        throw new typescript_rest_1.Errors.GoneError("The game you are looking for cannot be found");
    }
};
__decorate([
    typescript_rest_1.Path("new"),
    typescript_rest_1.GET,
    __param(0, typescript_rest_1.QueryParam("player2type")),
    __param(1, typescript_rest_1.QueryParam("player3type")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Object)
], GameService.prototype, "newGame", null);
__decorate([
    typescript_rest_1.Path("turn/:gameId"),
    typescript_rest_1.GET,
    __param(0, typescript_rest_1.PathParam('gameId')),
    __param(1, typescript_rest_1.QueryParam("segment")),
    __param(2, typescript_rest_1.QueryParam("radius")),
    __param(3, typescript_rest_1.QueryParam("depth")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Number, Number, Number]),
    __metadata("design:returntype", Object)
], GameService.prototype, "makeTurn", null);
__decorate([
    typescript_rest_1.Path("state/:gameId"),
    typescript_rest_1.GET,
    __param(0, typescript_rest_1.PathParam('gameId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], GameService.prototype, "gameState", null);
GameService = __decorate([
    typescript_rest_1.Path("/local")
], GameService);
exports.GameService = GameService;
const app = express_1.default();
app.set('port', process.env.PORT || 3001);
const corsOptions = {
    // origin: 'http://localhost:',
    origin: true,
    optionsSuccessStatus: 200,
    methods: ['GET', 'PUT', 'POST']
};
app.use(cors_1.default(corsOptions));
// app.options('*', cors())
typescript_rest_1.Server.buildServices(app);
exports.server = app.listen(app.get('port'), function () {
    console.log(`Rest Server listening on port ${app.get('port')}!`);
});
