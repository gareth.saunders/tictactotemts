import {GameBoard} from "./GameBoard";

export interface CylindricalKey {
    segment: number
    radius: number
    depth: number
}

export type CylindricalKeyHash = number;

export type ComputeResolver = (choices: Pair<CylindricalKey,number>[], gameBoard: GameBoard) => CylindricalKey;

export interface AttachedCompute {
    name:  string
    function: (gameBoard: GameBoard, currentPlayer: Player) => CylindricalKey
}

export interface Player {
    id: number
    name: string
    externalPlayerHash?: string
    attachedCompute?: AttachedCompute
}

export enum PlayerConnectionType {
    HOTSEAT = "hotseat",
    INVITE = "invite"
}

export function cylindricalKeyHash(key: CylindricalKey): CylindricalKeyHash {
    return (key.segment * 10000) + (key.radius * 100) + key.depth;
}

export enum WinConditionType {
    CIRCLE,
    THRU_LINE,
    DROP_LINE,
    DIAGONAL_LINE,
    CLOCKWISE_SPIRAL,
    ANTI_CLOCKWISE_SPIRAL
}

export interface WinCondition {
    type: WinConditionType,
    moveList: CylindricalKey[]
}

export interface DistanceToWin {
    condition: WinCondition,
    remainingMoves: CylindricalKey[],
    weight: number
}

export interface Pair<K,V> {
    key: K,
    value: V
}