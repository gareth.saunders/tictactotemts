import {menuData} from "../states/menu-data.js";

export default {
    data() {
        return menuData
    },
    computed: {
        playerDetails() {
            return menuData.currentPlayerDetails()
        },
    },
    template: `
        <div id="game-status" :class="{ darkMode: darkMode }" class="game-panel" v-show="!menuActive">
            <img src="assets/images/player-cross.png" v-show="playerDetails.id === 1">
            <img src="assets/images/player-circle.png" v-show="playerDetails.id === 2">
            <img src="assets/images/player-diamond.png" v-show="playerDetails.id === 3">
            <p>
                <span v-show="!playerDetails.isWinner && !playerDetails.isStalemate">{{playerDetails.name}} to move</span>
                <span v-show="playerDetails.isWinner">{{playerDetails.name}} wins!</span>
                <span v-show="playerDetails.isStalemate">Stalemate 🏳</span>
            </p>
        </div>
    `
}