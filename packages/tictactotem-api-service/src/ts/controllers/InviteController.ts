import {Errors, GET, HeaderParam, Path, PathParam, QueryParam} from "typescript-rest";
import {GameEndResult, TurnResult} from "../../../../tictactotem-engine/src/ts/ResultEntites";
import {PlayerConnectionType} from "../../../../tictactotem-engine/src/ts/BasicEntities";
import {GameService} from "../services/GameService";

@Path("/invite")
export class InviteController {
    gameService: GameService

    constructor() {
        this.gameService = GameService.get()
    }

    @Path("new")
    @GET
    async newGame(@QueryParam("player2type") player2Type: PlayerType = "computer",
                  @QueryParam("player3type") player3Type?: PlayerType): Promise<NewInviteGameRegister> {
        const game = await this.gameService.newGame(PlayerConnectionType.INVITE, player2Type, player3Type)
        return {
            links: {
                apiRegistrationLink: `/invite/register/${ game.id }`,
                userRegistrationLink: `?game-id=${ game.id }`,
                statusLink: `/invite/state/${ game.id }`
            }
        }
    }

    @Path("register/:gameId")
    @GET
    async register(
        @PathParam('gameId') gameId: string,
        @HeaderParam('registrant-id') registrantId: string,
        @HeaderParam('registrant-name') registrantName: string
    ): Promise<NewInviteGameStart> {
        const result = await this.gameService.register(gameId, registrantId, registrantName)
        if (result) {
            return result
        }
        throw new Errors.GoneError("The game you are looking for cannot be found");
    }

    @Path("turnsync/:gameId")
    @GET
    async makeTurnAndReturn(
        @PathParam('gameId') id: string,
        @QueryParam("segment") segment: number,
        @QueryParam("radius") radius: number,
        @QueryParam("depth") depth: number,
        @HeaderParam('registrant-id') registrantId: string
    ): Promise<SynchronousResult> {
        const result = await this.makeTurn(id, segment, radius, depth, registrantId)
        const state = await this.gameState(id)
        return {
            result: result,
            state: state
        }
    }

    @Path("turn/:gameId")
    @GET
    async makeTurn(
        @PathParam('gameId') id: string,
        @QueryParam("segment") segment: number,
        @QueryParam("radius") radius: number,
        @QueryParam("depth") depth: number,
        @HeaderParam('registrant-id') registrantId: string
    ): Promise<TurnResult | GameEndResult> {
        const result = await this.gameService.makeMove(id, segment, radius, depth, registrantId)
        if (result) {
            return result
        }
        throw new Errors.GoneError("The game you are looking for cannot be found");
    }

    @Path("state/:gameId")
    @GET
    /* eslint-disable-next-line  @typescript-eslint/explicit-module-boundary-types */
    async gameState(
        @PathParam('gameId') id: string
        // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    ) {
        const result = await this.gameService.gameState(id)
        if (result) {
            return result
        }
        throw new Errors.GoneError("The game you are looking for cannot be found");
    }
}

type PlayerType = "computer" | "human"

export interface NewInviteGameStart {
    gameId: string
    awaiting: PlayerDto[]
    links: {
        syncMoveLink: string
        moveLink: string
        statusLink: string
    }
}

export interface PlayerDto {
    order: number,
    name: string,
    accepted: boolean
}

export interface NewInviteGameRegister {
    links: {
        apiRegistrationLink: string
        userRegistrationLink: string
        statusLink: string
    }
}

export interface SynchronousResult {
    result: TurnResult | GameEndResult
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    state: any
}
