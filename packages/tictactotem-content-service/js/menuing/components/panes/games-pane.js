import {menuData} from "../../states/menu-data.js";

export default {
  data() {
    return menuData
  },
  template: ` 
    <div id="info-attribution">
        <p>An (over-engineered) open-source game</p>
        <p><a href="https://gitlab.com/gareth.saunders/tictactotemts">Source available under Apache License Version 2.0</a></p>
        <div class="spacer"></div>
        <p>© Gareth David Saunders, 2021</p>
        <p>Also, thanks to <a href="https://www.arjowiggins.com/">Arjowiggins</a> for use of their Inuit typeface, © Arjowiggins 2006</p>
    </div>
  `
}