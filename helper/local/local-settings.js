export const settings = {
  host: "http://${SERVER_ADDR}:8080",
  apiHost: "http://${SERVER_ADDR}:3001",
  eventsHost: "ws://${SERVER_ADDR}:3002",
  releaseIdentifier: "Local Docker Build"
}
