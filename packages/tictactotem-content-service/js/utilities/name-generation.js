import {messagingData} from "../menuing/states/messaging-data.js";

let names = ["Keith", "Lenny", "Betty"]
let nouns = ["Bunny", "Limpet", "Whippet"]
let adjectives = ["Boring", "Luminous", "Kicking"]

const populateNameArrays = () => {
    fetch("assets/lists/name-generation.json", {
        method: 'GET',
        cache: "no-cache",
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(
        response => response.json(),
        error => {
            messagingData.logAndInform(error, "Cannot populate name pool - reverting to defaults")

        }
    ).then(
        result => {
            names = result.names
            nouns = result.nouns
            adjectives = result.adjectives
        }
    )
}

export const generatePlayerName = () => {
    const nameIndex = Math.floor(Math.random() * names.length);
    const adjectiveIndex = Math.floor(Math.random() * adjectives.length);
    const nounIndex = Math.floor(Math.random() * nouns.length);
    return `${names[nameIndex]} the ${adjectives[adjectiveIndex]} ${nouns[nounIndex]}`
}

populateNameArrays()
