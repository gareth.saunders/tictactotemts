# 16. Notify clients using Web Sockets

Date: 2022-11-15

## Status

2022-11-15 proposed

## Context

Multiplayer across multiple clients requires asynchronous updates of game states so that each client can be aware of any
updates made by other clients. There are a number of options in which that can be implemented, but each has pros and cons
that could rule out their use:

- Interval calls to status endpoint: 
  - The basic option, with no extra server development and timer-based calls from each client.
  - Lots of extra service calls for the same information, significantly increasing bandwidth and client processing demands.
- Polling:
  - Small change to the server, with a new endpoint to inform a given client if there is new status available for any games.
  - Reduces bandwith, but still either frequent calls (short polling), or a flaky connection that ties up significan server connection resources (long polling).
- Server-Side Events:
  - A small technology that is like a uni-directional web-socket, ostensibly easier to implement.
  - (Mozilla SSE Introduction)[https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events]
  - It is not simple on the server side, requires strange data-block construction.
- Web Sockets:
  - (Mozilla WS Introduction)[https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API]
  - Bidirectional and always-on connection, _provided the active session is maintained_.
  - Good standard libraries, much simpler than the technology would suggest.
  - It actually works fully in all browsers 
- Background Notifications (Web Push):
  - (Web Push introduction)[https://developer.mozilla.org/en-US/docs/Web/API/Push_API]
  - One of two options that work without an active page-based connection, using Service Workers.
  - This allows the browser itself to subsume the notification aspect, which as well as the above means that the we have to implement very little of what we would with Web Sockets.
  - Requires one-time subscription (full effort unknown).
  - Works for every browser, **except Mobile Safari (in preview)**.
- Background Downloads (Sync/Fetch):
  - (Sync)[https://developer.mozilla.org/en-US/docs/Web/API/Background_Synchronization_API] / (Fetch)[https://developer.mozilla.org/en-US/docs/Web/API/Background_Fetch_API]
  - One of two options that work without an active page-based connection, using Service Workers.
  - Similar to the above, but just downloads the "files" to local storage - means that there's guarantee of notification.
  - Requires one-time subscription (full effort unknown).
  - Both currently experimental.

## Decision

Until Mobile Safari supports the Background Notification spec by default, we have forgo the best functional option, and rely on Web Sockets.

The implementation is lightweight, only passing notifications of game status update actions, to stimulate a separate call to the status endpoint.

The bidirectional nature has a very useful side-effect of allowing initial transmission of a binding Registrant ID on connection; any potential reconnection issues can be resolved by re-transmitting the ID on reconnection. 
 
## Consequences

- Web Sockets are a well-supported mechanism, and not architecturally-compromised like some alternatives.
  - Service will actually use bi-directional support for linking on (re-)connection
- There's a possibility of some or all of this mechanism being replaced by Background Notification web workers in the future.
- If not, there is a possible refactor to stop redundant responses from the registration and move endpoints.
- Also, we might also be able to attain some background _interaction_ with (the Notification API)[https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API/Using_the_Notifications_API#browser_compatibility] (mobile browser support pending).
