import {CylindricalKey, cylindricalKeyHash, CylindricalKeyHash, Player, PlayerConnectionType} from "./BasicEntities";
import {WinConditionManager} from "./WinConditions";
import {v4} from "uuid";
import {ComputeLookup} from "./Player";
import {GameEndResult, GameStatus, RegistrationResult, TurnResult, WaitingResult, WinResult} from "./ResultEntites";
import {createHash} from "crypto";

const defaultIdGen = () => {
    return v4()
}

export class GameBoard {
    readonly id: string
    readonly playerConnectionType: PlayerConnectionType
    private readonly boardMap = new Map<CylindricalKeyHash, number>()
    private readonly dimensions: CylindricalKey
    private readonly players: Player[]
    private _currentPlayer: Player
    private finalResult?: GameEndResult
    private winConditionManager: WinConditionManager

    // TODO: Look into replacing this with typescript-aop
    computeLookup: ComputeLookup

    get currentPlayer(): Player {
        return this._currentPlayer;
    }

    get boardMapView(): Readonly<Map<CylindricalKeyHash, Player>> {
        const richMap = new Map<CylindricalKeyHash, Player>()
        this.boardMap.forEach((value, key) => {
            const player = this.players.find(p => value === p.id)
            if (player){
                richMap.set(key, player)
            } else {
                console.log("Error encountered when finding valid player for boardMap view")
            }
        })
        return richMap
    }

    constructor(initData: GameBoardSetup | GameBoardData | string, options?: {
        idGenerator?: () => string
        computeLookup?: ComputeLookup
    }) {
        this.id = options?.idGenerator ? options.idGenerator() : defaultIdGen()
        this.computeLookup = options?.computeLookup ? options.computeLookup : new ComputeLookup()

        let init: GameBoardSetup | GameBoardData;
        if (typeof initData === 'string') {
            const reviver = function (key: string, value: unknown): unknown {
                if (key === 'boardMap') {
                    const bm = new Map<CylindricalKeyHash, number>()
                    const boardObj = value as Record<CylindricalKeyHash, number>
                    Object.keys(boardObj).forEach(k => {
                        const kInt = Number.parseInt(k)
                        bm.set(kInt, boardObj[kInt])
                    })
                    return bm
                }
                return value
            }
            const jsonInit = JSON.parse(initData, reviver) as GameBoardData
            jsonInit.players.forEach(player => {
                player.attachedCompute = this.computeLookup.getComputeForName(player.attachedCompute?.name)
            })

            this.id = jsonInit.id
            this._currentPlayer = jsonInit.players
                .find((player) => player.id === jsonInit.currentPlayer) ?? jsonInit.players[0]
            init = jsonInit
        } else {
            init = initData
        }

        this.playerConnectionType = init.playerConnectionType
        this.dimensions = init.dimensions
        this.players = init.players

        const data = init as GameBoardData

        if (data.currentPlayer == undefined) {
            this._currentPlayer = this.players[0]
            this.finalResult = undefined
            data.players.forEach((value, index) => value.id = index + 1)
        } else {
            this._currentPlayer = this.players
                .filter(p => p.id === data.currentPlayer)[0] // TODO: No check on file corruption
            this.finalResult = data.finalResult
        }

        if ((init as GameBoardData).boardMap) {
          this.boardMap = (init as GameBoardData).boardMap
        }

        this.winConditionManager = new WinConditionManager(this.dimensions)
    }

    gameState(): Readonly<GameBoardView> {
        return {
            id: this.id,
            status: this.determineGameStatus(),
            boardMap: this.boardMapView,
            dimensions: this.dimensions,
            players: this.players,
            currentPlayer: this._currentPlayer.id,
            finalResult: this.finalResult
        }
    }

    gameDimensions(): Readonly<CylindricalKey> {
        return this.dimensions
    }

    nextPlayer(): Readonly<Player> {
        return this.rotatePlayer(this._currentPlayer)
    }

    persist(): string {
        /* eslint-disable-next-line  @typescript-eslint/no-explicit-any */
        const boardMapped: any = {}
        this.boardMapView.forEach((value, key) => {
            boardMapped[key] = value.id
        });
        const toJson = {
            id: this.id,
            playerConnectionType: this.playerConnectionType,
            boardMap: boardMapped,
            dimensions: this.dimensions,
            players: this.players,
            currentPlayer: this._currentPlayer.id,
            finalResult: this.finalResult
        }
        return JSON.stringify(toJson, null, 2)
    }

    makeMove(key: CylindricalKey, player: Player, externalId?: string): TurnResult | GameEndResult {
        const checkForWin = <WinResult>this.finalResult
        if (checkForWin?.status === GameStatus.WIN) {
            return checkForWin
        }
        const status = this.determineGameStatus()

        if (this.players.find(p => p.id === player.id) === undefined) {
            return {
                status: status,
                moveAccepted: false,
                nextPlayer: this._currentPlayer
            }
        }

        const isGameInProgress = status === GameStatus.PLAYING
        const isMoveOnBoard = this.checkInDimensions(key, this.dimensions)
        const isPlayerCurrent = player.id === this._currentPlayer.id
        const isPlayerLegitimate = player.externalPlayerHash === this.hashIt(externalId)
        const isSegmentOccupied = this.boardMapView.has(cylindricalKeyHash(key))

        const isMoveAllowedForValidPlayer = isMoveOnBoard
            && isGameInProgress
            && isPlayerCurrent
            && isPlayerLegitimate
            && !isSegmentOccupied

        if (!isMoveAllowedForValidPlayer) {
            return {
                status: status,
                moveAccepted: false,
                nextPlayer: player
            }
        } else {
            this.boardMap.set(cylindricalKeyHash(key), player.id)
            this._currentPlayer = this.rotatePlayer(this._currentPlayer)
            return this.checkForEndCondition(this.boardMapView, key, player)
        }
    }

    private checkForEndCondition(boardMap: Map<CylindricalKeyHash, Player>, key: CylindricalKey, currentPlayer: Player): TurnResult | GameEndResult {
        const testResult = this.winConditionManager.testForCondition(boardMap)
        if (testResult) {
            this.finalResult = testResult
            return testResult
        }

        const nextPlayer = this.rotatePlayer(currentPlayer)

        return {
            status: this.determineGameStatus(),
            moveAccepted: true,
            acceptedMove: key,
            nextPlayer: nextPlayer
        };
    }

    private determineGameStatus(): GameStatus {
        const areAllPlayersDeclared = this.playerConnectionType === PlayerConnectionType.HOTSEAT ||
            this.players.filter( player =>
                !player.attachedCompute && !player.externalPlayerHash
            ).length === 0
        if (!areAllPlayersDeclared) {
            return GameStatus.REGISTRATION
        }

        // TODO: Is there a test for this?
        const checkForWin = <WinResult>this.finalResult
        const isWin = checkForWin?.status === GameStatus.WIN
        if (isWin) {
            return GameStatus.WIN
        }

        const isMapFull = this.boardMap.size === 32
        if (!isMapFull) {
            return GameStatus.PLAYING
        }

        //A fullmap without a win, this is stalemate!
        return GameStatus.STALEMATE
    }

    private rotatePlayer(currentPlayer: Player): Player {
        const currentPlayerIndex = this.players.indexOf(currentPlayer)
        const nextPlayer = this.players[(currentPlayerIndex + 1) % (this.players.length)]
        return nextPlayer
    }

    private checkInDimensions(move: CylindricalKey, dimensions: CylindricalKey) {
        const ceilingCheck = move.segment < dimensions.segment
            && move.radius < dimensions.segment
            && move.segment < dimensions.segment
        const floorCheck = move.segment > -1
            && move.radius > -1
            && move.depth > -1

        return ceilingCheck && floorCheck;
    }

    isPlayerRegistered(externalId: string) {
        const registrantHash = this.hashIt(externalId)
        const playersWithId =
            this.players.filter(player => player.externalPlayerHash === registrantHash)

        return playersWithId.length > 0
    }

    register(externalId: string, alternativeName?: string): WaitingResult {
        const applicablePlayers = this.players
            .filter(player => !player.attachedCompute && !player.externalPlayerHash)

        const playersCopy = () => JSON.parse(JSON.stringify(this.players))

        if (this.isPlayerRegistered(externalId)) {
            return {
                status: this.determineGameStatus(),
                awaitingPlayers: playersCopy(),
                registrationResult: RegistrationResult.AlreadyRegistered
            }
        }

        if (applicablePlayers.length === 0) {
            return {
                status: this.determineGameStatus(),
                awaitingPlayers: playersCopy(),
                registrationResult: RegistrationResult.NoMoreRegistrationSlots
            }
        }

        const registrantHash = this.hashIt(externalId)
        const firstApplicablePlayer = applicablePlayers[0]
        firstApplicablePlayer.externalPlayerHash = registrantHash
        if (alternativeName) {
            firstApplicablePlayer.name = alternativeName
        }

        return {
            status: this.determineGameStatus(),
            awaitingPlayers: playersCopy(),
            registrationResult: RegistrationResult.RegistrationSuccessful
        }
    }

    hashIt(externalId?: string): string | undefined {
        if (externalId) {
            return createHash('sha1').update(externalId).digest().toString()
        }
        return undefined
    }
}

export interface GameBoardSetup {
    playerConnectionType: PlayerConnectionType
    dimensions: CylindricalKey
    players: Player[]
}

export interface GameBoardData {
    id: string
    playerConnectionType: PlayerConnectionType
    boardMap: Map<CylindricalKeyHash, number>
    dimensions: CylindricalKey
    players: Player[]
    currentPlayer: number
    finalResult?: GameEndResult
}

export interface GameBoardView {
    id: string
    status: GameStatus
    boardMap: Map<CylindricalKeyHash, Player>
    dimensions: CylindricalKey
    players: Player[]
    currentPlayer: number
    finalResult?: GameEndResult
}