export function sleep(seconds: number): Promise<void> {
    return new Promise( resolve => setTimeout(resolve, seconds*1000) );
}

export function toNumber(envvar: string | undefined): number | undefined {
    if (!envvar) {
        return undefined
    }
    if (envvar === "") {
        return undefined
    }

    const convertedVar = Number.parseInt(envvar);

    if (isNaN(convertedVar)) {
        return undefined
    }
    return convertedVar
}