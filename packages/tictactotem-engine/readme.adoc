= TicTacTotem Engine (Library)

== Overview

As described in the parent link:../../readme.adoc[README], TicTacTotem (T4m) is a quantized, cylindrical Tic-Tac-Toe variant.

This package is the TicTacTotem-Typescript (T5S) game engine, which encapsulates the core game mechanics in a universally deliverable library.

=== Technology? Choices

Typescript:: Used to offer the portability of Javascript with good language facilities for structure, link:../../docs/adr/0002-use-typescript-as-primary-game-engine-language.md[ADR0002].

== Implementation Guide

CAUTION: This section describes a pre-release interface, which is neither yet focussed nor stable.

...

== Game Mechanics

=== Game Space Parameters

==== Game Board Dimensions and Representation

The T4M game "board" is defined using a quantised set of integer cylindrical coordinates, using the following dimensions:

|===
| Dimension | Type   |
| Radius    | Linear |
| Segment   | Radial |
| Depth     | Linear |
|===

As such, the game space can be modelled as map of _cylindrical coordinate keys_ (hashed) to _player values_.

==== Turn Progression and Game-End Conditions

Players progress the game by committing turns in a U-Go-I-Go fashion.

The winning player is the one who first satisfies one of the algorithmically-defined win condtions:

Circle:: stem:[ in_(0 -> S)^n [r = r_0 , s = n, d = d_0] ]
Drop Line:: stem:[ in_(0 -> D)^n [r = r_0 , s = s_0, d = n] ]
Thru Line:: stem:[ in_(-R -> R)^n [r = |n| , s = s_0 + ((Sn)/(2|n|))%S, d = d_0] ]
Diagonal:: stem:[ in_(-R -> R)^n [r = |n| , s = s_0 + ((Sn)/(2|n|))%S, d = d_0+n+R] ]
Clockwise Spiral:: stem:[ in_(0 -> D)^n [r = r_0, s = (s_0 + n)%S, d = n] ]
Anti-clockwise Spiral:: stem:[ in_(0 -> D)^n [r = r_0, s = |(s_0 - n)%S|, d = n] ]

This is checked internally by the library, and the move given by the WinResult response.

TIP: Although these conditions are capable of stem:[lim_(N->ZZ)], they were originally made with a board size of stem:[2xx4xx4] stem:[[R xx S xx D]]. As such, this can be considered a subset of

=== AI
