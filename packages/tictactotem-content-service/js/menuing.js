import { createApp } from 'https://cdn.jsdelivr.net/npm/vue@3.2.41/dist/vue.esm-browser.min.js'
import {menuData} from "./menuing/states/menu-data.js";
import MenuButton from './menuing/components/menu-button.js'
import MenuView from './menuing/components/menu-view.js'
import GameStatus from './menuing/components/game-status.js'
import BuildNotifier from './menuing/components/build-notifier.js'
import MessagePanel from './menuing/components/message-panel.js'
import InfoPanel from './menuing/components/info-panel.js'

export let initMenus = () => {
    const queryString = location.search;
    const params = new URLSearchParams(queryString);
    const gameId = params.get("game-id");
    if (gameId) {
        menuData.apiRegistrationUrl = `${menuData.environment().apiHost}/invite/register/${ gameId }`
        menuData.statusUrl = `${menuData.environment().apiHost}/invite/state/${ gameId }`
        menuData.toggle('tab', 'lobby')
        menuData.registerForGame()
    }

    createApp({
        data() {
            return menuData
        },
        components: {
            MenuButton,
            MenuView,
            GameStatus,
            BuildNotifier,
            MessagePanel,
            InfoPanel
        }
    }).mount('#menuing')
}