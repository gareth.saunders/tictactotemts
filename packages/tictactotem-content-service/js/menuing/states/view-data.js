import { reactive } from 'https://cdn.jsdelivr.net/npm/vue@3.2.41/dist/vue.esm-browser.min.js'

export const viewData = {
    viewStack: reactive([]),
    viewNames: {
        "new-game": "New Game",
        "lobby": "Lobby",
        "settings": "Settings",
        "attribution": "About"
    },

    pushView(stackId) {
        if (this.viewNames[stackId]) {
            this.viewStack.push(stackId)
        }
    },
    pullView(numberToRollBack) {
        if (!numberToRollBack) {
            numberToRollBack = 1
        }
        for (let i = 0; i < numberToRollBack+1; i++) {
            this.viewStack.pop()
        }
    },
    currentView() {
        if (this.viewStack.length === 0) {
            return undefined
        }

        return this.viewStack[this.viewStack.length-1]
    },
    currentViewName() {
        if (this.viewStack.length === 0) {
            return ""
        }

        let stackId = this.viewStack[this.viewStack.length-1]
        return this.viewNames[stackId]
    },
    pastViewNames() {
        let views = []

        for (const index in this.viewStack.slice(1)) {
            const name = this.viewNames[this.viewStack[index]]
            views.push({step: index, name: name})
        }

        views.push({step: views.length, name: "Home"})
        return views.reverse()
    },
    iterateAllViews() {
        let viewIterator = []
        for (const [key, value] of Object.entries(this.viewNames)) {
            viewIterator.push({ id: key, name: value})
        }
        return viewIterator
    }

}
