# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.6.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.5.4...tictactotem-content-service@1.6.0) (2022-12-15)


### Features

* Transition to push views over tabs ([ccc2a94](https://gitlab.com/gareth.saunders/tictactotemts/commit/ccc2a944c52b533a8720b8a667f30e6ffd0c395d))



### [1.5.4](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.5.3...tictactotem-content-service@1.5.4) (2022-12-12)


### Bug Fixes

* Add forgotten items from prior MR!85 ([ab9e663](https://gitlab.com/gareth.saunders/tictactotemts/commit/ab9e663ebd7d2f81f1ab56c8f55f2aa946cd4f11))



### [1.5.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.5.2...tictactotem-content-service@1.5.3) (2022-12-12)


### Bug Fixes

* Add further small UX refinements ([0ce51b0](https://gitlab.com/gareth.saunders/tictactotemts/commit/0ce51b01bdea56d7fa95eef88d042c7a496e5895))



### [1.5.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.5.1...tictactotem-content-service@1.5.2) (2022-12-09)


### Bug Fixes

* Allow running of docker solution over local networks ([a1a25a3](https://gitlab.com/gareth.saunders/tictactotemts/commit/a1a25a31c9958da368007f3f9f187fe55cf05ecd))



### [1.5.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.5.0...tictactotem-content-service@1.5.1) (2022-12-07)


### Bug Fixes

* Adjust gameboard touch targets to avoid mis-presses ([8b00bde](https://gitlab.com/gareth.saunders/tictactotemts/commit/8b00bdec2e907bbd06bcb41099fa0a1d187d3133))



## [1.5.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.4.2...tictactotem-content-service@1.5.0) (2022-12-03)


### Features

* Improvements to menu look-and-feel ([aaacad7](https://gitlab.com/gareth.saunders/tictactotemts/commit/aaacad7b4da200e31678671918d89f4384a551c2))



### [1.4.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.4.1...tictactotem-content-service@1.4.2) (2022-11-21)


### Bug Fixes

* Correct WS host configuration (attempt two) ([b1b69f4](https://gitlab.com/gareth.saunders/tictactotemts/commit/b1b69f4beebd031974956680caaef77a11093d45))



### [1.4.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.4.0...tictactotem-content-service@1.4.1) (2022-11-21)


### Bug Fixes

* Correct WS host configuration ([df756f2](https://gitlab.com/gareth.saunders/tictactotemts/commit/df756f2017ad8db41e5eb4caf4f4b519c4d152f8))



## [1.4.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.3.0...tictactotem-content-service@1.4.0) (2022-11-21)


### Features

* Introduce invite mode to UI ([ff01e7e](https://gitlab.com/gareth.saunders/tictactotemts/commit/ff01e7ef839e8081c876a5d9e64b7b5f45e3a2d2))



## [1.3.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.2.3...tictactotem-content-service@1.3.0) (2022-10-19)


### Features

* Add backend functionality and endpoint for invite-based games ([04c7b4c](https://gitlab.com/gareth.saunders/tictactotemts/commit/04c7b4c7c3d68ddfe6186ce486bf69d29304a7fa))



### [1.2.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.2.2...tictactotem-content-service@1.2.3) (2022-09-23)


### Bug Fixes

* Correct UI edge cases for mobile screens ([2b12cd4](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b12cd4ea06106d3fd0842c7b73fa9ccbb998622))



### [1.2.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.2.1...tictactotem-content-service@1.2.2) (2022-09-09)


### Bug Fixes

* Improve responsive of UI on smaller screens ([16d6aed](https://gitlab.com/gareth.saunders/tictactotemts/commit/16d6aed6dd5aff43e76a372b23c85e78bf7996dc))



### [1.2.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.2.0...tictactotem-content-service@1.2.1) (2022-08-18)


### Bug Fixes

* Security and functional improvements to the API health endpoint ([0a28954](https://gitlab.com/gareth.saunders/tictactotemts/commit/0a289549a7a924ac6eaedbad8cd2fe5e767e9227))



## [1.2.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.7...tictactotem-content-service@1.2.0) (2022-08-18)


### Features

* Streamline UI theme ([97da211](https://gitlab.com/gareth.saunders/tictactotemts/commit/97da211f0321fc091cc1203cc37e91231f179485))



### [1.1.7](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.6...tictactotem-content-service@1.1.7) (2022-07-27)


### Bug Fixes

* Add artifact declaration to content bump stage in pipeline ([f3de30b](https://gitlab.com/gareth.saunders/tictactotemts/commit/f3de30bb35f83d41416509b277e5e653d0390ebc))



### [1.1.6](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.5...tictactotem-content-service@1.1.6) (2022-07-27)


### Bug Fixes

* Ensure docker versions are not trailing release versions ([f59e91f](https://gitlab.com/gareth.saunders/tictactotemts/commit/f59e91fb6deae733f4b2ba8c268d25effef884f1))



### [1.1.5](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.4...tictactotem-content-service@1.1.5) (2022-04-03)


### Bug Fixes

* Add rolling update support (with health checks), and seperate live and preview pipeline steps ([a4d0687](https://gitlab.com/gareth.saunders/tictactotemts/commit/a4d06873732e19ab23fe1e3de2a6f3119f6d9607))



### [1.1.4](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.3...tictactotem-content-service@1.1.4) (2022-01-31)

**Note:** Version bump only for package tictactotem-content-service





### [1.1.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.2...tictactotem-content-service@1.1.3) (2022-01-28)

**Note:** Version bump only for package tictactotem-content-service





### [1.1.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.1...tictactotem-content-service@1.1.2) (2022-01-28)

**Note:** Version bump only for package tictactotem-content-service





### [1.1.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.1.0...tictactotem-content-service@1.1.1) (2022-01-26)


### Bug Fixes

* Only raycast when there is no board rotation ([c66af4b](https://gitlab.com/gareth.saunders/tictactotemts/commit/c66af4b14b3aeeebf166d49e05f26d81e966a3bf))



## [1.1.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@1.0.1...tictactotem-content-service@1.1.0) (2022-01-26)


### Features

* Suppress input and add visual cues when awaiting network results ([cf5c00a](https://gitlab.com/gareth.saunders/tictactotemts/commit/cf5c00a5274f6e91807b8c51d4b8c78a8b31ad91))



### [1.0.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-content-service@0.5.0...tictactotem-content-service@1.0.1) (2022-01-21)


### Bug Fixes

* Manual bump to one-aught (leave pre-release) ([17fb56d](https://gitlab.com/gareth.saunders/tictactotemts/commit/17fb56d4b187e716ceb80dedd87fffe66674f41a))



## 0.5.0 (2022-01-21)


### Bug Fixes

* Lerna builds to be versioned independently ([a71f7d6](https://gitlab.com/gareth.saunders/tictactotemts/commit/a71f7d68d3223b04874780305f2f429163b14c44))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))

## 0.2.0 (2022-01-06)


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* add vue.js-powered menuing to UI ([737871d](https://gitlab.com/gareth.saunders/tictactotemts/commit/737871dc287ba2ec14c3639f4511580b745d2d5d))
* add vue.js-powered menuing to UI ([f4ca6b9](https://gitlab.com/gareth.saunders/tictactotemts/commit/f4ca6b91321753066aec7f7e62381abaac2ae8af))
* Cleaner routing - includes crawler prevention, and force SSL redirect ([a58b4be](https://gitlab.com/gareth.saunders/tictactotemts/commit/a58b4be734b93a4d23e54acc0acecaccae57921d))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([ee66a05](https://gitlab.com/gareth.saunders/tictactotemts/commit/ee66a05fa5a3f14d0cca508d9a193ec5b3e22aec))
* First phase of front-end improvements for MVD. ([a07014d](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07014d83b02310e357b9d5e8831a3ffc6f6b2c1))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Adjust font sizes for iOS Safari viewport ([137bd94](https://gitlab.com/gareth.saunders/tictactotemts/commit/137bd94f888f57461af15793489e09bef0b39f51))
* Adjust font sizes for iOS Safari viewport pt2 ([6e9544d](https://gitlab.com/gareth.saunders/tictactotemts/commit/6e9544d9ab43011f9eeb0b0c67309d9f5fc78d95))
* Adjust font sizes for iOS Safari viewport pt3 ([8fb0132](https://gitlab.com/gareth.saunders/tictactotemts/commit/8fb0132109d34ae24c25ec324a691f04ad12ada5))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Clear up player symbol semantics w.r.t. new games ([ce79ee0](https://gitlab.com/gareth.saunders/tictactotemts/commit/ce79ee0ed13d5f635e23f1774a5f457fda918287))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Recovering from bad merge shenannigans ([9a02f76](https://gitlab.com/gareth.saunders/tictactotemts/commit/9a02f76e92d30e88ea8ef294f4ab73e4299b28f3))
* Recovering from bad merge shenannigans pt2 ([f22dbb0](https://gitlab.com/gareth.saunders/tictactotemts/commit/f22dbb006053e8a2c2c8bf3574c3a16495450619))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))



# 0.4.0 (2022-01-13)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Adjust font sizes for iOS Safari viewport ([137bd94](https://gitlab.com/gareth.saunders/tictactotemts/commit/137bd94f888f57461af15793489e09bef0b39f51))
* Adjust font sizes for iOS Safari viewport pt2 ([6e9544d](https://gitlab.com/gareth.saunders/tictactotemts/commit/6e9544d9ab43011f9eeb0b0c67309d9f5fc78d95))
* Adjust font sizes for iOS Safari viewport pt3 ([8fb0132](https://gitlab.com/gareth.saunders/tictactotemts/commit/8fb0132109d34ae24c25ec324a691f04ad12ada5))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Clear up player symbol semantics w.r.t. new games ([ce79ee0](https://gitlab.com/gareth.saunders/tictactotemts/commit/ce79ee0ed13d5f635e23f1774a5f457fda918287))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* Recovering from bad merge shenannigans ([9a02f76](https://gitlab.com/gareth.saunders/tictactotemts/commit/9a02f76e92d30e88ea8ef294f4ab73e4299b28f3))
* Recovering from bad merge shenannigans pt2 ([f22dbb0](https://gitlab.com/gareth.saunders/tictactotemts/commit/f22dbb006053e8a2c2c8bf3574c3a16495450619))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* add vue.js-powered menuing to UI ([737871d](https://gitlab.com/gareth.saunders/tictactotemts/commit/737871dc287ba2ec14c3639f4511580b745d2d5d))
* add vue.js-powered menuing to UI ([f4ca6b9](https://gitlab.com/gareth.saunders/tictactotemts/commit/f4ca6b91321753066aec7f7e62381abaac2ae8af))
* Cleaner routing - includes crawler prevention, and force SSL redirect ([a58b4be](https://gitlab.com/gareth.saunders/tictactotemts/commit/a58b4be734b93a4d23e54acc0acecaccae57921d))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([ee66a05](https://gitlab.com/gareth.saunders/tictactotemts/commit/ee66a05fa5a3f14d0cca508d9a193ec5b3e22aec))
* First phase of front-end improvements for MVD. ([a07014d](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07014d83b02310e357b9d5e8831a3ffc6f6b2c1))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))





# 0.3.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Adjust font sizes for iOS Safari viewport ([137bd94](https://gitlab.com/gareth.saunders/tictactotemts/commit/137bd94f888f57461af15793489e09bef0b39f51))
* Adjust font sizes for iOS Safari viewport pt2 ([6e9544d](https://gitlab.com/gareth.saunders/tictactotemts/commit/6e9544d9ab43011f9eeb0b0c67309d9f5fc78d95))
* Adjust font sizes for iOS Safari viewport pt3 ([8fb0132](https://gitlab.com/gareth.saunders/tictactotemts/commit/8fb0132109d34ae24c25ec324a691f04ad12ada5))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Clear up player symbol semantics w.r.t. new games ([ce79ee0](https://gitlab.com/gareth.saunders/tictactotemts/commit/ce79ee0ed13d5f635e23f1774a5f457fda918287))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* Recovering from bad merge shenannigans ([9a02f76](https://gitlab.com/gareth.saunders/tictactotemts/commit/9a02f76e92d30e88ea8ef294f4ab73e4299b28f3))
* Recovering from bad merge shenannigans pt2 ([f22dbb0](https://gitlab.com/gareth.saunders/tictactotemts/commit/f22dbb006053e8a2c2c8bf3574c3a16495450619))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* add vue.js-powered menuing to UI ([737871d](https://gitlab.com/gareth.saunders/tictactotemts/commit/737871dc287ba2ec14c3639f4511580b745d2d5d))
* add vue.js-powered menuing to UI ([f4ca6b9](https://gitlab.com/gareth.saunders/tictactotemts/commit/f4ca6b91321753066aec7f7e62381abaac2ae8af))
* Cleaner routing - includes crawler prevention, and force SSL redirect ([a58b4be](https://gitlab.com/gareth.saunders/tictactotemts/commit/a58b4be734b93a4d23e54acc0acecaccae57921d))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([ee66a05](https://gitlab.com/gareth.saunders/tictactotemts/commit/ee66a05fa5a3f14d0cca508d9a193ec5b3e22aec))
* First phase of front-end improvements for MVD. ([a07014d](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07014d83b02310e357b9d5e8831a3ffc6f6b2c1))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))





# 0.2.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Adjust font sizes for iOS Safari viewport ([137bd94](https://gitlab.com/gareth.saunders/tictactotemts/commit/137bd94f888f57461af15793489e09bef0b39f51))
* Adjust font sizes for iOS Safari viewport pt2 ([6e9544d](https://gitlab.com/gareth.saunders/tictactotemts/commit/6e9544d9ab43011f9eeb0b0c67309d9f5fc78d95))
* Adjust font sizes for iOS Safari viewport pt3 ([8fb0132](https://gitlab.com/gareth.saunders/tictactotemts/commit/8fb0132109d34ae24c25ec324a691f04ad12ada5))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Another docker push fix ([33e7c82](https://gitlab.com/gareth.saunders/tictactotemts/commit/33e7c82df0698ff0e651c7d103a27ed27e499490))
* Artifact Publishing Part Two: Correct allowed test fail plus docker paths ([2b4c988](https://gitlab.com/gareth.saunders/tictactotemts/commit/2b4c9880806a9492a7d50562fb7e2a230005bffa))
* Clear up player symbol semantics w.r.t. new games ([ce79ee0](https://gitlab.com/gareth.saunders/tictactotemts/commit/ce79ee0ed13d5f635e23f1774a5f457fda918287))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* correct docker artifact names ([aef72ff](https://gitlab.com/gareth.saunders/tictactotemts/commit/aef72ffbcfd271970f0638b15ef0c0e15535c027))
* Recovering from bad merge shenannigans ([9a02f76](https://gitlab.com/gareth.saunders/tictactotemts/commit/9a02f76e92d30e88ea8ef294f4ab73e4299b28f3))
* Recovering from bad merge shenannigans pt2 ([f22dbb0](https://gitlab.com/gareth.saunders/tictactotemts/commit/f22dbb006053e8a2c2c8bf3574c3a16495450619))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Add Docker Compose file to facilitate local builds ([c9c212e](https://gitlab.com/gareth.saunders/tictactotemts/commit/c9c212e902d54ad6cd02aed47186527a47266042))
* add vue.js-powered menuing to UI ([737871d](https://gitlab.com/gareth.saunders/tictactotemts/commit/737871dc287ba2ec14c3639f4511580b745d2d5d))
* add vue.js-powered menuing to UI ([f4ca6b9](https://gitlab.com/gareth.saunders/tictactotemts/commit/f4ca6b91321753066aec7f7e62381abaac2ae8af))
* Cleaner routing - includes crawler prevention, and force SSL redirect ([a58b4be](https://gitlab.com/gareth.saunders/tictactotemts/commit/a58b4be734b93a4d23e54acc0acecaccae57921d))
* defining and preparing service packages ([8bd7500](https://gitlab.com/gareth.saunders/tictactotemts/commit/8bd7500da4d4bbb8292db05edce7c3c8e3a542d9))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* First phase of front-end improvements for MVD. ([ee66a05](https://gitlab.com/gareth.saunders/tictactotemts/commit/ee66a05fa5a3f14d0cca508d9a193ec5b3e22aec))
* First phase of front-end improvements for MVD. ([a07014d](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07014d83b02310e357b9d5e8831a3ffc6f6b2c1))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))
