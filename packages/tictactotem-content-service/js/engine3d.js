import * as THREE from 'https://cdn.skypack.dev/three@0.129.0';

import {
    baseMaterial,
    selectedMaterial,
    hoverMaterial,
    chosenMaterialP1,
    chosenMaterialP2, chosenMaterialP3
} from "./engine3d/materials.js";
import {addP1Symbol, addRings, addBalls, addPoles, addRods, addP2Symbol, addP3Symbol} from "./engine3d/models.js";
import {createBackgroundTexture} from "./engine3d/textures.js";
import {menuData} from "./menuing/states/menu-data.js";

let container;

let camera, scene, renderer;

let group;

let targetRotation = 0;
let targetRotationOnPointerDown = 0;

let raycaster, intersects, pointer;
let pointerX = 0;
let pointerXOnPointerDown = 0;
let isPointerDown = false;
let tripped = false;
let SELECTED;

let directionalLightViewing;
let sineSeed = 0;
let sineSeedDirection = 1;

let windowHalfX = window.innerWidth / 2;

export let initThreeJs = () => {
    container = document.getElementById('main')

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    scene = new THREE.Scene();
    scene.background = createBackgroundTexture('pastel-background', 6,30, 20);

    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.set(0, 375, 550);
    camera.rotation.set(-0.35, 0, 0)
    scene.add(camera);

    directionalLightViewing = new THREE.DirectionalLight({
        color: 0xffffff,
        intensity: 1,
        position: new THREE.Vector3(5, 5, 5)
    });
    scene.add(directionalLightViewing);

    group = new THREE.Group();
    group.position.y = 50;
    scene.add(group);

    raycaster = new THREE.Raycaster();
    pointer = new THREE.Vector2();
    intersects = [];

    for (let radius = 0; radius < 2; radius++) {
        for (let depth = 0; depth < 4; depth++) {
            addRings(radius, depth, group);
        }
    }

    for (let radius = 0; radius < 2; radius++) {
        for (let segment = 0; segment < 4; segment++) {
            addPoles(segment, radius, group);
        }
    }

    for (let segment = 0; segment < 4; segment++) {
        for (let depth = 0; depth < 4; depth++) {
            addRods(segment, depth, group);
        }
    }

    for (let radius = 0; radius < 2; radius++) {
        for (let segment = 0; segment < 4; segment++) {
            for (let depth = 0; depth < 4; depth++) {
                addBalls(segment, radius, depth, group);
            }
        }
    }

    container.style.touchAction = 'none';
    container.addEventListener('pointerdown', onPointerDown);
    container.addEventListener('pointermove', onPointerMove);
    window.addEventListener('resize', onWindowResize);
};

function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onPointerDown(event) {
    if (menuData.uiLock) {
        return;
    }

    if (event.isPrimary === false) return;
    isPointerDown = true

    // console.log(event.clientX + "|" + event.clientY)
    pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;

    pointerXOnPointerDown = event.clientX - windowHalfX;
    targetRotationOnPointerDown = targetRotation;

    document.addEventListener('pointerup', onPointerUp);

}

function onPointerMove(event) {
    if (menuData.uiLock) {
        return;
    }

    if (event.isPrimary === false) return;

    if (isPointerDown) {
        pointerX = event.clientX - windowHalfX;
        targetRotation = targetRotationOnPointerDown + (pointerX - pointerXOnPointerDown) * 0.02;
    } else {
        pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
        pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;
    }

}

function onPointerUp(event) {

    if (event.isPrimary === false) return;

    pointerX = event.clientX - windowHalfX;
    document.removeEventListener('pointerup', onPointerUp);

    isPointerDown = false
    // Allow for inaccuracies found in touch interfaces
    if (Math.abs(pointerX - pointerXOnPointerDown) < 5) {
        tripped = true;
    }
}

export function animate() {
    requestAnimationFrame(animate);

    // const delta = clock.getDelta();
    // mixer.update( delta );

    group.updateMatrixWorld();
    render();
    // stats.update();

}

function updateForPlayer(coordHash, playerId) {
    if ( menuData.getPlayerByCoord(coordHash) ) return

    const segment = (coordHash - coordHash%10000)/10000
    const radius = (coordHash - coordHash%100 - segment*10000)/100
    const depth = coordHash%100

    switch (playerId) {
        case 1: addP1Symbol(segment, radius, depth, group); break;
        case 2: addP2Symbol(segment, radius, depth, group); break;
        case 3: addP3Symbol(segment, radius, depth, group); break;
    }
}

function dealWithIntersection(intersections) {
    const intersection = intersections
        .filter(i => i.object.meta)
        .filter(i => i.object.meta.type.interactive)[0]

    if (!intersection) {
        return;
    }

    if (tripped) {
        const loc = SELECTED ? SELECTED.meta.position : undefined;
        const coord = SELECTED ? loc.segment * 10000 + loc.radius * 100 + loc.depth : -1;
        if (loc === intersection.object.meta.position && menuData.getPlayerByCoord(coord)) {
            SELECTED = undefined
        } else if (loc === intersection.object.meta.position) {
            menuData.doMove(SELECTED.meta.position, updateStateFromStatus2)
        } else {
            SELECTED = intersection.object
        }

        tripped = false
    }
    return intersection;
}

export function updateStateFromStatus(result) {
    // TODO: Hack, unify with move state update, and move out of here ?!
    console.log(result)

    if (result.status === "win") {
        menuData.currentPlayerDetails(result.finalResult.winningPlayer, true)
        menuData.getWinningCoords().push(...result.finalResult.winningPath.map( value => ((value.segment * 10000) + (value.radius * 100) + value.depth) ) )
    } else {
        menuData.currentPlayerDetails(result.players[result.currentPlayer-1], false)
    }

    SELECTED = undefined
    // Adjust player data
    const boardMap = result.boardMap
    for (const feld in boardMap) {
        updateForPlayer(feld, boardMap[feld])
        menuData.setPlayerByCoord(feld, boardMap[feld])
    }

}

export function updateStateFromStatus2(result) {
    console.log(result)

    if (result.result.winConditionAcheived) {
        menuData.currentPlayerDetails(result.result.winningPlayer, true)
        menuData.getWinningCoords().push(...result.result.winningPath.map( value => ((value.segment * 10000) + (value.radius * 100) + value.depth) ) )
    } else {
        menuData.currentPlayerDetails(result.result.nextPlayer, false)
    }

    if (result.result.winConditionAcheived || result.result.moveAccepted) {
        // Adjust player data
        const boardMap = result.state.boardMap
        for (const feld in boardMap) {
            updateForPlayer(feld, boardMap[feld])
            menuData.setPlayerByCoord(feld, boardMap[feld])
        }
    } else {
        // TODO: Feedback to player ???
        SELECTED = undefined
    }
}

function updateElementEffects(mesh) {
    if (!mesh.meta) {
        return;
    }

    const loc = mesh.meta.position
    const coord = loc.segment * 10000 + loc.radius * 100 + loc.depth
    mesh.scale.set(1, 1, 1);

    if (menuData.getWinningCoords().includes(coord)) {
        let currentScale = 1.3 + Math.sin(sineSeed)*0.25;
        mesh.scale.set(currentScale, currentScale, currentScale);
        mesh.rotation.y = -group.rotation.y;

        if (!mesh.meta.type.playerToken) {
            setMaterialByPlayer(mesh, coord)
        }
        return;
    }

    if (mesh.meta.type.playerToken) {
        mesh.rotation.y = -group.rotation.y;
        return;
    }

    mesh.material = baseMaterial;
    mesh.rotation.y = mesh.rotation.y > 2 * Math.PI ? 0 : mesh.rotation.y + 0.01;

    if (loc && menuData.getPlayerByCoord(`${coord}`)) {
        setMaterialByPlayer(mesh, coord)
    }
}

function render() {
    if (menuData.darkMode) {
        scene.background = createBackgroundTexture('pexels-philippe-donn-1169754');
    } else {
        scene.background = createBackgroundTexture('pastel-background', 6, 30, 20);
    }

    if (menuData.menuActive
        || menuData.currentPlayerDetails().isWinner
        || menuData.currentPlayerDetails().isStalemate) {
        targetRotation = targetRotation === 2*Math.PI ? 0 : group.rotation.y + 0.05;
    }

    group.rotation.y += (targetRotation - group.rotation.y) * 0.1;

    if (directionalLightViewing) {
        let x = Math.sin(sineSeed)*3;
        let z = Math.cos(sineSeed)*3;

        directionalLightViewing.position.x = x+4;
        directionalLightViewing.position.y = 5;// * Math.sin(sineSeed)*sineSeedDirection;
        directionalLightViewing.position.z = z-1;

        sineSeedDirection = sineSeed > Math.PI ? 1 : sineSeed < 0 ? -1 : sineSeedDirection;
        const intensity = menuData.uiLock ? 0.5 : 0.01;
        sineSeed = sineSeed + (intensity * sineSeedDirection);
    }

    // console.time('determineIntersections');
    if (Math.abs(targetRotation - group.rotation.y) < 0.0001) {
        raycaster.setFromCamera(pointer, camera);
        intersects = raycaster.intersectObjects(scene.children, true);
    } else {
        intersects = [];
    }
    // console.timeEnd('determineIntersections');

    let currentIntersection = undefined;

    // console.time('dealWithIntersection');
    if (intersects.length > 0) {
        currentIntersection = dealWithIntersection(intersects)
    }
    // console.timeEnd('dealWithIntersection');

    // console.time('updateElementEffects');
    group.children.forEach((mesh) => {
        updateElementEffects(mesh)
    })

    if (currentIntersection) {
        const loc = currentIntersection.object.meta.position
        const coord = loc.segment * 10000 + loc.radius * 100 + loc.depth
        if ( !(loc && menuData.getPlayerByCoord(`${coord}`)) ) {
            currentIntersection.object.material = hoverMaterial;
            currentIntersection.object.scale.set(1.2, 1.2, 1.2);
        }
    }

    if (SELECTED) {
        const loc = SELECTED.meta.position
        const coord = loc.segment * 10000 + loc.radius * 100 + loc.depth
        if ( loc && menuData.getPlayerByCoord(`${coord}`) ){
            setMaterialByPlayer(SELECTED.material, coord)
        } else {
            SELECTED.material = selectedMaterial
        }
    }
    // console.timeEnd('updateElementEffects');

    renderer.render(scene, camera);

}

function setMaterialByPlayer(mesh, coord) {
    const player = menuData.getPlayerByCoord(`${coord}`)
    if (player === 1) {
        mesh.material = chosenMaterialP1
    }
    if (player === 2) {
        mesh.material = chosenMaterialP2
    }
    if (player === 3) {
        mesh.material = chosenMaterialP3
    }
}

window.addEventListener('gameNew', () => {
    SELECTED = undefined
    let remove = []

    group.children.forEach((mesh) => {
        if (!mesh.meta) {
            return;
        }

        if (!mesh.meta.type.playerToken) {
            return;
        }

        remove.push(mesh)
    })

    remove.forEach( mesh => {
        mesh.parent.remove( mesh );
    })

    renderer.renderLists.dispose()
})