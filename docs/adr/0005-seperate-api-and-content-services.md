# 5. Seperate API and content services

Date: 2021-05-26

## Status

Accepted

Builds on [4. Restructure to modular monorepo](0004-restructure-to-modular-monorepo.md)

Built upon by [6. Use typescript-rest for presenting API](0006-use-typescript-rest-for-presenting-api.md)

## Context

As alluded to in the previous ADR, there is a need to keep deliverable aspects of the game separate, starting with the game engine; this ADR expands on this, consideration now required for the classic separation of presentation and function. 

This is an age-old concern which as been discussed ad-nauseum and thus can be considered beyond the scope of this document, but _how_ the split is to delivered should be considered. As we have made this a monorepo, these splits can manifest as separate services or libraries as required.

## Decision

Although the technologies are yet to be decided, the split should manifest as one API service and a UI content service.

The former is to act a wrapper around the engine that provides the relevant registration/gameplay endpoints and state management.

The latter is to serve the UI through client-side web pages, potentially with any UI-focussed APIs if required. Hopefully, we can constrain this just needing static pages with AJAX.

This means that there is a maintainable split in responsibility, and we can also use the API with an external front-end (even via an intermediary) should the need arise.

## Consequences

- Looser coupling possible between the services means that they can be used with other agents in the future (much like with the gaem engine).
- There is separation of concerns, which generally means less spaghetti code as well as allowing for outright component replacement if necessary.
  - Small changes with have obvious borders (simpler), but will be more difficult making end-to-end changes. But at least in a monorepo, it will be contained to the repo.
- Delineation may make some debugging easier (simpler to pinpoint), but "cross-border" debugging might be more complex. 
- Will conflict with DRY principles at times, with redundant build config, etc.
