# Roadmap

The following milestones are to occur on the road to V1.0:

|===
| Milestone                       | Intended Scope                                                                    | ETA
| <<Minimum Viable Demonstrator>> | Basic/functional online hotseat game, deployed by a basic build pipeline          | Mid-december
| <<Minimum Viable Product>>      | Functionally refined online hotseat game, provisioned by a working build pipeline | Mid-Feb
| ... | ? | ?
| <<Version One-Aught>> | ? | ?
|===

## Planned Milestones

### Minimum Viable Demonstrator

Status:: Confirmed

Epics remaining 2021-11-17::
    - Documentation
    - Split environments to allow for preview
    - Add local development setup
    - Auto-deploy Kubernetes (own pipeline)
    - Build pipeline corrections
    - UI performance & usability corrections

### Minimum Viable Product

Status:: Finalising scope

Epics::
    - Develop test framework for 3D UI (click to REST call)
    - Implement non-3D tests for UI
    - Network play mode
    - Basic monitoring
    - Amenable documentation generation (ADR, etc)
    - Refine UI from feedback (green-blue?)

### Version One-Aught

Status:: No idea

### Unsorted Epics & Stories

These are here on a temporary basis, until the GitLab PM functionality is understood:

- Integrate known users
- Add score boards
- Implement game recording

