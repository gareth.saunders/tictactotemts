import {ComputeLookup, firstChoiceResolver, optimumWinCompute} from "./Player";
import {GameBoard} from "./GameBoard";
import {cylindricalKeyHash, Player, PlayerConnectionType, WinConditionType} from "./BasicEntities";
import {WinConditionManager} from "./WinConditions";
import {StalemateResult, TurnResult, WinResult} from "./ResultEntites";

const dimensions = {
    segment: 4,
    radius: 2,
    depth: 4
}

const predictableOWCompute = optimumWinCompute(firstChoiceResolver)

const player1 = {
    id: 1,
    name: "P1"
}

const player2 = {
    id: 2,
    name: "P2"
}

const cpu1: Player = {
    id: 4,
    name: "CPU1",
    attachedCompute: predictableOWCompute
}

describe('Simple AI predicate tests', () => {
    test('should make any move if first', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [cpu1, player1]
        })

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as TurnResult
        expect(expectedResult.nextPlayer.id).toBe(player1.id)
        expect(expectedResult.moveAccepted).toBeTruthy()

        expect(board.gameState().boardMap.size).toBe(1)
    })

    test('should take a known win', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [cpu1,player1]
        })
        board.makeMove({segment: 0, radius: 0, depth: 0}, cpu1)
        board.makeMove({segment: 3, radius: 1, depth: 1}, player1)
        board.makeMove({segment: 1, radius: 0, depth: 0}, cpu1)
        board.makeMove({segment: 3, radius: 1, depth: 2}, player1)
        board.makeMove({segment: 2, radius: 0, depth: 0}, cpu1)
        board.makeMove({segment: 3, radius: 0, depth: 2}, player1)

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as WinResult
        expect(expectedResult.winningPlayer).toBe(cpu1)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)

        expect(board.gameState().boardMap.size).toBe(7)

    })

    test('should prevent a known loss by the next player', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })
        board.makeMove({segment: 0, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 1}, cpu1)
        board.makeMove({segment: 1, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 2}, cpu1)
        board.makeMove({segment: 2, radius: 0, depth: 0}, player1)

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        // Player cannot play the winning move
        const nextFailedResult = board.makeMove({segment: 3, radius: 0, depth: 0}, player1)

        //Then
        const expectedResult = result as TurnResult
        expect(expectedResult.nextPlayer.id).toBe(player1.id)

        const expectedBlock = nextFailedResult as TurnResult
        expect(expectedBlock.nextPlayer.id).toBe(player1.id)
        expect(expectedBlock.moveAccepted).toBeFalsy()

        expect(board.gameState().boardMap.size).toBe(6)
    })

    test('should make the optimum move, when presented with one (couple of moves in, 2P)', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })
        board.makeMove({segment: 0, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 2, radius: 0, depth: 1}, cpu1)
        board.makeMove({segment: 1, radius: 0, depth: 0}, player1)

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as TurnResult
        expect(expectedResult.nextPlayer.id).toBe(player1.id)
        expect(expectedResult.moveAccepted).toBeTruthy()
        expect(expectedResult.acceptedMove).toBeDefined()
        if (expectedResult.acceptedMove) {
            // N.B. Although it would appear that to block the circle at this point would make more sense,
            // the game space is open enough that there are more opportunities to win generated from this move.
            expect(cylindricalKeyHash(expectedResult.acceptedMove)).toBe(cylindricalKeyHash({
                segment: 0,
                radius: 0,
                depth: 1
            }))
        }

        expect(board.gameState().boardMap.size).toBe(4)
    })

    test('should make the optimum move, when presented with one (closer to end, 3P)', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, player2, cpu1]
        })
        board.makeMove({segment: 0, radius: 1, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 1}, player2)
        board.makeMove({segment: 3, radius: 0, depth: 1}, cpu1)
        board.makeMove({segment: 1, radius: 1, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 2}, player2)
        board.makeMove({segment: 1, radius: 0, depth: 2}, cpu1)
        board.makeMove({segment: 2, radius: 1, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 3}, player2)

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as TurnResult
        expect(expectedResult.nextPlayer.id).toBe(player1.id)
        expect(expectedResult.moveAccepted).toBeTruthy()
        expect(expectedResult.acceptedMove).toBeDefined()
        if (expectedResult.acceptedMove) {
            // A diagonal path is definitely the most optimal here
            expect(cylindricalKeyHash(expectedResult.acceptedMove)).toBe(cylindricalKeyHash({
                segment: 3,
                radius: 1,
                depth: 0
            }))
        }

        expect(board.gameState().boardMap.size).toBe(9)

    })

})

describe('AI free-for-all', () => {

    test('lonely AI should play until a win', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [cpu1]
        })

        //When
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as WinResult
        expect(expectedResult.winningPlayer).toBe(cpu1)

        expect(board.gameState().boardMap.size).toBe(4)
    })

    //FLAKEY - sometimes requires more moves
    test('AI should defeat stupid player', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })
        const manager = new WinConditionManager(dimensions)

        //When
        board.makeMove(
            {
                segment: 1,
                radius: 1,
                depth: 1
            },
            player1
        )
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        board.makeMove(
            manager.orderedDistancesToWin(board.gameState().boardMap, player1)
                .sort((a,b) => a.weight - b.weight)[0].remainingMoves[0],
            player1
        )
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        board.makeMove(
            manager.orderedDistancesToWin(board.gameState().boardMap, player1)
                .sort((a,b) => a.weight - b.weight)[0].remainingMoves[0],
            player1
        )
        board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)
        board.makeMove(
            manager.orderedDistancesToWin(board.gameState().boardMap, player1)
                .sort((a,b) => a.weight - b.weight)[0].remainingMoves[0],
            player1
        )
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as WinResult
        expect(expectedResult.winningPlayer).toBe(cpu1)

        expect(board.gameState().boardMap.size).toBe(8)
    })

    test.skip('AI should defeat average player', () => {
        //Given

        //When

        //Then
    })

    // There is a hint of randomness here, so not sure how to test
    test('multi-AI game should complete', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })

        //When
        let terminatesGame = false
        while (terminatesGame) {
            const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)
            const winResult = result as WinResult
            const stalemateResult = result as StalemateResult
            terminatesGame = winResult.winningPlayer != undefined || stalemateResult == undefined
        }

        //Then
    })
})

describe('AI exceptional state handling', () => {
    test('should handle the aftermath of a once known loss by the next player', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })
        board.makeMove({segment: 0, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 1}, cpu1)
        board.makeMove({segment: 1, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 1, depth: 2}, cpu1)
        board.makeMove({segment: 2, radius: 0, depth: 0}, player1)
        board.makeMove({segment: 3, radius: 0, depth: 0}, cpu1)
        board.makeMove({segment: 2, radius: 1, depth: 2}, player1)

        //When
        const result = board.makeMove(predictableOWCompute.function(board, cpu1), cpu1)

        //Then
        const expectedResult = result as TurnResult
        expect(expectedResult.moveAccepted).toBeTruthy()
        expect(expectedResult.nextPlayer.id).toBe(player1.id)
        expect(cylindricalKeyHash(expectedResult.acceptedMove! )).not.toBe(30000)

        expect(board.gameState().boardMap.size).toBe(8)
    })
})

describe('Compute Lookup', () => {
    test('should find a custom compute', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, cpu1]
        })

        let customCompute = {
            name: 'custom',
            function(gameBoard: GameBoard, currentPlayer: Player) {
                return {segment: 0, radius: 1, depth: 2}
            }
        }
        let lookup = new ComputeLookup()
        lookup.registerCompute(customCompute)

        //When
        let returnedCompute = lookup.getComputeForName("custom")

        //Then
        expect(returnedCompute?.name).not.toBeNull()
        expect(returnedCompute?.name).toBe('custom')
        expect(returnedCompute?.function(board, player1).segment).toBe(0)
        expect(returnedCompute?.function(board, player1).radius).toBe(1)
        expect(returnedCompute?.function(board, player1).depth).toBe(2)
    })

    test('should find all default computes', () => {
        //Given
        let lookup = new ComputeLookup()

        //When
        let lazyCompute = lookup.getComputeForName("lazy")
        let optimumWinCompute = lookup.getComputeForName("optimumWin")

        //Then
        expect(lazyCompute?.name).not.toBeNull()
        expect(lazyCompute?.name).toBe('lazy')

        expect(optimumWinCompute?.name).not.toBeNull()
        expect(optimumWinCompute?.name).toBe('optimumWin')
    })

    test('should return undefined compute for undefined lookup', () => {
        //Given
        let lookup = new ComputeLookup()

        //When
        let nullCompute = lookup.getComputeForName(undefined)

        //Then
        expect(nullCompute?.name).toBeUndefined()
    })
})