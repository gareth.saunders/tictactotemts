# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [1.1.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/holding-service@1.1.1...holding-service@1.1.2) (2022-08-18)


### Bug Fixes

* Security and functional improvements to the API health endpoint ([0a28954](https://gitlab.com/gareth.saunders/tictactotemts/commit/0a289549a7a924ac6eaedbad8cd2fe5e767e9227))



### [1.1.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/holding-service@1.1.0...holding-service@1.1.1) (2022-04-03)


### Bug Fixes

* Add rolling update support (with health checks), and seperate live and preview pipeline steps ([a4d0687](https://gitlab.com/gareth.saunders/tictactotemts/commit/a4d06873732e19ab23fe1e3de2a6f3119f6d9607))



## 1.1.0 (2022-02-19)


### Features

* Introduce environments by namespace ([7abedac](https://gitlab.com/gareth.saunders/tictactotemts/commit/7abedacaa9b739f83c401f11a264acf06b313422))
