import {CylindricalKey, Player, PlayerConnectionType, WinConditionType} from "./BasicEntities";
import {GameBoard} from "./GameBoard";
import {ComputeLookup} from "./Player";
import {GameStatus, RegistrationResult, TurnResult, WaitingResult, WinResult} from "./ResultEntites";
import {createHash} from "crypto";
import exp from "constants";

const dimensions = {
    segment: 4,
    radius: 2,
    depth: 4
}

let player1: Player

let player2: Player

let player3: Player

const sequenceCompute = (moves: CylindricalKey[]) => {
    let moveIterator = moves.entries()
    return {
        name: "dummy",
        function(gameBoard: GameBoard, currentPlayer: Player): CylindricalKey {
            return moveIterator.next().value[1]
        }
    }
}

const cpu1: Player = {
    id: 4,
    name: "CPU1",
    attachedCompute: sequenceCompute([
        {
            segment: 1,
            radius: 0,
            depth: 0
        }
    ])
}

beforeEach(() => {
    player1 = {
        id: 1,
        name: 'P1'
    }

    player2 = {
        id: 2,
        name: 'P2'
    }

    player3 = {
        id: 3,
        name: 'P3'
    }
});

describe('Game Board operation', () => {

    test('should play a simple game through', () => {
        //Given
        const board = new GameBoard({
                playerConnectionType: PlayerConnectionType.HOTSEAT,
                dimensions: dimensions,
                players: [player1, player2, player3]
            })

        //When
        board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 0
        }, player2)
        board.makeMove({
            segment: 1,
            radius: 0,
            depth: 0
        }, player3)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 1
        }, player2)
        board.makeMove({
            segment: 2,
            radius: 0,
            depth: 1
        }, player3)
        board.makeMove({
            segment: 2,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 3
        }, player2)
        const penultimateResult = board.makeMove({
            segment: 3,
            radius: 0,
            depth: 0
        }, player3)
        const finalResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player1)

        //Then
        const penultimateResultExpected = penultimateResult as TurnResult
        expect(penultimateResultExpected).toBeDefined()
        expect(penultimateResultExpected.moveAccepted).toBeTruthy()
        expect(penultimateResultExpected.nextPlayer).toBe(player1)

        const expectedResult = finalResult as WinResult
        expect(expectedResult).toBeDefined()

        expect(expectedResult.winningPlayer).toBe(player1)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)

    })

    test('should play a game uninterrupted by mis-ordered plays', () => {
        //Given
        const board = new GameBoard({
                playerConnectionType: PlayerConnectionType.HOTSEAT,
                dimensions: dimensions,
                players: [player1, player2]
            })

        //When
        board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 2
        }, player2)
        const duplicateTurnResult = board.makeMove({
            segment: 1,
            radius: 0,
            depth: 2
        }, player2)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 1
        }, player2)
        board.makeMove({
            segment: 2,
            radius: 1,
            depth: 2
        }, player1)
        const penultimateResult = board.makeMove({
            segment: 0,
            radius: 0,
            depth: 3
        }, player2)
        const illegalPlayerResult = board.makeMove({
            segment: 3,
            radius: 0,
            depth: 0
        }, player3)
        const finalResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player1)
        const postWinResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player1)

        //Then
        const duplicateTurnResultExpected = duplicateTurnResult as TurnResult
        expect(duplicateTurnResultExpected).toBeDefined()
        expect(duplicateTurnResultExpected.moveAccepted).toBeFalsy()
        expect(duplicateTurnResultExpected.nextPlayer).toBe(player2)

        const illegalPlayerResultExpected = illegalPlayerResult as TurnResult
        expect(illegalPlayerResultExpected).toBeDefined()
        expect(illegalPlayerResultExpected.moveAccepted).toBeFalsy()
        expect(illegalPlayerResultExpected.nextPlayer).toBe(player1)

        const penultimateResultExpected = penultimateResult as TurnResult
        expect(penultimateResultExpected).toBeDefined()
        expect(penultimateResultExpected.moveAccepted).toBeTruthy()
        expect(penultimateResultExpected.nextPlayer).toBe(player1)

        const expectedResult = finalResult as WinResult
        expect(expectedResult).toBeDefined()

        expect(expectedResult.winningPlayer).toBe(player1)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)

        const postWinResultExpected = postWinResult as WinResult
        expect(postWinResultExpected).toBeDefined()

        expect(postWinResultExpected.winningPlayer).toBe(expectedResult.winningPlayer)
        expect(postWinResultExpected.winConditionAcheived.type).toBe(expectedResult.winConditionAcheived.type)

    })

    test('should prevent move in an occupied slot', () => {
        //Given
        const board = new GameBoard({
                playerConnectionType: PlayerConnectionType.HOTSEAT,
                dimensions: dimensions,
                players: [player1, player2]
            })

        //When
        const okayResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player1)
        const badPlacementResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player2)
        const goodPlacementResult = board.makeMove({
            segment: 2,
            radius: 0,
            depth: 2
        }, player2)
        const successivePlacementResult = board.makeMove({
            segment: 1,
            radius: 1,
            depth: 1
        }, player2)

        //Then
        const okayResultExpected = okayResult as TurnResult
        expect(okayResultExpected).toBeDefined()
        expect(okayResultExpected.moveAccepted).toBeTruthy()
        expect(okayResultExpected.nextPlayer).toBe(player2)

        const badPlacementResultExpected = badPlacementResult as TurnResult
        expect(badPlacementResultExpected).toBeDefined()
        expect(badPlacementResultExpected.moveAccepted).toBeFalsy()
        expect(badPlacementResultExpected.nextPlayer).toBe(player2)

        const goodPlacementResultExpected = goodPlacementResult as TurnResult
        expect(goodPlacementResultExpected).toBeDefined()
        expect(goodPlacementResultExpected.moveAccepted).toBeTruthy()
        expect(goodPlacementResultExpected.nextPlayer).toBe(player1)

        const successivePlacementResultExpected = successivePlacementResult as TurnResult
        expect(successivePlacementResultExpected).toBeDefined()
        expect(successivePlacementResultExpected.moveAccepted).toBeFalsy()
        expect(successivePlacementResultExpected.nextPlayer).toBe(player2)

    })

    test('should restrict illegal moves that exceed dimensions', () => {
    //Given
    const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, player2]
        })

    //When
    const okayResult = board.makeMove({
        segment: 3,
        radius: 1,
        depth: 2
    }, player1)
    const outOfBoundSegmentResult = board.makeMove({
        segment: 8,
        radius: 1,
        depth: 2
    }, player2)
    const outOfBoundRadiusResult = board.makeMove({
        segment: 8,
        radius: -1,
        depth: 2
    }, player2)
    const outOfBoundDepthResult = board.makeMove({
        segment: 8,
        radius: 1,
        depth: 28
    }, player2)

    //Then
    const okayResultExpected = okayResult as TurnResult
    expect(okayResultExpected).toBeDefined()
    expect(okayResultExpected.moveAccepted).toBeTruthy()
    expect(okayResultExpected.nextPlayer).toBe(player2)

    const outOfBoundSegmentResultExpected = outOfBoundSegmentResult as TurnResult
    expect(outOfBoundSegmentResultExpected).toBeDefined()
    expect(outOfBoundSegmentResultExpected.moveAccepted).toBeFalsy()
    expect(outOfBoundSegmentResultExpected.nextPlayer).toBe(player2)

    const outOfBoundRadiusResultExpected = outOfBoundRadiusResult as TurnResult
    expect(outOfBoundRadiusResultExpected).toBeDefined()
    expect(outOfBoundRadiusResultExpected.moveAccepted).toBeFalsy()
    expect(outOfBoundRadiusResultExpected.nextPlayer).toBe(player2)

    const outOfBoundDepthResultExpected = outOfBoundDepthResult as TurnResult
    expect(outOfBoundDepthResultExpected).toBeDefined()
    expect(outOfBoundDepthResultExpected.moveAccepted).toBeFalsy()
    expect(outOfBoundDepthResultExpected.nextPlayer).toBe(player2)

})

})

describe('Game Board persistence mechanism', () => {
    test('should persist state to valid JSON string', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, player2, cpu1]
        }, {
            idGenerator: () => "f9ff0027-5e80-4000-a51b-6f35cca817ba"
        })

        board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 0
        }, player2)
        board.makeMove( cpu1.attachedCompute!.function(board, cpu1), cpu1)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player1)

        //When
        let jsonString = board.persist()

        //Then
        let expectedJson = `{
  "id": "f9ff0027-5e80-4000-a51b-6f35cca817ba",
  "playerConnectionType": "hotseat",
  "boardMap": {
    "0": 2,
    "102": 1,
    "10000": 3,
    "10102": 1
  },
  "dimensions": {
    "segment": 4,
    "radius": 2,
    "depth": 4
  },
  "players": [
    {
      "id": 1,
      "name": "P1"
    },
    {
      "id": 2,
      "name": "P2"
    },
    {
      "id": 3,
      "name": "CPU1",
      "attachedCompute": {
        "name": "dummy"
      }
    }
  ],
  "currentPlayer": 2
}`

        expect(jsonString).toBeDefined()
        expect(jsonString).toBe(expectedJson)
    })

    test('should rehydrate from JSON string', () => {
        //Given
        let importJson = `{
  "id": "f9ff0027-5e80-4000-a51b-6f35cca817ba",
  "playerConnectionType": "hotseat",
  "boardMap": {
    "0": 2,
    "102": 1,
    "10000": 3,
    "10102": 1
  },
  "dimensions": {
    "segment": 4,
    "radius": 2,
    "depth": 4
  },
  "players": [
    {
      "id": 1,
      "name": "P1"
    },
    {
      "id": 2,
      "name": "CPU1",
      "attachedCompute": {
        "name": "dummy"
      }
    },
    {
      "id": 3,
      "name": "P2"
    }
  ],
  "currentPlayer": 2
}`
        //When
        const computeLookup = new ComputeLookup()
        computeLookup.registerCompute(sequenceCompute([]))
        const board = new GameBoard(importJson, {
            computeLookup: computeLookup
        })

        //Then
        expect(board.id).toBe("f9ff0027-5e80-4000-a51b-6f35cca817ba")
        expect(board.playerConnectionType).toBe(PlayerConnectionType.HOTSEAT)
        let gameState = board.gameState()
        expect(gameState.id).toBe("f9ff0027-5e80-4000-a51b-6f35cca817ba")
        expect(gameState.dimensions).toStrictEqual({segment: 4, radius: 2, depth: 4})
        expect(gameState.status).toBe(GameStatus.PLAYING)

        expect(gameState.players.length).toBe(3)
        expect(gameState.players[0].id).toBe(1)
        expect(gameState.players[0].name).toBe("P1")
        expect(gameState.players[0].attachedCompute).toBeUndefined()
        expect(gameState.players[1].id).toBe(2)
        expect(gameState.players[1].name).toBe("CPU1")
        expect(gameState.players[1].attachedCompute).toBeDefined()
        expect(gameState.players[1].attachedCompute?.name).toBe("dummy")
        expect(gameState.players[1].attachedCompute?.function).toBeDefined()
        expect(gameState.players[2].id).toBe(3)
        expect(gameState.players[2].name).toBe("P2")
        expect(gameState.players[2].attachedCompute).toBeUndefined()

        expect(gameState.currentPlayer).toBe(2)
        expect(gameState.boardMap.size).toBe(4)
        expect(gameState.finalResult).toBeUndefined()

        expect(gameState.boardMap.get(0)?.id).toBe(2)
    })

    test('should persist and rehydrate with a win condition', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: dimensions,
            players: [player1, player2]
        }, {
            idGenerator: () => "f9ff0027-5e80-4000-a51b-6f35cca817ba"
        })

        //When
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 0
        }, player1)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 0
        }, player2)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 1
        }, player1)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 1
        }, player2)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 2
        }, player1)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player2)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 3
        }, player1)

        //When
        let jsonString = board.persist()

        const computeLookup = new ComputeLookup()
        computeLookup.registerCompute(sequenceCompute([]))
        const rehydratedBoard = new GameBoard(jsonString, {
            computeLookup: computeLookup
        })

        //Then
        expect(board.id).toBe("f9ff0027-5e80-4000-a51b-6f35cca817ba")
        expect(board.playerConnectionType).toBe(PlayerConnectionType.HOTSEAT)
        let gameState = board.gameState()
        expect(gameState.id).toBe("f9ff0027-5e80-4000-a51b-6f35cca817ba")
        expect(gameState.dimensions).toStrictEqual({segment: 4, radius: 2, depth: 4})
        expect(gameState.status).toBe(GameStatus.WIN)

        expect(gameState.players.length).toBe(2)
        expect(gameState.players[0].id).toBe(1)
        expect(gameState.players[0].name).toBe("P1")
        expect(gameState.players[0].attachedCompute).toBeUndefined()
        expect(gameState.players[1].id).toBe(2)
        expect(gameState.players[1].name).toBe("P2")
        expect(gameState.players[1].attachedCompute).toBeUndefined()

        expect(gameState.currentPlayer).toBe(2)
        expect(gameState.boardMap.size).toBe(7)
        expect(gameState.finalResult).toBeDefined()
        expect(gameState.finalResult?.status).toBe(GameStatus.WIN)

        const finalResult = <WinResult>gameState.finalResult
        expect(finalResult?.winConditionAcheived.type).toBe(WinConditionType.DROP_LINE)
        expect(finalResult?.winningPlayer.id).toBe(1)
        expect(finalResult?.winningPath[0].depth).toBe(0)
        expect(finalResult?.winningPath[1].depth).toBe(1)
        expect(finalResult?.winningPath[2].depth).toBe(2)
        expect(finalResult?.winningPath[3].depth).toBe(3)

        expect(gameState.boardMap.get(0)?.id).toBe(1)
    })

    test.skip('should handle missing current player', () => {})
    test.skip('should handle corrupted JSON string', () => {})

    test.skip('should detect inconsistent game state', () => {
        // This test is a placeholder for future functionality
        // Ideally, the game state should be checked for inconsistency
        // Might be more than one test eventually
    })
})

describe('Game Board invite-based play', () => {
    test('should play a simple game through with completed registration', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.INVITE,
            dimensions: dimensions,
            players: [player1, cpu1, player2]
        })

        const player1NewName = "Alice"
        const player1ExternalId = "AliceID"
        const player2ExternalId = "BobID"

        //When
        board.register(player1ExternalId, player1NewName)
        board.register(player2ExternalId)

        board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 0
        }, cpu1)
        board.makeMove({
            segment: 1,
            radius: 0,
            depth: 0
        }, player2, player2ExternalId)
        board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 1
        }, cpu1)
        board.makeMove({
            segment: 2,
            radius: 0,
            depth: 1
        }, player2, player2ExternalId)
        board.makeMove({
            segment: 2,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)
        board.makeMove({
            segment: 0,
            radius: 0,
            depth: 3
        }, cpu1)
        const penultimateResult = board.makeMove({
            segment: 3,
            radius: 0,
            depth: 0
        }, player2, player2ExternalId)
        const finalResult = board.makeMove({
            segment: 3,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)

        //Then
        const player1ExpectedHash = createHash('sha1').update(player1ExternalId).digest().toString()

        const penultimateResultExpected = penultimateResult as TurnResult
        expect(penultimateResultExpected).toBeDefined()
        expect(penultimateResultExpected.moveAccepted).toBeTruthy()
        expect(penultimateResultExpected.nextPlayer).toBe(player1)

        const expectedResult = finalResult as WinResult
        expect(expectedResult).toBeDefined()

        expect(expectedResult.winningPlayer).toBe(player1)
        expect(expectedResult.winningPlayer.externalPlayerHash).toBe(player1ExpectedHash)
        expect(expectedResult.winningPlayer.name).toBe(player1NewName)
        expect(expectedResult.winConditionAcheived.type).toBe(WinConditionType.CIRCLE)
    })

    test('should block play until registration is complete', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.INVITE,
            dimensions: dimensions,
            players: [player1, cpu1, player2]
        })

        const player1Name = "Alice"
        const player2Name = "Bob"

        const player1ExternalId = "AliceID"
        const player2ExternalId = "BobID"

        //When
        const firstRegistrationResponse = board.register(player1ExternalId, player1Name)

        const preRegistrationResult = board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)

        const lastRegistrationResponse = board.register(player2ExternalId, player2Name)

        const postRegistrationResult = board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)

        //Then
        expect(firstRegistrationResponse.status).toBe(GameStatus.REGISTRATION)
        expect(firstRegistrationResponse.awaitingPlayers.length).toBe(3)
        expect(firstRegistrationResponse.awaitingPlayers[0].id).toBe(1)
        expect(firstRegistrationResponse.awaitingPlayers[0].name).toBe(player1Name)
        expect(firstRegistrationResponse.awaitingPlayers[0].attachedCompute).toBeUndefined()
        expect(firstRegistrationResponse.awaitingPlayers[0].externalPlayerHash).toBe(
            createHash('sha1').update(player1ExternalId).digest().toString()
        )
        expect(firstRegistrationResponse.awaitingPlayers[1].id).toBe(2)
        expect(firstRegistrationResponse.awaitingPlayers[1].name).toBeDefined()
        expect(firstRegistrationResponse.awaitingPlayers[1].attachedCompute).toBeDefined()
        expect(firstRegistrationResponse.awaitingPlayers[1].externalPlayerHash).toBeUndefined()
        expect(firstRegistrationResponse.awaitingPlayers[2].id).toBe(3)
        expect(firstRegistrationResponse.awaitingPlayers[2].name).toBe("P2")
        expect(firstRegistrationResponse.awaitingPlayers[2].attachedCompute).toBeUndefined()
        expect(firstRegistrationResponse.awaitingPlayers[2].externalPlayerHash).toBeUndefined()

        expect(lastRegistrationResponse.status).toBe(GameStatus.PLAYING)
        expect(lastRegistrationResponse.awaitingPlayers.length).toBe(3)
        expect(lastRegistrationResponse.awaitingPlayers[2].id).toBe(3)
        expect(lastRegistrationResponse.awaitingPlayers[2].name).toBe(player2Name)
        expect(lastRegistrationResponse.awaitingPlayers[2].attachedCompute).toBeUndefined()
        expect(lastRegistrationResponse.awaitingPlayers[2].externalPlayerHash).toBe(
            createHash('sha1').update(player2ExternalId).digest().toString()
        )

        const preRegistrationResultExpected = preRegistrationResult
        expect(preRegistrationResultExpected).toBeDefined()
        expect(preRegistrationResult.status).toBe(GameStatus.REGISTRATION)

        const postRegistrationResultExpected = postRegistrationResult as TurnResult
        expect(postRegistrationResultExpected).toBeDefined()
        expect(postRegistrationResultExpected.status).toBe(GameStatus.PLAYING)
        expect(postRegistrationResultExpected.moveAccepted).toBeTruthy()
        expect(postRegistrationResultExpected.nextPlayer).toBe(cpu1)

    })

    test('should ignore repeated registration', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.INVITE,
            dimensions: dimensions,
            players: [player1, cpu1, player2]
        })

        const player1ExternalId = "AliceID"
        const player2ExternalId = "BobID"

        //When
        const firstRegistrationResponse = board.register(player1ExternalId)
        const reregistrationResponse = board.register(player1ExternalId)
        const lastRegistrationResponse = board.register(player2ExternalId)


        //Then
        expect(firstRegistrationResponse.status).toBe(GameStatus.REGISTRATION)
        expect(firstRegistrationResponse.awaitingPlayers.length).toBe(3)
        expect(firstRegistrationResponse.awaitingPlayers[0].externalPlayerHash).toBeDefined()
        expect(firstRegistrationResponse.awaitingPlayers[2].externalPlayerHash).toBeUndefined()
        expect(firstRegistrationResponse.registrationResult).toBe(RegistrationResult.RegistrationSuccessful)

        expect(reregistrationResponse.status).toBe(GameStatus.REGISTRATION)
        expect(reregistrationResponse.awaitingPlayers.length).toBe(3)
        expect(reregistrationResponse.awaitingPlayers[0].externalPlayerHash).toBeDefined()
        expect(reregistrationResponse.awaitingPlayers[2].externalPlayerHash).toBeUndefined()
        expect(reregistrationResponse.registrationResult).toBe(RegistrationResult.AlreadyRegistered)

        expect(lastRegistrationResponse.status).toBe(GameStatus.PLAYING)
        expect(lastRegistrationResponse.awaitingPlayers.length).toBe(3)
        expect(lastRegistrationResponse.awaitingPlayers[0].externalPlayerHash).toBeDefined()
        expect(lastRegistrationResponse.awaitingPlayers[2].externalPlayerHash).toBeDefined()
        expect(lastRegistrationResponse.registrationResult).toBe(RegistrationResult.RegistrationSuccessful)

    })

    test('should ignore registration when no more game slots are available', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.INVITE,
            dimensions: dimensions,
            players: [player1, cpu1, player2]
        })

        const player1ExternalId = "AliceID"
        const player2ExternalId = "BobID"
        const player3ExternalId = "CharlieID"

        //When
        const firstRegistrationResponse = board.register(player1ExternalId)
        const lastRegistrationResponse = board.register(player2ExternalId)
        const lateRegistrationResponse = board.register(player3ExternalId)

        //Then
        expect(firstRegistrationResponse.status).toBe(GameStatus.REGISTRATION)
        expect(firstRegistrationResponse.registrationResult).toBe(RegistrationResult.RegistrationSuccessful)

        expect(lastRegistrationResponse.status).toBe(GameStatus.PLAYING)
        expect(lastRegistrationResponse.registrationResult).toBe(RegistrationResult.RegistrationSuccessful)

        expect(lateRegistrationResponse.status).toBe(GameStatus.PLAYING)
        expect(lateRegistrationResponse.registrationResult).toBe(RegistrationResult.NoMoreRegistrationSlots)
    })

    test('should block play of masquerading player', () => {
        //Given
        const board = new GameBoard({
            playerConnectionType: PlayerConnectionType.INVITE,
            dimensions: dimensions,
            players: [player1, player2]
        })

        const player1ExternalId = "AliceID"
        const player2ExternalId = "EveID"

        const player1As2 = {
            id: player1.id,
            name: player2.name
        }

        //When
        board.register(player1ExternalId)
        board.register(player2ExternalId)

        board.makeMove({
            segment: 0,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)
        const naughtyResult = board.makeMove({
            segment: 1,
            radius: 1,
            depth: 2
        }, player2, player1ExternalId)
        board.makeMove({
            segment: 1,
            radius: 0,
            depth: 0
        }, player2, player2ExternalId)
        const niceResult = board.makeMove({
            segment: 2,
            radius: 1,
            depth: 2
        }, player1, player1ExternalId)

        //Then
        const naughtyResultExpected = naughtyResult as TurnResult
        expect(naughtyResultExpected).toBeDefined()
        expect(naughtyResultExpected.moveAccepted).toBeFalsy()
        expect(naughtyResultExpected.nextPlayer).toBe(player2)

        const expectedResult = niceResult as TurnResult
        expect(expectedResult).toBeDefined()
        expect(expectedResult.moveAccepted).toBeTruthy()
        expect(expectedResult.nextPlayer).toBe(player2)

    })
})