# 10. Direct imports to be used between internal libraries and services

Date: 2022-01-05

## Status

Accepted

Builds on [4. Restructure to modular monorepo](0004-restructure-to-modular-monorepo.md)

## Context

There is a dichotomy regarding the importation of library packages internally, within the monorepo:
1. The import between packages is a direct reference, using a local file path (supported by a root tsconfig)
2. The library is published to artifactory, and the import is pulled from the published artifact.

We cannot do both, as the underlying interpreters do not support it.

Although originally the second option had been implemented, there is an underlying complication that has forced us to reconsider. Although previous docker builds had worked, an unknown regression related to yarn hoisting in the Docker build context has brought in a cascade of workspace-related issues that have been difficult (potentially impossible?) to resolve (notably, the inability to resolve Engine imports from NPM due to disagreement between ts-node and the ES6 modules spec). 

_Note that this was only a problem in Docker builds._

## Decision

Although a fix for option 1 could have been pursued, effort has become unreasonable and a shift to option 2 has become preferable from this perspective (as this can be made to work now).

Outside of effort, neither approach is necessarily better or worse, but a matter of tradeoffs (as outlined in **Consequences**).

## Consequences

- From the experience described in the **Context**, this is a more robust approach.
- Development changes can be instantly realised when running locally.
- If the changes between packages are not staggered:
  - library and service version become coupled, and thus changes could be immediately promoted upstream (this could also be a benefit, as the compiler will immediately enforce contract). 
  - Dependent services might be incorrectly versioned (e.g. API might be promoted as breaking, even though it's an Engine contract change, and invisible at the API).
