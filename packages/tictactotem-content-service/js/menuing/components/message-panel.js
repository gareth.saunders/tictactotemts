import {messagingData} from "../states/messaging-data.js";

export default {
    data() {
        return messagingData
    },
    watch: {
        currentMessage(newValue) {
            if( newValue === "" ) {
                return
            }
            setTimeout(() => messagingData.currentMessage = "", 8000)
        }
    },

    template: `
    <transition name="message"> 
        <div id="message-panel" class="right-align" v-show="currentMessage !== ''">
            <p>Message</p>
            <span>{{currentMessage}}</span>
        </div>
    </transition>
  `
}