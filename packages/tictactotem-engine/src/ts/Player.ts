import {
    AttachedCompute,
    ComputeResolver,
    CylindricalKey,
    cylindricalKeyHash,
    DistanceToWin,
    Pair,
    Player
} from "./BasicEntities";
import {randomInt} from "crypto";
import {WinConditionManager} from "./WinConditions";
import {GameBoard} from "./GameBoard";

export class ComputeLookup {
    private computeById = new Map<string, AttachedCompute>()

    constructor() {
        this.registerCompute(lazyCompute())
        this.registerCompute(optimumWinCompute())
    }

    public getComputeForName(name?: string): AttachedCompute | undefined {
        if (name) {
            return this.computeById.get(name)
        }
        return undefined
    }

    public registerCompute(attachedCompute: AttachedCompute): void {
        this.computeById.set(attachedCompute.name, attachedCompute)
    }
}

// This is not something will necessarily win, so not a simple one to test
// We can keep this _very_ simple, by relying on turn rejection mechanism to ensure re-attempts for failed placements
export function lazyCompute(tiebreak: ComputeResolver = randomResolver): AttachedCompute {
    return {
        name: "lazy",
        function(gameBoard: GameBoard, currentPlayer: Player): CylindricalKey {
            const chosenMove = tiebreak([], gameBoard)
            console.log('Chosen move (rnd): ', chosenMove, 'for player: ', currentPlayer.name)
            return chosenMove
        }
    }
}

export function optimumWinCompute(tiebreak: ComputeResolver = randomResolver): AttachedCompute {
    return {
        name: "optimumWin",
        function(gameBoard: GameBoard, currentPlayer: Player): CylindricalKey {
            const manager = new WinConditionManager(gameBoard.gameDimensions())
            // Eliminate impossible wins + rank wins by moves remaining
            const rankedCPUWins = manager.orderedDistancesToWin(gameBoard.gameState().boardMap, currentPlayer)

            // SHORT CIRCUIT - First move might as well be random
            if (gameBoard.gameState().boardMap.size === 0) {
                return tiebreak([], gameBoard)
            }

            // SHORT CIRCUIT - If win within one turn, take it
            const possibleWin = rankedCPUWins.filter(distance => distance.remainingMoves.length === 1)[0]?.remainingMoves[0]
            if (possibleWin != undefined) {
                return possibleWin
            }

            // Deduce how close other players are to a win
            const rankedWinsPerPlayer = new Map<Player, DistanceToWin[]>()
            const players: Player[] = []
            gameBoard.gameState().boardMap.forEach((value: Player) => {
                if (players.filter(p => p.id === value.id).length === 0) {
                    players.push(value)
                }
            })
            players.forEach((value: Player) => {
                if (value.id !== currentPlayer.id) {
                    rankedWinsPerPlayer.set(value, manager.orderedDistancesToWin(gameBoard.gameState().boardMap, value))
                }
            })

            // SHORT CIRCUIT - If next player will win within one turn, take it
            let returnedLoss: CylindricalKey | void = undefined
            rankedWinsPerPlayer.forEach((value: DistanceToWin[], key: Player) => {
                if (gameBoard.nextPlayer().id !== key.id) {
                    return
                }
                //this player is the next player
                const possibleLoss = value
                    .filter(distance => distance.remainingMoves.length === 1)[0]
                    ?.remainingMoves[0]
                returnedLoss = possibleLoss
            })
            if (returnedLoss != undefined) {
                return returnedLoss
            }

            const allKeys = []
            for (let segment = 0; segment < gameBoard.gameDimensions().segment; segment++) {
                for (let radius = 0; radius < gameBoard.gameDimensions().radius; radius++) {
                    for (let depth = 0; depth < gameBoard.gameDimensions().depth; depth++) {
                        allKeys.push({
                            segment: segment,
                            radius: radius,
                            depth: depth
                        })
                    }
                }
            }

            // Iterate through each valid move, and get the combined weight
            const nextMoves = allKeys
                .map(move => getScoreForKey(move, rankedCPUWins, rankedWinsPerPlayer))
                .filter(scoredMove => scoredMove.value > 0)
                .sort((a, b) => b.value - a.value)

            // Pick the one of the best
            const maxScore = nextMoves[0]?.value ?? 0
            const topMoves = nextMoves.filter(scoredMove => scoredMove.value === maxScore)
            const chosenMove = tiebreak(topMoves, gameBoard)
            console.log('Chosen move: ', chosenMove)
            return chosenMove
        }
    }
}

function randomResolver(choices: Pair<CylindricalKey,number>[], gameBoard: GameBoard): CylindricalKey {
    if (choices.length === 0) {
        return {
            segment: randomInt(0, gameBoard.gameDimensions().segment),
            radius: randomInt(0, gameBoard.gameDimensions().radius),
            depth: randomInt(0, gameBoard.gameDimensions().depth),
        }
    }
    return ( choices.length > 1 ? choices[randomInt(choices.length-1)] : choices[0] ).key
}

export function firstChoiceResolver(choices: Pair<CylindricalKey,number>[], gameBoard: GameBoard): CylindricalKey {
    if (gameBoard.gameState().boardMap.size === 0 || choices.length === 0) {
        return {
            segment: 0,
            radius: 0,
            depth: 0
        }
    }
    return choices[0].key
}

function getScoreForKey(move: CylindricalKey, rankedCPUWins: DistanceToWin[], rankedWinsPerPlayer: Map<Player, DistanceToWin[]>): Pair<CylindricalKey, number> {
    let score = rankedCPUWins
        .filter(win => win.remainingMoves.filter(key => cylindricalKeyHash(key) === cylindricalKeyHash(move)).length > 0)
        .map(win => Math.pow(3, win.weight+1))
        .reduce((acc, val) => acc as number + val as number, 0)

    rankedWinsPerPlayer.forEach( (value) => {
        score += value
            .filter(win => win.remainingMoves.filter(key => cylindricalKeyHash(key) === cylindricalKeyHash(move)))
            .map(win => Math.pow(2, win.weight+1))
            .reduce((acc, val) => acc as number + val as number, 0)
    })

    return {
        key: move,
        value: score
    };
}
