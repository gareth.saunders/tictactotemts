import {menuData} from "../../states/menu-data.js";

export default {
  data() {
    return menuData
  },
  template: ` 
    <div id="new-game-options">
        <p>New game</p>
        <div class="btn-group">
            <button v-on:click="toggle('type', 'hotseat')" v-bind:class="{ active: isSelected('type', 'hotseat') }">Hotseat</button>
            <button v-on:click="toggle('type', 'invite')" v-bind:class="{ active: isSelected('type', 'invite') }">Invite</button>
        </div>
        <p>Player Two</p>
        <div class="btn-group">
            <button v-on:click="toggle('playerTwo', 'human')" v-bind:class="{ active: isSelected('playerTwo', 'human') }">Human</button>
            <button v-on:click="toggle('playerTwo', 'computer')" v-bind:class="{ active: isSelected('playerTwo', 'computer') }">Computer</button>
        </div>
        <p>Player Three</p>
        <div class="btn-group">
            <button v-on:click="toggle('playerThree', null)" v-bind:class="{ active: isSelected('playerThree', null) }">None</button>
            <button v-on:click="toggle('playerThree', 'human')" v-bind:class="{ active: isSelected('playerThree', 'human') }">Human</button>
            <button v-on:click="toggle('playerThree', 'computer')" v-bind:class="{ active: isSelected('playerThree', 'computer') }">Computer</button>
        </div>

        <div>
            <button v-on:click="newGame()" >START NEW GAME</button>
            <button v-show="gameStarted" v-on:click="menuToggle()">Cancel</button>
        </div>
    </div>
  `
}