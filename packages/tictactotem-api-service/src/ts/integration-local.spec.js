"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
const supertest_1 = __importDefault(require("supertest"));
const BasicEntities_1 = require("tictactotem-engine/src/ts/BasicEntities");
describe('?', () => {
    it('basic turn-take test, one human, one AI', () => __awaiter(void 0, void 0, void 0, function* () {
        const newGameResult = yield supertest_1.default(index_1.server).get("/local/new");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined();
        const gameLinks = JSON.parse(newGameResult.text);
        expect(gameLinks.moveLink).toMatch(/\/local\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);
        const result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=0`);
        expect(result.statusCode).toEqual(200);
        const status = yield supertest_1.default(index_1.server).get(`${gameLinks.statusLink}`);
        expect(status.statusCode).toEqual(200);
        const gameStatusModel = JSON.parse(status.text);
        expect(Object.keys(gameStatusModel).length).toEqual(2);
    }));
    it('basic get-to-win test, three humans', () => __awaiter(void 0, void 0, void 0, function* () {
        const newGameResult = yield supertest_1.default(index_1.server).get("/local/new?player2type=human&player3type=human");
        expect(newGameResult.statusCode).toEqual(200);
        expect(newGameResult.text).toBeDefined();
        const gameLinks = JSON.parse(newGameResult.text);
        expect(gameLinks.moveLink).toMatch(/\/local\/turn\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);
        let result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=0&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=0&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=1&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=1&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=1&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=2&radius=0&depth=0`); //P1
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=2&radius=1&depth=0`); //P2
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=2&radius=0&depth=3`); //P3
        expect(result.statusCode).toEqual(200);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=3&radius=0&depth=0`); //P1 (Win)
        expect(result.statusCode).toEqual(200);
        const winCondition = JSON.parse(result.text);
        expect(winCondition).toBeDefined();
        expect(winCondition.winConditionAcheived.type).toEqual(BasicEntities_1.WinConditionType.CIRCLE);
        result = yield supertest_1.default(index_1.server).get(`${gameLinks.moveLink}?segment=3&radius=1&depth=0`); //P2 (Can't move)
        expect(result.statusCode).toEqual(200);
        const status = yield supertest_1.default(index_1.server).get(`${gameLinks.statusLink}`);
        expect(status.statusCode).toEqual(200);
        const gameStatusModel = JSON.parse(status.text);
        expect(Object.keys(gameStatusModel).length).toEqual(10);
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield index_1.server.close();
    }));
});
