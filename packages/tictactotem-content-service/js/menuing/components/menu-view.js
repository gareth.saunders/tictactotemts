import PaneHolder from "./pane-holder.js";
import EntryPane from "./panes/entry-pane.js";
import NewGamePane from "./panes/new-game-pane.js";
import LobbyPane from "./panes/lobby-pane.js";
import SettingsPane from "./panes/settings-pane.js";
import AttributionPane from "./panes/attribution-pane.js";
import {viewData} from "../states/view-data.js";
import {menuData} from "../states/menu-data.js";

export default {
  data() {
    return {
      menuData,
      viewData
    }
  },
  components: {
    EntryPane,
    NewGamePane,
    AttributionPane,
    SettingsPane,
    LobbyPane,
    PaneHolder
  },
  template: ` 
    <div id="info" :class="{ darkMode: menuData.darkMode }" class="game-panel" v-show="menuData.menuActive">
        <img class="large-screen-only" src="assets/images/menu-banner-vertical.png"/>
        <img class="small-screen-only" src="assets/images/menu-banner-horizontal.png"/>
        
        <pane-holder v-bind:title="viewData.currentViewName()">
            <entry-pane v-if="viewData.currentView() === undefined"></entry-pane>
            <new-game-pane v-else-if="viewData.currentView() === 'new-game'"></new-game-pane>
            <lobby-pane v-else-if="viewData.currentView() === 'lobby'"></lobby-pane>
            <settings-pane v-else-if="viewData.currentView() === 'settings'"></settings-pane>
            <attribution-pane v-else-if="viewData.currentView() === 'attribution'"></attribution-pane>
        </pane-holder>
        
    </div>
  `
}