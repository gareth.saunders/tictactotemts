import {Context, GET, HeaderParam, Path, ServiceContext} from "typescript-rest";
import {RedisConnector} from "../connectors/RedisConnector";

@Path("/health")
export class HealthController {
    @Context
    context: ServiceContext | undefined;

    @GET
    async basicHealth(@HeaderParam("monitoring_token") monitoringToken: string): Promise<HealthResponse> {
        const helper = await RedisConnector.getInstance();
        const overallStatus = helper.isRedisUp() ? HealthStatus.HEALTHY : HealthStatus.DEGRADED
        this.context?.response.setHeader("service_status", overallStatus.toString())

        // const isLocalhost = this.context?.request?.url.split(':')[0].substring(2) === "localhost"

        const simpleResponse = {
            service: overallStatus
        }

        if (!monitoringToken /*&& !isLocalhost*/) {
            return simpleResponse
        }

        if (monitoringToken && monitoringToken !== process.env.MONITORING_TOKEN /*&& !isLocalhost*/) {
            return simpleResponse
        }

        return {
            service: overallStatus,
            redis: helper.isRedisUp() ? HealthStatus.HEALTHY : HealthStatus.UNAVAILABLE
        }
    }
}

type HealthResponse = SimpleHealthResponse | DetailedHealthResponse

type SimpleHealthResponse = {
    service: HealthStatus
}

type DetailedHealthResponse = {
    service: HealthStatus,
    redis: HealthStatus
}

enum HealthStatus {
    HEALTHY = 'healthy',
    DEGRADED = 'degraded',
    UNAVAILABLE = 'unavailable'
}