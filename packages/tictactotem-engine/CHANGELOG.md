# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.2.0...tictactotem-engine@1.3.0) (2022-11-21)


### Features

* Introduce invite mode to UI ([ff01e7e](https://gitlab.com/gareth.saunders/tictactotemts/commit/ff01e7ef839e8081c876a5d9e64b7b5f45e3a2d2))



## [1.2.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.1.1...tictactotem-engine@1.2.0) (2022-10-19)


### Features

* Add backend functionality and endpoint for invite-based games ([04c7b4c](https://gitlab.com/gareth.saunders/tictactotemts/commit/04c7b4c7c3d68ddfe6186ce486bf69d29304a7fa))



### [1.1.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.1.0...tictactotem-engine@1.1.1) (2022-08-18)


### Bug Fixes

* Security and functional improvements to the API health endpoint ([0a28954](https://gitlab.com/gareth.saunders/tictactotemts/commit/0a289549a7a924ac6eaedbad8cd2fe5e767e9227))



## [1.1.0](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.0.3...tictactotem-engine@1.1.0) (2022-07-26)


### Features

* Add Redis shared state support to the API service ([16f2609](https://gitlab.com/gareth.saunders/tictactotemts/commit/16f26099936ac1a1db890a0c5cef31af286367ed))



### [1.0.3](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.0.2...tictactotem-engine@1.0.3) (2022-04-03)


### Bug Fixes

* Add rolling update support (with health checks), and seperate live and preview pipeline steps ([a4d0687](https://gitlab.com/gareth.saunders/tictactotemts/commit/a4d06873732e19ab23fe1e3de2a6f3119f6d9607))



### [1.0.2](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@1.0.1...tictactotem-engine@1.0.2) (2022-01-26)


### Bug Fixes

* **build:** Add general root build step, enable pipelines for root changes ([bd45bb5](https://gitlab.com/gareth.saunders/tictactotemts/commit/bd45bb52093de5dd49e9e6f0bc0fe0faadc0cdd2))



### [1.0.1](https://gitlab.com/gareth.saunders/tictactotemts/compare/tictactotem-engine@0.5.0...tictactotem-engine@1.0.1) (2022-01-21)


### Bug Fixes

* Manual bump to one-aught (leave pre-release) ([17fb56d](https://gitlab.com/gareth.saunders/tictactotemts/commit/17fb56d4b187e716ceb80dedd87fffe66674f41a))



## 0.5.0 (2022-01-21)


### Bug Fixes

* Lerna builds to be versioned independently ([a71f7d6](https://gitlab.com/gareth.saunders/tictactotemts/commit/a71f7d68d3223b04874780305f2f429163b14c44))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))

## 0.2.0 (2022-01-06)


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add default email for npm publish version bump ([f6c7061](https://gitlab.com/gareth.saunders/tictactotemts/commit/f6c70614d0b0bb55b95f919c8774d623ded9f670))
* Add reset for GitLab CI git remotes ([bbadaac](https://gitlab.com/gareth.saunders/tictactotemts/commit/bbadaac4fac8ee16b486a0e9021b0a7331e8f30c))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Attempt a direct merge to main with Deploy Key ([a07b7bd](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07b7bd02fd8f5e727e429bf6a2b783cc82cdc91))
* Attempt autobump ([0421036](https://gitlab.com/gareth.saunders/tictactotemts/commit/04210361c4d87542b49b791dfc9bac6cb2807838))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* Correct engine version for publish and push ([6c18694](https://gitlab.com/gareth.saunders/tictactotemts/commit/6c186943b9cd8363213935104812b7c062a50dcb))
* Ensure engine deploy step pushes bump to master ([8a913d3](https://gitlab.com/gareth.saunders/tictactotemts/commit/8a913d3bea2589f0659550560818eeb9739e5546))
* Force an Engine bump to reanimate build ([71aac20](https://gitlab.com/gareth.saunders/tictactotemts/commit/71aac20e3f4bbf56733607412b18f92ed1cd1e1a))
* JS module flag set for Engine ([07dda1e](https://gitlab.com/gareth.saunders/tictactotemts/commit/07dda1e5b45e95d31eb864e1915ae265b6951fa1))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Revert modularisation of Engine library by package definition ([64708ea](https://gitlab.com/gareth.saunders/tictactotemts/commit/64708eac3e0d479fb4d168494f039a5b67c7d1a7))
* Use Deploy Key to push version bump to master ([36306fe](https://gitlab.com/gareth.saunders/tictactotemts/commit/36306fe1f42bd8156671a7f051fcb172263b3163))



# 0.4.0 (2022-01-13)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add default email for npm publish version bump ([f6c7061](https://gitlab.com/gareth.saunders/tictactotemts/commit/f6c70614d0b0bb55b95f919c8774d623ded9f670))
* Add reset for GitLab CI git remotes ([bbadaac](https://gitlab.com/gareth.saunders/tictactotemts/commit/bbadaac4fac8ee16b486a0e9021b0a7331e8f30c))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Attempt a direct merge to main with Deploy Key ([a07b7bd](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07b7bd02fd8f5e727e429bf6a2b783cc82cdc91))
* Attempt autobump ([0421036](https://gitlab.com/gareth.saunders/tictactotemts/commit/04210361c4d87542b49b791dfc9bac6cb2807838))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* Correct engine version for publish and push ([6c18694](https://gitlab.com/gareth.saunders/tictactotemts/commit/6c186943b9cd8363213935104812b7c062a50dcb))
* Ensure engine deploy step pushes bump to master ([8a913d3](https://gitlab.com/gareth.saunders/tictactotemts/commit/8a913d3bea2589f0659550560818eeb9739e5546))
* Force an Engine bump to reanimate build ([71aac20](https://gitlab.com/gareth.saunders/tictactotemts/commit/71aac20e3f4bbf56733607412b18f92ed1cd1e1a))
* JS module flag set for Engine ([07dda1e](https://gitlab.com/gareth.saunders/tictactotemts/commit/07dda1e5b45e95d31eb864e1915ae265b6951fa1))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Revert modularisation of Engine library by package definition ([64708ea](https://gitlab.com/gareth.saunders/tictactotemts/commit/64708eac3e0d479fb4d168494f039a5b67c7d1a7))
* Use Deploy Key to push version bump to master ([36306fe](https://gitlab.com/gareth.saunders/tictactotemts/commit/36306fe1f42bd8156671a7f051fcb172263b3163))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))





# 0.3.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add default email for npm publish version bump ([f6c7061](https://gitlab.com/gareth.saunders/tictactotemts/commit/f6c70614d0b0bb55b95f919c8774d623ded9f670))
* Add reset for GitLab CI git remotes ([bbadaac](https://gitlab.com/gareth.saunders/tictactotemts/commit/bbadaac4fac8ee16b486a0e9021b0a7331e8f30c))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Attempt a direct merge to main with Deploy Key ([a07b7bd](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07b7bd02fd8f5e727e429bf6a2b783cc82cdc91))
* Attempt autobump ([0421036](https://gitlab.com/gareth.saunders/tictactotemts/commit/04210361c4d87542b49b791dfc9bac6cb2807838))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* Correct engine version for publish and push ([6c18694](https://gitlab.com/gareth.saunders/tictactotemts/commit/6c186943b9cd8363213935104812b7c062a50dcb))
* Ensure engine deploy step pushes bump to master ([8a913d3](https://gitlab.com/gareth.saunders/tictactotemts/commit/8a913d3bea2589f0659550560818eeb9739e5546))
* Force an Engine bump to reanimate build ([71aac20](https://gitlab.com/gareth.saunders/tictactotemts/commit/71aac20e3f4bbf56733607412b18f92ed1cd1e1a))
* JS module flag set for Engine ([07dda1e](https://gitlab.com/gareth.saunders/tictactotemts/commit/07dda1e5b45e95d31eb864e1915ae265b6951fa1))
* Make git version tagging monorepo-aware ([c8bbabe](https://gitlab.com/gareth.saunders/tictactotemts/commit/c8bbabe230dd6e5b88e70fa2c969d8706e5c14d3))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Revert modularisation of Engine library by package definition ([64708ea](https://gitlab.com/gareth.saunders/tictactotemts/commit/64708eac3e0d479fb4d168494f039a5b67c7d1a7))
* Use Deploy Key to push version bump to master ([36306fe](https://gitlab.com/gareth.saunders/tictactotemts/commit/36306fe1f42bd8156671a7f051fcb172263b3163))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))





# 0.2.0 (2022-01-06)


### Bug Fixes

*  Move yarn install to new checkout contexts, for pipeline bumps ([0b400e3](https://gitlab.com/gareth.saunders/tictactotemts/commit/0b400e33a426f220178680e1af025c39c258732d))
* Add default email for npm publish version bump ([f6c7061](https://gitlab.com/gareth.saunders/tictactotemts/commit/f6c70614d0b0bb55b95f919c8774d623ded9f670))
* Add reset for GitLab CI git remotes ([bbadaac](https://gitlab.com/gareth.saunders/tictactotemts/commit/bbadaac4fac8ee16b486a0e9021b0a7331e8f30c))
* Add updated yarn.lock (breaks build if updated in build) ([f5231fd](https://gitlab.com/gareth.saunders/tictactotemts/commit/f5231fd750500d261beed71a68d5a47135a6aa5b))
* Alter pipeline yarn installs to correctly reflect the changes of context ([6983f30](https://gitlab.com/gareth.saunders/tictactotemts/commit/6983f3003475a9019704f048e89b0bd43553afb4))
* Attempt a direct merge to main with Deploy Key ([a07b7bd](https://gitlab.com/gareth.saunders/tictactotemts/commit/a07b7bd02fd8f5e727e429bf6a2b783cc82cdc91))
* Attempt autobump ([0421036](https://gitlab.com/gareth.saunders/tictactotemts/commit/04210361c4d87542b49b791dfc9bac6cb2807838))
* Control package bump conflicts using resource groups and safety pulls ([38a8cc0](https://gitlab.com/gareth.saunders/tictactotemts/commit/38a8cc0cff86ab38a1b282f24e90ac8bc04e7e0d))
* Correct bump-skip check for all pipelines ([659197f](https://gitlab.com/gareth.saunders/tictactotemts/commit/659197f975870f44ecda26ffa4f09075369b7a3e))
* Correct engine version for publish and push ([6c18694](https://gitlab.com/gareth.saunders/tictactotemts/commit/6c186943b9cd8363213935104812b7c062a50dcb))
* Ensure engine deploy step pushes bump to master ([8a913d3](https://gitlab.com/gareth.saunders/tictactotemts/commit/8a913d3bea2589f0659550560818eeb9739e5546))
* Force an Engine bump to reanimate build ([71aac20](https://gitlab.com/gareth.saunders/tictactotemts/commit/71aac20e3f4bbf56733607412b18f92ed1cd1e1a))
* JS module flag set for Engine ([07dda1e](https://gitlab.com/gareth.saunders/tictactotemts/commit/07dda1e5b45e95d31eb864e1915ae265b6951fa1))
* Replace standard version with the monorepo-aware lerna publish command ([9bba9fa](https://gitlab.com/gareth.saunders/tictactotemts/commit/9bba9fa4883e02e6c32738bc8ba0e107ba94be43))
* Revert modularisation of Engine library by package definition ([64708ea](https://gitlab.com/gareth.saunders/tictactotemts/commit/64708eac3e0d479fb4d168494f039a5b67c7d1a7))
* Use Deploy Key to push version bump to master ([36306fe](https://gitlab.com/gareth.saunders/tictactotemts/commit/36306fe1f42bd8156671a7f051fcb172263b3163))


### Features

* Add artifact publishing to pipeline (Part One). ([d1bb8cc](https://gitlab.com/gareth.saunders/tictactotemts/commit/d1bb8cc4739823e43b726f9460f4c62af780e0ce))
* Extend pipelines using Conventional Commits to automate SemVer ([a68ea12](https://gitlab.com/gareth.saunders/tictactotemts/commit/a68ea12316228c127f708241a0c56258cc648681))
* Introduce three.js to create a Minimum Viable Demonstrator for front-end ([c5e90bf](https://gitlab.com/gareth.saunders/tictactotemts/commit/c5e90bf14292f4832fbfbbdb349fc0b964176367))
