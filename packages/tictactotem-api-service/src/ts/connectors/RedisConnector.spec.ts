import * as redis from 'redis';
import {RedisError} from 'redis-errors';
import {RedisConnector} from "./RedisConnector";
import {GameBoard} from "../../../../tictactotem-engine/src/ts/GameBoard";
import {PlayerConnectionType} from "../../../../tictactotem-engine/src/ts/BasicEntities";
import {GoneError} from "typescript-rest/dist/server/model/errors";
import {sleep} from "../Utils";

const redisEquivalentMap = new Map<string, string>()
const redisCallBackMap = new Map<string, (err: unknown) => void>()
let breakConnection = false

jest.mock('redis', () => {
    return {
        createClient: () => {
            return {
                on: (event: string, callback: (err: unknown) => void) => {
                    redisCallBackMap.set(event, callback)
                },
                connect: async () => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    return
                },
                GET: async (key: string) => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    return redisEquivalentMap.get(key)
                },
                SET: async (key: string, value: string) => {
                    if (breakConnection) {
                        throw new RedisError("test")
                    }
                    redisEquivalentMap.set(key, value)
                }
            }
        }
    }
});

beforeEach( async () => {
    redisEquivalentMap.clear()
    redisCallBackMap.clear()
    breakConnection = false

    redisHelper = await RedisConnector.getInstance()
})

let redisHelper: RedisConnector;

describe('Redis Get Operation', () => {

    it('should return game when one is found', async () => {
        //Given
        const players = []
        players.push({
            id: 1,
            name: "Player One (Human)"
        })
        const game = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        }, {
            idGenerator: () => "key"
        })
        redisEquivalentMap.set('key', game.persist())

        //When
        const result = await redisHelper.get('key')

        //Then
        if (result instanceof GameBoard) {
            expect(result.id).toBe(game.id)
        } else {
            expect(false).toBe(true)
        }
        expect(redisHelper.isRedisUp()).toBeTruthy()
    })

    it('should report missing game when one cannot be found', async () => {
        //Given


        //When
        const result = await redisHelper.get('key')

        //Then
        expect(result).toBeUndefined()
        expect(redisHelper.isRedisUp()).toBeTruthy()
    })

    it('should report failure upon client connection failure', async () => {
        //Given
        breakConnection = true
        let expectedError = null

        //When
        try {
            await redisHelper.get('key')
        } catch (e) {
            expectedError = e
        }

        //Then
        expect(expectedError).toStrictEqual(new GoneError("The game you are looking for cannot be found"))
        expect(redisHelper.isRedisUp()).toBeFalsy()
    })
})

describe('Redis Set Operation', () => {

    it('should persist new game in normal operation', async () => {


        //Given
        const players = []
        players.push({
            id: 1,
            name: "Player One (Human)"
        })
        const oldGame = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        }, {
            idGenerator: () => "key"
        })
        redisEquivalentMap.set('key', oldGame.persist())

        //When
        await redisHelper.set(oldGame)

        //Then
        expect(redisEquivalentMap.get('key')).toBe(oldGame.persist())
        expect(redisHelper.isRedisUp()).toBeTruthy()
    })


    it('should overwrite new game in normal operation', async () => {
        //Given
        const players = []
        players.push({
            id: 1,
            name: "Player One (Human)"
        })
        const oldGame = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        }, {
            idGenerator: () => "key"
        })
        redisEquivalentMap.set('key', oldGame.persist())

        const newGame = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: {
                segment: 5,
                radius: 3,
                depth: 5
            },
            players: players
        }, {
            idGenerator: () => "key"
        })

        //When
        await redisHelper.set(newGame)

        //Then
        expect(redisEquivalentMap.get('key')).toBe(newGame.persist())
        expect(redisHelper.isRedisUp()).toBeTruthy()

    })

    it('should report failure upon client connection failure', async () => {
        //Given
        breakConnection = true
        let expectedError = null

        const players = []
        players.push({
            id: 1,
            name: "Player One (Human)"
        })
        const oldGame = new GameBoard({
            playerConnectionType: PlayerConnectionType.HOTSEAT,
            dimensions: {
                segment: 4,
                radius: 2,
                depth: 4
            },
            players: players
        }, {
            idGenerator: () => "key"
        })

        //When
        try {
            await redisHelper.set(oldGame)
        } catch (e) {
            expectedError = e
        }

        //Then
        expect(expectedError).toStrictEqual(new GoneError("This game cannot be persisted at this time"))
        expect(redisHelper.isRedisUp()).toBeFalsy()
    })
})

describe('Redis Helper construction', () => {
    it('should alert to immediate connection issues', async () => {
        //Given
        breakConnection = true
        let expectedError = null

        //When
        const helper = new RedisConnector({})
        try {
            await helper.attemptConnection()
        } catch (e) {
            expectedError = e
        }

        //Then
        expect(expectedError).toStrictEqual(new GoneError('Cache connection has failed'))
        expect(helper.isRedisUp()).toBeFalsy()

    })

    it('should correct to connection after backoff', async () => {
        //Given
        breakConnection = true
        let expectedError = null

        //When
        const helper = new RedisConnector({backoffMultiplier: 3})
        try {
            await helper.attemptConnection()
        } catch (e) {
            expectedError = e
        }

        await sleep(12)
        expect(helper.isRedisUp()).toBeFalsy();
        expect(helper.currentBackoffPeriod()).toBe(27);
        breakConnection = false
        await sleep(3)

        //Then
        expect(expectedError).toStrictEqual(new GoneError('Cache connection has failed'))
        expect(helper.isRedisUp()).toBeTruthy();
        expect(helper.currentBackoffPeriod()).toBe(1);

    },20000)
})

describe('Redis Connection Tracking', () => {

    it.skip('should return true after normal operation', () => {
        // Part of other tests, double-check if needed explicitly
    })

    it.skip('should return false if connection issues are encountered', () => {
        // Part of other tests, double-check if needed explicitly
    })

})
