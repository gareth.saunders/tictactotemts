export const settings = {
  host: "http://localhost:63342",
  apiHost: "http://localhost:3001",
  eventsHost: "ws://localhost:3002",
  releaseIdentifier: "Running from IDE"
}
