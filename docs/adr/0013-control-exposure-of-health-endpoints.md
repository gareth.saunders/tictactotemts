# 13. Control Exposure of Health Endpoints

Date: 2022-07-31

## Status

2022-07-31 proposed

## Context

In order to correctly facilitate rolling update, service definitions have recently been updated to use readiness probes.
Further work involving the API service and downstream Redis further evolved this to use separate liveness and readiness 
probes with differing health interpretation semantics; the API health endpoint was modified to provide extra health data,
including the ability to distinguish between degraded and offline states.

The distinction is necessary in that the service can recover from an unavailable Redis, but cannot function without it.
Therefore, it should fail the readiness check but not the 

Presently, the health endpoint for the API service with always return a 200 status (if the service is up and responsive), 
where the JSON payload contains the main service status, and that by inference of downstream dependencies (i.e. the 
Redis cluster).

Healthy example: 

GET /health 200
{
    service: healthy
    redis: healthy
}

Degraded example:

GET /health 200
{
    service: degraded
    redis: unavailable
}

The addition and elaboration of the API service health endpoint has introduced two opposing development avenues for 
further consideration: 
 - We now have a service endpoint that exposes details about underlying services and their states.
 - Externally-accessible API health status could be of great use in providing helpful client feedback.

In the former case, where some underlying service details should not be seen outside the cluster, this introduces an 
increase in attack surface area. 

In the latter case, the UI (entirely browser-based) can inform users of, or mitigate issues with, server-side issues 
ahead-of-time, providing means of avoiding frustration and technical complications that arise from connection problems; 
other future services could use the extra detail to enact context-sensitive mitigations, or (in the case of a service 
like Grafana) reflect richer diagnostics.

Both scenarios must be accommodated for; we cannot risk exposing internal state, nor do we want to remove the option of 
rich failure resolution. But if we consider the overall state of a public service to be discoverable regardless, only 
the downstream dependencies need to be controlled.

## Decision

To facilitate this, there are a number of options to consider

1. Only use the service field, removing any reference of downstream Redis
2. Return 200 OK/503 Service Unavailable instead of a payload
   - Kubernetes cannot differentiate between error codes using HTTP checks, which means readiness/liveliness ambiguity
   - Ironically, outside the pod, a failed readiness check will impose _this_ mechanism on other observers
3. Limit payload exposure by using an access token to control access to sensitive fields
4. Limit payload exposure by limiting access to sensitive fields using a "localhost" filter (in-pod access only)

If we take into account status code behaviour re: readiness failure, we can use a hybrid of options 2 & 3; by using an
access token, we can limit the _granular_ output to only trusted services, but still let all services use the overall 
state. In the event of service degradaton, the health endpoint (in fact, the whole pod) would simply return a 503 
status - if we wish to use Kubernetes probes correctly, this has to be the way; however, the extra state information is 
useful for differentiating failure semantics for the different probes, debugging by port-forward or potentially for 
future sidecars. 

Implementing the health endpoint with an access key can also be easily augmented by RBAC in the future. 

It is worth noting that 1) is 3) without a given token, and 4) can be replaced with smart token provision within the build. 

## Consequences

- Reduces current attack surface, without sacrificing service health automation.
- Debug info outside of pod will be reduced due to a mix of Kubernetes readiness probe and security considerations.
- There must be further consideration given to handling secrets within both build and cluster (SOPS? Secure variables + EnvSubst?).
- This will be the first piece of work to consider future RBAC implementation.