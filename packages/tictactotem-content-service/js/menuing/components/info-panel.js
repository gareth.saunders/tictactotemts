import {menuData} from "../states/menu-data.js";
import MenuButton from "./menu-button.js";

export default {
    data() {
        return menuData
    },
    computed: {
        playerDetails() {
            return {
                show: !menuData.menuActive && menuData.playerName !== '?',
                name: menuData.playerName,
            }
        },
    },
    components: {
        MenuButton
    },
    template: ` 
    <div id="info-panel" :class="{ darkMode: darkMode }" class="game-panel">
        <menu-button></menu-button>
        <p>
<!--        <img src="assets/images/player-cross.png" v-show="playerDetails.id === 1">-->
<!--        <img src="assets/images/player-circle.png" v-show="playerDetails.id === 2">-->
<!--        <img src="assets/images/player-diamond.png" v-show="playerDetails.id === 3">-->
            You are {{playerDetails.name}}
        </p>
    </div>
  `
}