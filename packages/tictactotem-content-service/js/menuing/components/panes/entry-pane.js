import {viewData} from "../../states/view-data.js";

export default {
  data() {
    return viewData
  },
  template: ` 
    <div class="menu-root">       
        <button v-for="(item,index) in iterateAllViews()" v-on:click="pushView(item.id)">{{item.name}}</button>
    </div>
  `
}