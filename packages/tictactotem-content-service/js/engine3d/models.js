import * as THREE from 'https://cdn.skypack.dev/three@0.129.0';
import {baseMaterial, genericMaterial, playerMaterial} from "./materials.js";

const circleRadius = 60;
const connectorRadius = 2;
const verticalDisplacement = 80;

const symbolRadius = 10;
const symbolExtrude = 5;

const p1Geometry = function() {
    let long = symbolRadius;
    let short = symbolRadius / 3;

    const plusShape = new THREE.Shape()
        .moveTo(-short, -short)
        .lineTo(-short, -long)
        .lineTo(short, -long)
        .lineTo(short, -short)
        .lineTo(long, -short)
        .lineTo(long, short)
        .lineTo(short, short)
        .lineTo(short, long)
        .lineTo(-short, long)
        .lineTo(-short, short)
        .lineTo(-long, short)
        .lineTo(-long, -short)
    return new THREE.ExtrudeGeometry(plusShape,
        {depth: symbolExtrude, bevelEnabled: false, bevelSegments: 2, steps: 2, bevelSize: 1, bevelThickness: 1});
}()

const p2Geometry = new THREE.TorusGeometry(symbolRadius, 2, 20, 20);

const p3Geometry = function() {
    const diamondShape = new THREE.Shape()
        .moveTo(symbolRadius, 0)
        .lineTo(0, symbolRadius)
        .lineTo(-symbolRadius, 0)
        .lineTo(0, -symbolRadius)
    return new THREE.ExtrudeGeometry(diamondShape,
        {depth: symbolExtrude, bevelEnabled: true, bevelSegments: 2, steps: 2, bevelSize: 3, bevelThickness: 3});
}()

export function addP1Symbol(segment, radius, depth, group) {
    let mesh = new THREE.Mesh(p1Geometry, playerMaterial(0xc8c8ff));
    switch (segment) {
        case 0:
            mesh.position.set(0, (depth * verticalDisplacement), circleRadius * (radius + 1));
            break;
        case 1:
            mesh.position.set(circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
        case 2:
            mesh.position.set(0, (depth * verticalDisplacement), -circleRadius * (radius + 1));
            break;
        case 3:
            mesh.position.set(-circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
    }
    mesh.rotation.set(0, 0, 0);
    mesh.scale.set(1, 1, 1);

    mesh.meta = {
        type: {
            playerToken: true,
        },
        position: {
            segment: segment,
            radius: radius,
            depth: depth,
        }
    }

    group.add(mesh);

    // const light = new THREE.PointLight( 0xffffff, 5, 1000, 1 );
    // light.position.set( mesh.position.x, mesh.position.y, mesh.position.z );
    // group.add( light );
}

export function addP2Symbol(segment, radius, depth, group) {
    let mesh = new THREE.Mesh(p2Geometry, playerMaterial(0xffc8c8));
    switch (segment) {
        case 0:
            mesh.position.set(0, (depth * verticalDisplacement), circleRadius * (radius + 1));
            break;
        case 1:
            mesh.position.set(circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
        case 2:
            mesh.position.set(0, (depth * verticalDisplacement), -circleRadius * (radius + 1));
            break;
        case 3:
            mesh.position.set(-circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
    }
    mesh.rotation.set(0, 0, 0);
    mesh.scale.set(1, 1, 1);

    mesh.meta = {
        type: {
            playerToken: true,
        },
        position: {
            segment: segment,
            radius: radius,
            depth: depth,
        }
    }

    group.add(mesh);
}

export function addP3Symbol(segment, radius, depth, group) {
    let mesh = new THREE.Mesh(p3Geometry, playerMaterial(0xc8ffc8));
    switch (segment) {
        case 0:
            mesh.position.set(0, (depth * verticalDisplacement), circleRadius * (radius + 1));
            break;
        case 1:
            mesh.position.set(circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
        case 2:
            mesh.position.set(0, (depth * verticalDisplacement), -circleRadius * (radius + 1));
            break;
        case 3:
            mesh.position.set(-circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
    }
    mesh.rotation.set(0, 0, 0);
    mesh.scale.set(1, 1, 1);

    mesh.meta = {
        type: {
            playerToken: true,
        },
        position: {
            segment: segment,
            radius: radius,
            depth: depth,
        }
    }

    group.add(mesh);

    // const light = new THREE.PointLight( 0xffffff, 5, 1000, 1 );
    // light.position.set( mesh.position.x, mesh.position.y, mesh.position.z );
    // group.add( light );
}

export function addRings(radius, depth, group) {
    // extruded shape
    const geometry = new THREE.TorusGeometry(circleRadius * (radius + 1), connectorRadius, 16, 100);

    let mesh = new THREE.Mesh(geometry, genericMaterial);
    mesh.position.set(0, (depth * verticalDisplacement), 0);
    mesh.rotation.set(Math.PI / 2, 0, 0);
    mesh.scale.set(1, 1, 1);

    group.add(mesh);
}

export function addPoles(segment, radius, group) {
    const circleShape = new THREE.Shape()
        .moveTo(-connectorRadius, -connectorRadius)
        .quadraticCurveTo(0, 0, connectorRadius, -connectorRadius)
        .quadraticCurveTo(0, 0, connectorRadius, connectorRadius)
        .quadraticCurveTo(0, 0, -connectorRadius, connectorRadius)
    let geometry = new THREE.ExtrudeGeometry(circleShape,
        {
            depth: verticalDisplacement * 3,
            bevelEnabled: false,
            bevelSegments: 2,
            steps: 2,
            bevelSize: 1,
            bevelThickness: 1
        });

    let mesh = new THREE.Mesh(geometry, genericMaterial);
    switch (segment) {
        case 0:
            mesh.position.set(0, verticalDisplacement * 3, circleRadius * (radius + 1));
            break;
        case 1:
            mesh.position.set(circleRadius * (radius + 1), verticalDisplacement * 3, 0);
            break;
        case 2:
            mesh.position.set(0, verticalDisplacement * 3, -circleRadius * (radius + 1));
            break;
        case 3:
            mesh.position.set(-circleRadius * (radius + 1), verticalDisplacement * 3, 0);
            break;
    }

    // mesh.rotation.set( Math.PI/2, 0, Math.PI/2 * segment );
    mesh.rotation.set(Math.PI / 2, 0, 0);
    mesh.scale.set(1, 1, 1);

    group.add(mesh);
}

export function addRods(segment, depth, group) {
    const circleShape = new THREE.Shape()
        .moveTo(-connectorRadius, -connectorRadius)
        .quadraticCurveTo(0, 0, connectorRadius, -connectorRadius)
        .quadraticCurveTo(0, 0, connectorRadius, connectorRadius)
        .quadraticCurveTo(0, 0, -connectorRadius, connectorRadius)
    let geometry = new THREE.ExtrudeGeometry(circleShape,
        {
            depth: circleRadius * 2,
            bevelEnabled: false,
            bevelSegments: 2,
            steps: 2,
            bevelSize: 1,
            bevelThickness: 1
        });

    let mesh = new THREE.Mesh(geometry, genericMaterial);

    mesh.position.set(0, (depth * verticalDisplacement), 0);
    mesh.rotation.set(0, Math.PI / 2 * segment, Math.PI / 2);
    mesh.scale.set(1, 1, 1);

    group.add(mesh);
}

export function addBalls(segment, radius, depth, group) {

    let geometry = new THREE.SphereGeometry(20, 32, 32);

    let mesh = new THREE.Mesh(geometry, baseMaterial);
    switch (segment) {
        case 0:
            mesh.position.set(0, (depth * verticalDisplacement), circleRadius * (radius + 1));
            break;
        case 1:
            mesh.position.set(circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
        case 2:
            mesh.position.set(0, (depth * verticalDisplacement), -circleRadius * (radius + 1));
            break;
        case 3:
            mesh.position.set(-circleRadius * (radius + 1), (depth * verticalDisplacement), 0);
            break;
    }
    mesh.rotation.set(0, 0, 0);
    mesh.scale.set(1, 1, 1);

    mesh.meta = {
        type: {
            interactive: true,
        },
        position: {
            segment: segment,
            radius: radius,
            depth: depth,
        }
    }

    group.add(mesh);
}