import {Errors, GET, Path, PathParam, QueryParam} from "typescript-rest";
import {PlayerConnectionType} from "../../../../tictactotem-engine/src/ts/BasicEntities";
import {GameEndResult, TurnResult} from "../../../../tictactotem-engine/src/ts/ResultEntites";
import {GameService} from "../services/GameService";

@Path("/hotseat")
export class HotseatController {
    gameService: GameService

    constructor() {
        this.gameService = GameService.get()
    }

    @Path("new")
    @GET
    async newGame(@QueryParam("player2type") player2Type: PlayerType = "computer",
                  @QueryParam("player3type") player3Type?: PlayerType): Promise<NewHotseatGameStart> {
        const game = await this.gameService.newGame(PlayerConnectionType.HOTSEAT, player2Type, player3Type)
        return {
            syncMoveLink: `/hotseat/turnsync/${ game.id }`,
            moveLink: `/hotseat/turn/${ game.id }`,
            statusLink: `/hotseat/state/${ game.id }`
        }
    }

    @Path("turnsync/:gameId")
    @GET
    async makeTurnAndReturn(
        @PathParam('gameId') id: string,
        @QueryParam("segment") segment: number,
        @QueryParam("radius") radius: number,
        @QueryParam("depth") depth: number
    ): Promise<SynchronousResult> {
        const result = await this.makeTurn(id, segment, radius, depth)
        const state = await this.gameState(id)
        return {
            result: result,
            state: state
        }
    }

    @Path("turn/:gameId")
    @GET
    async makeTurn(
        @PathParam('gameId') id: string,
        @QueryParam("segment") segment: number,
        @QueryParam("radius") radius: number,
        @QueryParam("depth") depth: number
    ): Promise<TurnResult | GameEndResult> {
        const result = await this.gameService.makeMove(id, segment, radius, depth)
        if (result) {
            return result
        }
        throw new Errors.GoneError("The game you are looking for cannot be found");
    }

    @Path("state/:gameId")
    @GET
    /* eslint-disable-next-line  @typescript-eslint/explicit-module-boundary-types */
    async gameState(
        @PathParam('gameId') id: string
        // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    ) {
        const result = await this.gameService.gameState(id)
        if (result) {
            return result
        }
        throw new Errors.GoneError("The game you are looking for cannot be found");
    }
}

type PlayerType = "computer" | "human"

export interface NewHotseatGameStart {
    syncMoveLink: string
    moveLink: string
    statusLink: string
}

export interface SynchronousResult {
    result: TurnResult | GameEndResult
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    state: any
}
