# 2. Use Typescript as primary game engine language

Date: 2021-05-20

## Status

Accepted

## Context

There have been a number of attempts to (re)write the core engine, all of which have been considered toy projects and 
each has had ... niggles, often around typing/structure (or in the case of JS, a lack of it). Typescript offers flexible 
typing and interfaces that seem an interesting envelope to push.

## Decision

This is to be written in Typescript - Jest is to be used for unit testing.

## Consequences

- We are still fundamentally in Javascript territory, but with extra syntax constraints.
- As a "play" project, this is likely to have code that tries to be clever, rather than "Production-ready".
-- ... and also some approaches that are more Java/JS-aligned, rather than idiomatic TypeScript (feel free to comment). 
