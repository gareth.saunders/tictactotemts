import {
    CylindricalKey,
    CylindricalKeyHash,
    cylindricalKeyHash,
    DistanceToWin,
    Pair,
    Player,
    WinCondition,
    WinConditionType
} from "./BasicEntities";
import {GameEndResult, GameStatus, WinResult} from "./ResultEntites";

export class WinConditionManager {
    private allWinConditions: WinCondition[] = []

    constructor(dimensions: CylindricalKey) {
        this.allWinConditions = this.generateAllConditions(dimensions)
    }

    testForCondition(gameMap: Map<CylindricalKeyHash, Player>): WinResult | void {
        return this.allWinConditions
            .map(wc => this.applyCondition(gameMap, wc))
            .filter((value => value != undefined))[0]
    }

    orderedDistancesToWin(gameMap: Readonly<Map<CylindricalKeyHash, Player>>, player: Player): DistanceToWin[] {
        return this.allWinConditions
            .map(wc => { return {
                condition: wc,
                remainingMoves: this.movesToWin(gameMap, player, wc),
                weight: this.distanceToWin(gameMap, player, wc)
            }})
            .filter((value => value.weight > 0))
            .sort((a, b) => b.weight - a.weight)
    }

    generateAllConditions(dimensions: CylindricalKey): WinCondition[] {
        const allWinConditions: WinCondition[] = []

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.CIRCLE, dimensions, "depth", "radius"))

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.THRU_LINE, dimensions, "depth", "segment"))

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.DROP_LINE, dimensions, "segment", "radius"))

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.DIAGONAL_LINE, dimensions, "segment"))

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.CLOCKWISE_SPIRAL, dimensions, "segment", "radius"))

        allWinConditions.push(
            ...iterateOverKey(WinConditionType.ANTI_CLOCKWISE_SPIRAL, dimensions, "segment", "radius"))

        return allWinConditions
    }

    static generateCondition(type: WinConditionType, start: CylindricalKey, dimensions: CylindricalKey): CylindricalKey[] {
        switch(type) {
            case WinConditionType.CIRCLE: {
                return generateCircleCondition(start, dimensions.segment)
            }
            case WinConditionType.THRU_LINE: {
                return generateThrulineCondition(start, dimensions.radius, dimensions.segment)
            }
            case WinConditionType.DROP_LINE: {
                return generateDroplineCondition(start, dimensions.depth)
            }
            case WinConditionType.DIAGONAL_LINE: {
                return generateDiagonalLineCondition(start, dimensions.radius, dimensions.segment)
            }
            case WinConditionType.CLOCKWISE_SPIRAL: {
                return generateSpiralCondition(start, dimensions.segment, dimensions.depth)
            }
            case WinConditionType.ANTI_CLOCKWISE_SPIRAL: {
                return generateSpiralCondition(start, dimensions.segment, dimensions.depth, true)
            }
        }

        return []
    }

    private applyCondition(gameMap: Map<CylindricalKeyHash, Player>, wc: WinCondition): WinResult | void {
        const winningPlayer: Player | void = wc.moveList
            .map(move => gameMap.get(cylindricalKeyHash(move)))
            .reduce((previousValue, currentValue) => currentValue === previousValue && currentValue != undefined ? currentValue : undefined)
        if (winningPlayer == undefined) {
            return undefined
        } else {
            return {
                status: GameStatus.WIN,
                winConditionAcheived: wc,
                winningPath: wc.moveList,
                winningPlayer: winningPlayer
            }
        }
    }

    private movesToWin(gameMap: Readonly<Map<CylindricalKeyHash, Player>>, player: Player, wc: WinCondition): CylindricalKey[] {
        // TODO: More complex, but consider aligning to distanceToWin re: eliminating blocked wins
        // Currently, impossible wins will be filtered upstream via dTW === 0
        return wc.moveList
            .filter(move => gameMap.get(cylindricalKeyHash(move)) === undefined)
    }

    private distanceToWin(gameMap: Readonly<Map<CylindricalKeyHash, Player>>, player: Player, wc: WinCondition): number {
        if (wc.moveList.filter(move => gameMap.get(cylindricalKeyHash(move)) !== undefined && gameMap.get(cylindricalKeyHash(move))?.id != player.id).length > 0 ) {
            return 0
        }

        return wc.moveList
            .map(move => gameMap.get(cylindricalKeyHash(move)))
            .reduce((previousValue, currentValue) => previousValue + (player.id === currentValue?.id ? 1 : 0), 0)
    }
}

function generateCircleCondition(start: CylindricalKey, totalSegments: number): CylindricalKey[] {
    const conditionMap: CylindricalKey[] = Array<CylindricalKey>(totalSegments)
    for (let i = 0; i < totalSegments; i++) {
        conditionMap[i] = {
            segment: i,
            radius: start.radius,
            depth: start.depth
        }
    }
    return conditionMap;
}

function generateThrulineCondition(start: CylindricalKey, totalRadius: number, totalSegments: number): CylindricalKey[] {
    const conditionMap: CylindricalKey[] = Array<CylindricalKey>(totalRadius*2)
    for (let i = 0; i < totalRadius; i++) {
        conditionMap[i] = {
            segment: start.segment,
            radius: totalRadius-i-1,
            depth: start.depth
        }
    }
    for (let i = 0; i < totalRadius; i++) {
        conditionMap[i+totalRadius] = {
            segment: (start.segment + totalSegments/2) % totalSegments,
            radius: i,
            depth: start.depth
        }
    }
    return conditionMap;
}

function generateDroplineCondition(start: CylindricalKey, totalDepth: number): CylindricalKey[] {
    const conditionMap: CylindricalKey[] = Array<CylindricalKey>(totalDepth)
    for (let i = 0; i < totalDepth; i++) {
        conditionMap[i] = {
            segment: start.segment,
            radius: start.radius,
            depth: i
        }
    }
    return conditionMap;
}

function generateDiagonalLineCondition(start: CylindricalKey, totalRadius: number, totalSegments: number): CylindricalKey[] {
    const conditionMap: CylindricalKey[] = Array<CylindricalKey>(totalRadius*2)
    for (let i = 0; i < totalRadius; i++) {
        conditionMap[i] = {
            segment: start.segment,
            radius: totalRadius - i - 1,
            depth: i
        }
    }
    for (let i = 0; i < totalRadius; i++) {
        conditionMap[i+totalRadius] = {
            segment: (start.segment + totalSegments/2) % totalSegments,
            radius: i,
            depth: i+totalRadius
        }
    }
    return conditionMap;
}

function generateSpiralCondition(start: CylindricalKey, totalSegments: number, totalDepth: number, antiClockwise = false): CylindricalKey[] {
    const conditionMap: CylindricalKey[] = Array<CylindricalKey>(totalDepth)
    for (let i = 0; i < totalDepth; i++) {
        const vector = antiClockwise ? i * -1 : i
        conditionMap[i] = {
            segment: (start.segment + vector) < 0 ? totalSegments + (start.segment + vector) : ((start.segment + vector) % totalSegments),
            radius: start.radius,
            depth: i
        }
    }
    return conditionMap;
}

type DimensionEntry = Pair<string,number> | string

function iterateOverKey(type: WinConditionType, extent: CylindricalKey, ...varyingDimensions: DimensionEntry[]): WinCondition[] {
    const allWinConditions: WinCondition[] = []

    const unprocessedFields = varyingDimensions.filter(value => (value as Pair<string, number>).key == undefined)

    if (unprocessedFields.length > 0) {
        for (let i = 0; i < extent[unprocessedFields[0] as keyof CylindricalKey] ; i++) {
            const newVaryingDimensions: DimensionEntry[] = [...varyingDimensions]
            newVaryingDimensions[newVaryingDimensions.indexOf(unprocessedFields[0])] = {
                key: unprocessedFields[0] as string,
                value: i
            }
            allWinConditions.push(...iterateOverKey(type, extent, ...newVaryingDimensions))
        }
    } else {
        const startingKey: CylindricalKey = {
            segment: 0,
            radius: 0,
            depth: 0
        }

        varyingDimensions.forEach(value => {
            const dimensionEntry = value as Pair<string,number>
            startingKey[dimensionEntry.key as keyof CylindricalKey] = dimensionEntry.value
        })

        allWinConditions.push({
            type: type,
            moveList: WinConditionManager.generateCondition(type, startingKey, extent)
        })
    }

    return allWinConditions
}