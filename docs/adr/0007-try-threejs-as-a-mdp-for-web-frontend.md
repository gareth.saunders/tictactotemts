# 7. Try ThreeJS as a MDP for Web FrontEnd

Date: 2021-07-31

## Status

Accepted

Built upon by [8. use-vuejs-for-frontend-menus](0008-use-vuejs-for-frontend-menus.md)

## Context

MVP for TicTacTotem is to use a web interface, as this can be developed in lockstep, and co-hosted with the API service (alluded to in ADR0005). There are a number of approaches that we could use for this:

* The original direction was to use static web resources:
    * Go 2.5D and use Canvas
    * Leverage WebGL and go full 3D
        * The popular library for wrapping WebGL is ThreeJS
* Write a thick client, specifying a specific mobile platform (iOS, Android) and use their equivalent libraries to the prior approaches.

## Decision

Writing a thick client of any kind is out-of-scope for MVP; it would limit coverage for potential testers/users, the initial plan is to be stateless, and the main benefit of a mobile client is monetisation.   

Previous forays into 2.5D have been limiting; the game board naturally lends itself to a pure 3D environment, particularly where rotation of the board (to get a better view) is concerned.

Three.js is an excellent wrapper for WebGL, it's proven and provide very straightforward abstractions (cameras, raycasting, etc). It appears to allow for rapid iteration, already provides good results where a game engine is concerned and has scope to provide more powerful features downstream.

Much more importantly, Three.js uses ES6 modules, which allows for static HTML to be deployed to host it. As such, we can use it to develop a Minimum Demonstratable Product, that is somewthing that can be built quickly to illustrate how the Game Engine is to be used. 

## Consequences

* Use of Three.js in a lightweight static HTML 'host' will allow for quick iteration, allowing for earlier demonstration.
* When more advanced features are introduced, it may mean more load placed on the API, or other such services introduced to handle the extra functionality that a thick client would handle.
* Not sure yet on the browser-compatibility matrix for Three.js/WebGL, whereas Canvas is W3C and widely adopted. 
